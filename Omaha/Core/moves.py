# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

from collections import OrderedDict
from collections.abc import Iterable, Iterator
from typing import TYPE_CHECKING, Any, Optional
if TYPE_CHECKING:
    from .game import GameState
    from .piece_holder import Location, PieceHolder
    from .player import Player

class Move:
    """The root class of all Move objects."""
    special_moves: OrderedDict[str,dict[str,Any]] = OrderedDict()
    def __init__(self, *, gamestate: "GameState", player: Optional[Player]):
        # This ctor *must not* have a **kwargs, so any stray kwargs
        # passed by error are correctly reported as errors.
        assert gamestate.is_frozen
        self.__gamestate = gamestate
        self.__player = player
    @property
    def gamestate(self) -> "GameState":
        return self.__gamestate
    @property
    def player(self) -> Optional[Player]:
        return self.__player
    def assert_locations(self, holders: Iterable["PieceHolder"]) -> None:
        """Assert any locations referenced by this move belongs to specified holders.

        This allows to easily and early pinpoint problems with the
        game cloning mechanism.  Move classes adding new types of
        references to holders must add the necessary assertions here."""

class PutMove(Move):
    """A move where a piece is put onto a location."""
    def __init__(self, target: Optional["Location"]=None, **kwargs: Any):
        super().__init__(**kwargs)
        self.set_target(target)
    def set_target(self, location: Optional["Location"]) -> None:
        self.__target = location
    @property
    def target(self) -> Optional["Location"]:
        return self.__target
    def assert_locations(self, holders: Iterable["PieceHolder"]) -> None:
        super().assert_locations(holders)
        assert self.__target
        assert self.__target.holder in holders, \
            "%s not in %s" % (self.__target.holder, holders)

class PutOnlyMove(PutMove):
    """A PutMove where the source of piece needs not be specified."""
    def __str__(self) -> str:
        return str(self.target)

class TakeMove(Move):
    """A move where a piece is removed from a location."""
    def __init__(self, source: Optional["Location"]=None, **kwargs: Any):
        super().__init__(**kwargs)
        self.set_source(source)
    def set_source(self, location: Optional["Location"]) -> None:
        self.__source = location
    @property
    def source(self) -> Optional["Location"]:
        return self.__source
    def assert_locations(self, holders: Iterable["PieceHolder"]) -> None:
        super().assert_locations(holders)
        assert self.__source
        assert self.__source.holder in holders, \
            "%s not in %s" % (self.__source.holder, holders)

class GenericDisplaceMove(TakeMove):
    @property
    def target(self) -> Optional["Location"]:
        raise NotImplementedError()

class SimpleDisplaceMove(PutMove, GenericDisplaceMove):
    """A move where a piece is moved from one location to another."""
    def __init__(self, **kwargs: Any):
        super().__init__(**kwargs)
    def set_source(self, location: Optional["Location"]) -> None:
        TakeMove.set_source(self, location)
        PutMove.set_target(self, None)
    def __str__(self) -> str:
        return "%s -> %s" % (self.source, self.target)

class WaypointDisplaceMove(GenericDisplaceMove):
    def __init__(self, **kwargs: Any):
        super().__init__(**kwargs)
        self.waypoints: list["Location"] = []
        self.finished = False
    @property
    def target(self) -> Optional["Location"]:
        if self.finished:
            assert len(self.waypoints) > 0
            return self.waypoints[-1]
        else:
            return None
    @property
    def steps(self) -> Iterator[tuple["Location", "Location"]]:
        """Iterator returning 2-tuples for each step in the move."""
        if len(self.waypoints) > 0:
            assert self.source
            yield (self.source, self.waypoints[0])
        for src_idx in range(0, len(self.waypoints) - 1):
            yield (self.waypoints[src_idx], self.waypoints[src_idx + 1])
    def assert_locations(self, holders: Iterable["PieceHolder"]) -> None:
        super().assert_locations(holders)
        for waypoint in self.waypoints:
            assert waypoint.holder in holders, \
                "%s not in %s" % (waypoint.holder, holders)

    def add_waypoint(self, location: "Location") -> None:
        self.waypoints.append(location)

    def finish(self) -> None:
        self.finished = True

    def __str__(self) -> str:
        result = str(self.source)
        for step in self.waypoints:
            result += " -> %s" % step
        if not self.finished:
            result += " -> ?"
        return result
