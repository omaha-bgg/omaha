# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

from enum import Enum
from collections import OrderedDict
from collections.abc import Sequence
import logging
import sys
from typing import Any, Callable, Optional, Union

import Overlord
from Overlord.misc import pretty

from .exceptions import UserCancel, UnsatisfiableConstraint

if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

__all__ = [ 'Toolkit', 'App',
            'ParamWidget', 'PluginChoiceWidget',
            'VectorImage' ]

logger = logging.getLogger(__name__)

class ParamWidget:
    """
    Base class for a widget that edits a Param.
    """
    def __init__(self, param: Overlord.Param[Overlord.ParamValue], gui: App):
        assert isinstance(param, Overlord.Param)
        self.param, self.gui = param, gui
    def retrieve_value(self, entry: object=None) -> None:
        pass
    def update(self) -> None:
        raise NotImplementedError()

class LabelWidget(ParamWidget):
    def widget(self) -> object:
        return self.gui.tk.RawLabelWidget(self.param.label)

class ChoiceWidget(ParamWidget):
    __subparam_hboxes: dict[tuple[str,str],object]
    __subparam_widgets: dict[Overlord.Param,ParamWidget]
    def _add_subparams(self, combo: object, vbox: object) -> None:
        # handle subparams
        if self.param.has_subparams:
            # create a box holding the combo and it's subparams hboxes
            self.__subparam_hboxes = {}
            self.__subparam_widgets = {}
            for parent_id, parent_param in self.param.subparams.items():
                # FIXME: walk_params ?
                for subparam_key, subparam in reversed(Overlord.dep_ordered_params(parent_param)):
                    hbox = self.__make_subparam_widget(subparam, parent_id, parent_param)
                    if hbox is None:
                        continue
                    self.gui.tk._vbox_add(vbox, hbox)
                    # FIXME this does not support subparam nesting
                    self.__subparam_hboxes[(parent_id, subparam_key)] = hbox

            def __update_subparams_visibility(combo: object, param: Overlord.Param) -> None:
                # FIXME: should also shrink dialog when needed
                for parent_choice in param.subparams.keys():
                    for subparam_key, subparam in param.subparams[parent_choice].items():
                        if not subparam.decl.pinned_value and not subparam.decl.hidden:
                            self._update_one_subparam_visibility(
                                combo, parent_choice,
                                self.__subparam_hboxes[(parent_choice, subparam_key)],
                                subparam)

            self.gui.tk._combo_setchanged_hook(
                combo, __update_subparams_visibility, self.param)

    def _update_one_subparam_visibility(self, combo: object, parent_choice: str,
                                        subparam_box: object, subparam: Overlord.Param) -> None:
        raise NotImplementedError()

    def __make_subparam_widget(self, subparam: Overlord.Param, parent_id: str,
                               parent_param: dict[str,Overlord.Param]) -> object:
        assert isinstance(subparam, Overlord.Param)
        if subparam.decl.pinned_value or subparam.decl.hidden:
            return None
        try:
            hbox, self.__subparam_widgets[subparam] = \
                  self.gui.tk.create_param_widget(subparam,
                                                  self.gui,
                                                  parent_param)
        except UnsatisfiableConstraint as ex:
            hbox = self.gui.tk.RawLabelWidget(ex)
        # FIXME: something causes all subparams to be shown
        # by default, so we need to hide those for
        # non-default choices
        assert isinstance(self.param.value, Overlord.Plugin)
        if parent_id != self.param.value.name:
            self.gui.tk._widget_hide(hbox)
        return hbox

    def _retrieve_subparam_values(self, active_text: str) -> None:
        if  (self.param.has_subparams and
             active_text in self.param.subparams):
            for subparam in self.param.subparams[active_text].values():
                if subparam.decl.pinned_value or subparam.decl.hidden:
                    continue
                self.__subparam_widgets[subparam].retrieve_value()

PluginGroupKey: TypeAlias = Any

class PluginChoiceWidget(ParamWidget):
    """
    A ChoiceWidget wrapper for selecting in an Omaha plugin list.
    """

    __logger = logging.getLogger('Omaha.Core.UI.PluginChoiceWidget')

    def __init__(self, param: Overlord.Param, gui: App):
        assert isinstance(param, Overlord.Param)
        assert isinstance(param.decl, Overlord.params.Plugin)
        super().__init__(param, gui)

        # group plugins by attribute specified in param.decl.group_by if any

        plugin_list = [x for x in gui.plugin_manager.get_plugins_list(
            param.decl.plugintype, pythonplugin=param.decl.pythonplugin,
            pattern=param.decl.pattern)]
        plugin_groups: list[tuple[Overlord.Plugin, PluginGroupKey]] = [
            (plugin, (getattr(plugin.Class, param.decl.group_by)
                      if param.decl.group_by else None))
            for plugin in plugin_list]
        groupset = {group for plugin, group in plugin_groups}
        grouped_plugins: dict[PluginGroupKey,list[Overlord.Plugin]] = {
            g: sorted((plugin for plugin, group in plugin_groups if group is g),
                      key = lambda x: x.name)
            for g in groupset}

        # sort None first if present, so they show before first named group
        groups = list(groupset)
        if None in groups and groups[0] != None:
            groups.remove(None)
            groups[0:0] = [None]

        alternatives: dict[str,Optional[Overlord.Plugin]] = OrderedDict()
        subparams: dict[str,dict[str,Overlord.ParamDecl]] = OrderedDict()
        for group in groups:
            plugin_group = grouped_plugins[group]
            if group != None:
                alternatives["   %s" % group] = None # poor-man's group header
            for plugin in plugin_group:
                if not plugin.usable:
                    # we will gray it out
                    alternatives[plugin.name] = None
                    continue
                alternatives[plugin.name] = plugin
                if not plugin.pythonplugin:
                    continue # no parameter to take care of
                plugin_cls = plugin.Class
                if not issubclass(plugin_cls, Overlord.Parametrizable):
                    continue # no parameter to take care of

                subparamdecls = plugin_cls.parameters_dcl(param.decl.context)
                # pin requested values
                self.__pin_decls(plugin, subparamdecls)

                if len(subparamdecls) > 0:
                    # FIXME: must also inject values from prefs into default
                    subparams[plugin.name] = subparamdecls
        choiceparam = Overlord.Param(
            Overlord.params.Choice(
                label=param.label,
                alternatives = alternatives,
                subparams = subparams,
                default = param.value))
        choiceparam.owner = self
        param.set_subparams(choiceparam.subparams) # FIXME: that shall not be :)

        self._choice_widget = gui.tk.ChoiceWidget(choiceparam, gui)

    @classmethod
    def __pin_decls(cls, plugin: Overlord.Plugin, decls: dict[str,Overlord.ParamDecl]) -> None:
        for subparamid, subparamdecl in decls.items():
            assert isinstance(subparamdecl, Overlord.ParamDecl), \
                   "%r is not a ParamDecl" % (subparamdecl,)
            pinnedvalue_key = "X-Omaha-Parameter-{0}".format(subparamid)
            if plugin.has_datafield(pinnedvalue_key):
                subparamdecl.pinned_value = plugin.datafield(pinnedvalue_key)


class App(Overlord.App):
    def __init__(self, plugin_manager: Overlord.PluginManager, toolkit: Toolkit, **kwargs: Any):
        self.tk = toolkit
        super().__init__(plugin_manager=plugin_manager, **kwargs)
    def _create_gui(self) -> None:
        raise NotImplementedError()

    # serialized-move input

    def set_manual_move_entry_callback(self, callback: Callable[[str],None]) -> None:
        raise NotImplementedError()
    def unset_manual_move_entry_callback(self, callback: Callable[[str],None]) -> None:
        raise NotImplementedError()
    def clear_manual_move_entry(self) -> None:
        raise NotImplementedError()

class VectorImage:
    """An interface for vector image manipulation by the UI toolkit."""
    def __init__(self, svgfilename: str):
        raise NotImplementedError()
    @property
    def width(self) -> int:
        raise NotImplementedError()
    @property
    def height(self) -> int:
        raise NotImplementedError()
    def render_to_bitmap(self, bitmap: Toolkit.BitmapImage, x: int, y: int, width: int, height: int,
                         rotation: Optional[float]=None) -> None:
        """
        Render vector graphics in bbox, after possible rotation.
        """
        raise NotImplementedError()


class FillPattern:
    """An interface for toolkit filling functionnality."""
    # helps coping with the usage differences between Cairo.Pattern and QBrush

class SolidFillPattern(FillPattern):
    def __init__(self, color: object):
        raise NotImplementedError()


DrawingContext: TypeAlias = object

class Toolkit(metaclass=Overlord.OuterClass):
    __innerclasses__ = {'BitmapImage', 'VectorImage',
                        'SolidFillPattern',
                        'RawLabelWidget', 'LabelWidget',
                        'StringWidget', 'IntWidget', 'FloatWidget',
                        'ChoiceWidget', 'MultiChoiceWidget',
                        'PluginChoiceWidget'}

    def __init__(self) -> None:
        self._paramwidget_classes = {
            Overlord.params.String:      self.StringWidget,
            Overlord.params.Int:         self.IntWidget,
            Overlord.params.Float:       self.FloatWidget,
            Overlord.params.Choice:      self.ChoiceWidget,
            Overlord.params.MultiChoice: self.MultiChoiceWidget,
            Overlord.params.Plugin:      self.PluginChoiceWidget,
            }

    # to be refined in subclasses

    class BitmapImage:
        """An interface for bitmap image manipulation by the UI toolkit."""
        def __init__(self, width: int, height: int):
            raise NotImplementedError()
        @property
        def _img(self) -> object:
            raise NotImplementedError()
        @property
        def width(self) -> int:
            raise NotImplementedError()
        @property
        def height(self) -> int:
            raise NotImplementedError()
        def draw_sized_text(self, x: int, y: int, width: int, height: int, text: str, **attrs) -> None:
            raise NotImplementedError()
        def fill_circle(self, center_x: int, center_y: int, radius: int, **attrs) -> None:
            raise NotImplementedError()

    VectorImage: TypeAlias = VectorImage
    SolidFillPattern: TypeAlias = SolidFillPattern

    # those classes, referenced below, that have to be further
    # specialized for concrete toolkits
    _UnimplementedWidgetClasses: tuple[type[ParamWidget], ...] = (ParamWidget,
                                                                  PluginChoiceWidget)

    RawLabelWidget: type = object # would be `= type` if Gtk classes were indeed types
    LabelWidget: TypeAlias = LabelWidget
    StringWidget: TypeAlias = ParamWidget
    IntWidget: TypeAlias = ParamWidget
    FloatWidget: TypeAlias = ParamWidget
    ChoiceWidget: TypeAlias = ChoiceWidget
    MultiChoiceWidget: TypeAlias = ParamWidget
    PluginChoiceWidget: TypeAlias = PluginChoiceWidget

    # generic GUI elements

    def notify(self, title: str, text: str) -> None:
        raise NotImplementedError()

    def askuser_yesno(self, title: str, text: str) -> None:
        raise NotImplementedError()

    def select_load_file(self, callback: Callable[[str], None]) -> None:
        raise NotImplementedError()

    def select_save_file(self, callback: Callable[[str], None]) -> None:
        raise NotImplementedError()

    #@overload
    #... synchronous: Literal[False]=False) -> object
    #@overload
    #... synchronous: Literal[True]=True) -> bool
    def open_params_dialog(self, title: str, params: dict[str, Overlord.Param], app_ui: App,
                           accept_cb: Optional[Callable[[dict[str, Overlord.Param]],None]]=None,
                           refuse_cb: Optional[Callable[[dict[str, Overlord.Param]],None]]=None,
                           parent_plugin: Optional[Overlord.Plugin]=None,
                           parent_window: Optional[object]=None,
                           destroy: bool=True, synchronous: bool=False) -> Union[object,bool]:
        raise NotImplementedError()

    def askuser_params(self, title: str, params: dict[str, Overlord.Param], app_ui: App,
                       parent_plugin: Optional[Overlord.Plugin]=None) -> None:
        """
        Present the user with a dialog allowing him to set parameters.
        """
        answer = self.open_params_dialog(title, params, app_ui, accept_cb=None,
                                         parent_plugin=parent_plugin,
                                         synchronous=True)
        if answer is False:
            raise UserCancel()

    def _create_widget(self, param: Overlord.Param, app_ui: App,
                       params: dict[str, Overlord.Param]) -> ParamWidget:
        try:
            cls = self._paramwidget_classes[type(param.decl)]
        except KeyError:
            # FIXME: can also be a wrong parameter decl (missing
            # mandatory attribute)
            raise Overlord.ParameterError("Parameter type '%s' not supported for %s" %
                                          (type(param.decl), pretty(param)))
        w: ParamWidget
        if cls in self._UnimplementedWidgetClasses:
            # pseudo-param to forge a label
            pseudo: Overlord.Param[None] = Overlord.Param(Overlord.ParamDecl(
                "(parameter type '%s' not supported)" %
                (type(param.decl),)))
            w = self.LabelWidget(pseudo, app_ui)
            pseudo.owner = w
        else:
            w = cls(param, app_ui)
            # update this widget when the param's dependencies change
            for dep_name, callback in param.decl.depends_on.items():
                assert dep_name in params, \
                       "%r not in %r" % (dep_name, params)
                def __hook(param: Overlord.Param, oldvalue: Optional[Overlord.ParamValue],
                           dependant_param: Overlord.Param[Overlord.ParamValue]) -> None:
                    callback(param, oldvalue, dependant_param)
                    w.update()
                params[dep_name].register_changed(__hook,
                                                  dependant_param=param)
        return w

    @staticmethod
    def _vbox_add(vbox: object, widget: object) -> None:
        raise NotImplementedError()
    @staticmethod
    def _widget_hide(widget: object) -> None:
        raise NotImplementedError()
    @staticmethod
    def _combo_setchanged_hook(combo: object, callback: Callable[[object,Overlord.Param],None],
                               param: Overlord.Param) -> None:
        raise NotImplementedError()

    def create_param_widget(self, param: Overlord.Param, app_ui: App,
                            params: dict[str,Overlord.Param]) -> tuple[object,ParamWidget]:
        raise NotImplementedError()

    ## main event loop

    def run(self) -> None:
        raise NotImplementedError()

    ## canvas user interaction

    # A GUI needs to implement at least one of those groups for user
    # interaction:

    # 1. file/stream input, timers

    class CallbackType(Enum):
        READ = 0
        ERROR = 1
        TIMER = 2

    _TStream: TypeAlias = object
    _TCallbackValue: TypeAlias = Any

    def set_stream_callback(self, cbtype: CallbackType, stream: _TStream,
                            callback: Callable[[_TStream,_TCallbackValue],None],
                            arg: _TCallbackValue=None) -> None:
        raise NotImplementedError()
    def unset_stream_callback(self, cbtype: CallbackType, stream: _TStream,
                              callback: Callable[[_TStream,_TCallbackValue],None]) -> None:
        raise NotImplementedError()

    def set_timer_callback(self, timeout: int, callback: Callable[[],None]) -> None:
        raise NotImplementedError()
    def unset_timer_callback(self, callback: Callable[[],None]) -> None:
        raise NotImplementedError()

    # 2. pointing-device input

    def set_mouse_release_callback(self, canvas: object,
                                   callback: Callable[[int,int],None]) -> None:
        raise NotImplementedError()
    def unset_mouse_release_callback(self, canvas: object,
                                     callback: Callable[[int,int],None]) -> None:
        raise NotImplementedError()

    def set_mouse_press_callback(self, canvas: object,
                                 callback: Callable[[int,int],None]) -> None:
        raise NotImplementedError()
    def unset_mouse_press_callback(self, canvas: object,
                                   callback: Callable[[int,int],None]) -> None:
        raise NotImplementedError()

    def set_mouse_move_callback(self, canvas: object,
                                callback: Callable[[int,int,int],None]) -> None:
        raise NotImplementedError()
    def unset_mouse_move_callback(self, canvas: object,
                                  callback: Callable[[int,int,int],None]) -> None:
        raise NotImplementedError()

    ## canvas drawing methods

    @staticmethod
    def context_rotate(drawing_context: DrawingContext, rotation: tuple[int,int,float]) -> None:
        raise NotImplementedError()
    @staticmethod
    def context_scale(drawing_context: DrawingContext,
                      factor: float, center: tuple[int,int]) -> None:
        raise NotImplementedError()

    def clear(self, drawing_context: DrawingContext) -> None:
        raise NotImplementedError()

    def color(self, canvas: object, colorname: str) -> object:
        raise NotImplementedError()

    def color_transparent(self, color: object, alpha: float) -> object:
        raise NotImplementedError()

    def draw_line(self, drawing_context: DrawingContext,
                  x0: float, y0: float, x1: float, y1: float, **attrs) -> None:
        raise NotImplementedError()
    def draw_rectangle(self, drawing_context: DrawingContext,
                       x: float, y: float, w: float, h: float, **attrs
                       ) -> None:
        raise NotImplementedError()
    def fill_rectangle(self, drawing_context: DrawingContext,
                       x: float, y: float, w: float, h: float, **attrs
                       ) -> None:
        raise NotImplementedError()
    def draw_rounded_rectangle(self, drawing_context: DrawingContext,
                               x: float, y: float, w: float, h: float,
                               radius: float, **attrs) -> None:
        raise NotImplementedError()
    def fill_rounded_rectangle(self, drawing_context: DrawingContext,
                               x: float, y: float, w: float, h: float,
                               radius: float, **attrs) -> None:
        raise NotImplementedError()
    def draw_circle(self, drawing_context: DrawingContext,
                    center_x: float, center_y: float, radius, **attrs
                    ) -> None:
        raise NotImplementedError()
    def fill_circle(self, drawing_context: DrawingContext,
                    center_x: float, center_y: float, radius, **attrs
                    ) -> None:
        raise NotImplementedError()
    def draw_polygon(self, drawing_context: DrawingContext,
                     pointlist: Sequence[tuple[float,float]], **attrs
                     ) -> None:
        raise NotImplementedError()
    def fill_polygon(self, drawing_context: DrawingContext,
                     pointlist: Sequence[tuple[float,float]], **attrs
                     ) -> None:
        raise NotImplementedError()

    # FIXME: completely hackish, color must be str here
    def draw_centered_text(self, drawing_context: DrawingContext,
                           x: float, y: float, text: str, color: Optional[str]=None, bold: bool=False,
                           height: Optional[float]=None) -> None:
        raise NotImplementedError()

    def draw_sized_text(self, drawing_context: DrawingContext,
                        x: float, y: float, width: float, height: float,
                        text: str, **attrs) -> None:
        raise NotImplementedError()

    def draw_image(self, drawing_context: DrawingContext,
                   image: BitmapImage, x: float, y: float) -> None:
        raise NotImplementedError()

    #def image_from_svg(self, canvas, filename, width, height):
    #    image = self.BitmapImage(int(width), int(height))
    #    vimg = self.VectorImage(filename)
    #    vimg.render_to_bitmap(image, 0, 0, width, height)
    #
    #    return image
