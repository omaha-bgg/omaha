# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .game_renderer import GameRenderer
    from .piece_holder import Location
    from .moves import Move
    from .UI import DrawingContext

__all__ = [ 'LocationHighlighter', 'MoveHighlighter' ]

class LocationHighlighter:
    def __init__(self, game_renderer: GameRenderer):
        raise NotImplementedError()
    def highlight_location(self, drawing_context: DrawingContext, location: Location) -> None:
        raise NotImplementedError()

class MoveHighlighter:
    def __init__(self, game_renderer: GameRenderer):
        raise NotImplementedError()
    def highlight_move(self, drawing_context: DrawingContext, move: Move) -> None:
        raise NotImplementedError()
