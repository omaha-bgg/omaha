# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from typing import AnyStr, IO
import Overlord
from .game import Game, Match

__all__ = ['GameReader', 'GameWriter']

class GameReader:
    suffix: str

    def __init__(self) -> None:
        raise NotImplementedError()
    def parse(self, stream: IO[AnyStr]) -> Game:
        raise NotImplementedError()

    def game(self) -> Game:
        raise NotImplementedError()
    def game_plugin(self) -> Overlord.Plugin:
        raise NotImplementedError()

class GameWriter:
    def __init__(self, match: Match):
        raise NotImplementedError()

    def write_to(self, stream: IO[AnyStr]) -> None:
        raise NotImplementedError()
