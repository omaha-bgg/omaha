# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import logging
import sys
from typing import TYPE_CHECKING, Any, Iterable, Optional, Sequence, TypeVar

import Overlord
from Overlord.misc import classproperty

from .piece import Piece

if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

if TYPE_CHECKING:
    from .game import Game

__all__ = [ 'PieceHolder', 'HolderState', 'Location', 'LocationContents',
            'OnePieceTypePerLocHolderState', 'OnePiecePerLocHolderState',
            'OnePiecePerLocHolder' ]

logger = logging.getLogger(__name__)

class Location:
    """Base class for description of piece location within a holder.

    Description of the precise location within holder is left to
    the specific subclasses.

    Used as hierarchy root, but does not provide proper __init__
    chaining for multiple inheritance.
    """
    def __init__(self, holder: "PieceHolder"):
        assert isinstance(holder, PieceHolder)
        assert holder.Location is type(self), \
            "%s is not %s" % (holder.Location, type(self))
        self.__holder = holder
    @property
    def holder(self) -> "PieceHolder":
        return self.__holder

class LocationDesc:
    "Description of a given location, immutable during the whole game."
    def __init__(self) -> None:
        self.__available = True

    @property
    def available(self) -> bool:
        return self.__available
    def set_available(self, available: bool) -> None:
        self.__available = available

class LocationContents:
    "Contents of a given location, varying between GameState's"
    _TLocationContents = TypeVar("_TLocationContents", bound="LocationContents") # poor man's Self
    def __init__(self) -> None:
        super().__init__()
        self.__pieces: list[Piece] = []

    @property
    def pieces(self) -> tuple[Piece, ...]:
        return tuple(self.__pieces)
    def add_piece(self, piece: Piece) -> None:
        assert piece not in self.__pieces
        self.__pieces.append(piece)
    def remove_piece(self, piece: Piece) -> None:
        assert piece in self.__pieces
        self.__pieces.remove(piece)

    def __contains__(self, piece: Piece) -> bool:
        return piece in self.__pieces
    def __len__(self) -> int:
        return len(self.__pieces)

    def clone(self: _TLocationContents) -> _TLocationContents:
        c = type(self)()
        c.__pieces = list(self.__pieces)
        return c
    def freeze(self) -> None:
        self.add_piece = NotImplemented     # type: ignore[assignment]
        self.remove_piece = NotImplemented  # type: ignore[assignment]
    def frozen(self: _TLocationContents) -> _TLocationContents:
        c = self.clone()
        c.freeze()
        return c

class OnePieceTypePerLocContents(LocationContents):
    @property
    def piece(self) -> Optional[Piece]:
        pieces = self.pieces
        if pieces:
            return pieces[0]
        return None

class OnePiecePerLocContents(OnePieceTypePerLocContents):
    def add_piece(self, piece: Piece) -> None:
        assert not self.pieces
        super().add_piece(piece)

THolderState = TypeVar("THolderState", bound="HolderState") # poor man's Self

class HolderState(metaclass=Overlord.OuterClass):
    "State of a given holder in a game."

    __innerclasses__ = {'LocationContents'}
    LocationContents: TypeAlias = LocationContents

    def __init__(self, holder: "PieceHolder"):
        self.__holder = holder
    @property
    def holder(self) -> "PieceHolder":
        return self.__holder

    def clone(self: THolderState) -> THolderState:
        return type(self)(self.__holder)
    def frozen(self: THolderState) -> THolderState:
        c = self.clone()
        c.freeze()
        return c
    def freeze(self) -> None:
        self.put_piece = NotImplemented # type: ignore[assignment]
        self.take_piece = NotImplemented # type: ignore[assignment]
        self.clear = NotImplemented # type: ignore[assignment]

    def contents_at(self, location: Location) -> LocationContents:
        """Returns LocationContents for `location`.
        Throws IndexError if location coordinates are out of bounds."""
        raise NotImplementedError()
    def can_insert(self, location: Location) -> bool:
        """Returns whether `location` is valid for insertion."""
        assert location.holder is self.holder
        return True
    def take_piece(self, piece: Piece) -> None: # pylint: disable=method-hidden
        """Remove piece from location in holder and return it."""
        # FIXME: the holder implementation still has to do the real
        # removal, this only deals with the piece side of things and
        # should probably be a separate protected method
        if piece is None:
            return
        assert isinstance(piece, Piece), "%r is not a Piece" % (piece,)
        assert self.piece_location(piece), "%s is not in %s" % (piece, self)

    def put_piece(self, location: Location, piece: Piece) -> None: # pylint: disable=method-hidden
        """Put piece at given location in the holder."""
        assert self.can_insert(location), \
            "cannot put_piece at %s" % location

    def piece_location(self, piece: Piece) -> Location:
        "Return Location in `self` where `piece` can be found, or None."
        raise NotImplementedError("in %s" % self)

    def clear(self) -> None: # pylint: disable=method-hidden
        raise NotImplementedError()

    @property
    def all_locations(self) -> Iterable[Location]:
        raise NotImplementedError()

    def valid_location(self, location: Location) -> bool:
        """Returns whether the given location is usable to store a Piece.

        It is expected from concrete subclasses making upcalls that
        fetching `contents_at(location)` does not throw any exception.
        """
        if location.holder is not self.holder:
            return False
        if not self.holder.location_desc(location).available:
            return False
        return True

    @property
    def pieces(self) -> Sequence[Piece]:
        "The list of pieces in this PieceHolder."
        raise NotImplementedError()

# kind of hack to encompass PiecePool
class OnePieceTypePerLocHolderState(HolderState):
    LocationContents: TypeAlias = OnePieceTypePerLocContents
    def piece_at(self, location: Location) -> Optional[Piece]:
        """Return piece at location (may be None).
        Throws IndexError if location coordinates are out of bounds."""
        pieces = self.contents_at(location).pieces
        return pieces[0] if pieces else None

class OnePiecePerLocHolderState(OnePieceTypePerLocHolderState):
    LocationContents: TypeAlias = OnePiecePerLocContents
    def can_insert(self, location: Location) -> bool:
        if not super().can_insert(location):
            return False
        return self.piece_at(location) is None

class PieceHolder(Overlord.Parametrizable,
                  metaclass=Overlord.ParametrizableOuterclass):
    """
    Base class for piece-holding objects: boards, bags, ...

    Used as hierarchy root, but does not provide proper __init__
    chaining for multiple inheritance.
    """

    __innerclasses__ = {'Location', 'LocationDesc', 'State'}

    # the default, to be subclassed by most PieceHolder subclasses
    Location: TypeAlias = Location
    LocationDesc: TypeAlias = LocationDesc
    State: TypeAlias = HolderState

    def __init__(self, game: "Game", **kwargs: Any):
        super().__init__(**kwargs)
        self.__game = game

    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return PieceHolder

    @property
    def game(self) -> "Game":
        return self.__game

    def location_desc(self, location: Location) -> LocationDesc:
        raise NotImplementedError()

    def clone(self) -> "PieceHolder":
        # prevent calling of Parametrizable.clone()
        raise SyntaxError("clone() is not supported in PieceHolder")

class OnePiecePerLocHolder(PieceHolder):
    "A PieceHolder with zero or one Piece in each Location"
    State: TypeAlias = OnePiecePerLocHolderState
