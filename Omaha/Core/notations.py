# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

import logging
from typing import TYPE_CHECKING, Any, Generic, TypeVar

import Overlord
from .piece import PieceState
from .piece_holder import Location
from .moves import Move
from .player import Player
if TYPE_CHECKING:
    from .game import Game, GameState

__all__ = [ 'PieceNotation', 'LocationNotation',
            'MoveNotation', 'PositionNotation',
           ]

logger = logging.getLogger(__name__)

# core classes

class Notation(metaclass=Overlord.OuterClass):
    def __init__(self, game: Game):
        self.game = game

class PieceNotation(Notation):
    def piece_name(self, piecestate: PieceState) -> str:
        raise NotImplementedError()
    def piece_type(self, name: str) -> Any:
        raise NotImplementedError()

TLocation = TypeVar("TLocation", bound=Location)

class LocationNotation(Notation, Generic[TLocation]):
    def location_name(self, location: TLocation) -> str:
        raise NotImplementedError()
    def location(self, string: str) -> TLocation:
        raise NotImplementedError()
# FIXME: this is specific to holders featuring cols and rows,
#  ie. Euclidian2D boards, and that could turn into a limitation
#  some day
    def colname(self, location: TLocation) -> str:
        raise NotImplementedError()
    def rowname(self, location: TLocation) -> str:
        raise NotImplementedError()
    def col(self, colname: str) -> int:
        raise NotImplementedError()
    def row(self, rowname: str) -> int:
        raise NotImplementedError()

TMove = TypeVar("TMove", bound=Move)

class MoveNotation(Notation, Generic[TMove]):
    __innerclasses__ = {'LocationNotation', 'PieceNotation'}
    LocationNotation = LocationNotation
    # PieceNotation is not used by all MoveNotation's, but by enough
    # of them to keep it here
    PieceNotation = PieceNotation

    def __init__(self, game: Game):
        super().__init__(game)
        self.location_notation: LocationNotation  = self.LocationNotation(game)
        self.piece_notation = self.PieceNotation(game)
    def move_serialization(self, move: TMove) -> str:
        raise NotImplementedError()
    def move_deserialization(self, gamestate: GameState, string: str, player: Player) -> TMove:
        raise NotImplementedError()

class PositionNotation(Notation):
    "Notation for the full state of a game"
    __innerclasses__ = {'LocationNotation', 'PieceNotation'}
    LocationNotation = LocationNotation
    PieceNotation = PieceNotation
    def __init__(self, game: Game):
        super().__init__(game)
        self.location_notation: LocationNotation = self.LocationNotation(game)
        self.piece_notation = self.PieceNotation(game)
    def position_serialization(self, gamestate: GameState) -> str:
        raise NotImplementedError()
    def position_deserialization(self, string: str) -> GameState:
        raise NotImplementedError()
