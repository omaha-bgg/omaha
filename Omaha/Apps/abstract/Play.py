# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from collections import OrderedDict
import logging

import Overlord
from Overlord import plugin_pattern

from Omaha import Core

logger = logging.getLogger(__name__)

class _GameAction:
    def __init__(self, handle):
        self.handle = handle
        self.callbacks = []

class Play(Core.GameApp):

    def __init__(self, **kwargs):
        super(Play, self).__init__(**kwargs)
        self.window = None
        self.__history_notation = None
        self.__game_actions = OrderedDict()

    def _quit_hook(self):
        self.release_backrefs()
        self.disconnect_players()
        self.game = None
        self.game_renderer = None
        # FIXME: should cleanup click handlers and such ?

        self.window.destroy()
        self.window = None

    def _resign_hook(self):
        player = self.game.scratch_state.whose_turn
        if player not in self.local_players():
            self.tk.notify("error",
                           "Resign is only possible during your own turn")
        elif self.tk.askuser_yesno("%s resigns ?" % player.name):
            self.game.declare_resignation(player)

    ###

    def register_game_action(self, label, callback):
        if label not in self.__game_actions:
            button_handle = self._add_game_action(label, self.__dispatch_game_action(label))
            self.__game_actions[label] = _GameAction(button_handle)
        assert callback not in self.__game_actions[label].callbacks
        self.__game_actions[label].callbacks.append(callback)
        return (label, callback) # handle

    def unregister_game_action(self, game_action):
        label, callback = game_action
        self.__game_actions[label].callbacks.remove(callback)
        if not self.__game_actions[label].callbacks:
            self._remove_game_action(self.__game_actions[label].handle)
            del self.__game_actions[label]

    def __dispatch_game_action(self, label):
        def wrapped():
            assert label in self.__game_actions
            for callback in self.__game_actions[label].callbacks:
                handled = callback()
                if handled:
                    return
        wrapped.__name__ = "__dispatch_game_action for '{}'".format(label)
        return wrapped

    ###

    def _prefs_hook(self):
        params = self.get_params()

        # go ask
        self.tk.open_params_dialog(title="omaha - UI parameters",
                                   params=params, app_ui=self,
                                   accept_cb=self.set_params,
                                   parent_window=self.main_window)

    def set_params(self, params):
        super(Play, self).set_params(params)
        # redraw with new parameters, if the canvas is here already
        if hasattr(self, "canvas"):
            self.tk.invalidate_canvas(self.canvas)
        # reformat move history - FIXME only do so if notation changed
        if self._history_size() != 0:
            self._history_listener(self.game.moves, 0, self.game.moves.nmoves_done)

    def _saveas_hook(self):
        self.game.ui_suspend()
        self.tk.select_save_file(callback=self.save_game)

    def save_game(self, filename):
        if filename != None:
            super(Play, self).save_game(filename)
        self.game.ui_resume()

    ###

    def _undo_hook(self):
        if isinstance(self.game, Core.UndoableGame):
            try:
                self.game.undo_move()
                self.tk.invalidate_canvas(self.canvas)
            except Core.InvalidMove as ex:
                logger.warning("cannot undo: %s", ex)
        else:
            logger.info("undo not supported")

    def _redo_hook(self):
        try:
            self.game.redo_move()
            self.tk.invalidate_canvas(self.canvas)
        except IndexError:
            logger.info("nothing to redo")
        except Core.InvalidMove as ex:
            logger.warning("cannot redo: %s", ex)

    def _refuse_hook(self):
        try:
            # FIXME: should belong to PlayerDriver
            self.game.refuse_move(self.game.moves.last, self.game.scratch_state.whose_turn)
            self.tk.invalidate_canvas(self.canvas)
        except IndexError:
            logger.error("nothing to refuse")
        except Core.InvalidMove as ex:
            logger.warning("cannot refuse: %s", ex)

    ### move history

    def _history_append(self, movestr):
        raise NotImplementedError()

    def _history_size(self):
        raise NotImplementedError()

    def _history_delete(self, first, last):
        raise NotImplementedError()

    @Overlord.parameter
    def history_notation_class(cls, context):
        game_plugin = context.game_plugin
        if game_plugin.has_datafield("X-Omaha-DefaultNotation"):
            default_notation = game_plugin.datafield("X-Omaha-DefaultNotation")
        else:
            default_notation = None
        return Overlord.params.Plugin(
            label = 'Move notation for history panel',
            plugintype = 'X-Omaha-MoveNotation',
            context = context,
            pattern = plugin_pattern.ParentOf(
                'X-Omaha-Game-Categories', game_plugin.Class),
            default_by_name = default_notation,
            )

    @property
    def history_notation(self):
        notationcls = self.history_notation_class.Class
        # (re)instanciate notation if required
        if not isinstance(self.__history_notation, notationcls):
            self.__history_notation = notationcls(self.game)
        return self.__history_notation

    def _history_listener(self, movelist, oldpos, newpos):
        history_size = self._history_size()
        assert oldpos <= history_size, \
            "oldpos=%s, history size=%s" % (oldpos, history_size)
        assert newpos != oldpos
        # if we are overwriting, nuke old items first
        if oldpos < history_size:
            assert newpos > oldpos # overwriting and undo and incompatible
            self._history_delete(oldpos, history_size)
        if newpos > oldpos:
            movelist.for_each_played(
                lambda i, move: self._history_append(
                    self.game.history_formatmove(movelist, i, move, self.history_notation)),
                oldpos, newpos)
        else: # new < old, undo
            assert oldpos == history_size # cannot undo move without all following ones
            self._history_delete(newpos, oldpos)
