# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# FIXME: display outcome and disable game controls when Finished

from Omaha import Core
from ..abstract.Play import Play as AbstractPlay
import Overlord

from Omaha.Toolkits.Qt import Canvas, Qt as Toolkit

import logging
logger = logging.getLogger(__name__)

import Omaha.Toolkits.Qt
QtCore = Omaha.Toolkits.Qt.QtCore
QtGui = Omaha.Toolkits.Qt.QtGui
QtWidgets = Omaha.Toolkits.Qt.QtWidgets
QAction = Omaha.Toolkits.Qt.QAction

class QtPlay(AbstractPlay):
    def __init__(self, **kwargs):
        self.widgets = {}
        self.actions = {}
        self.__moveentry_callbacks = dict()
        super(QtPlay, self).__init__(**kwargs)

    def __add_toolbutton(self, label, icon=None, key=None, **kwargs):
        if icon:
            action = QAction(icon, label, self.toolbar, **kwargs)
        else:
            action = QAction(label, self.toolbar, **kwargs)
        self.toolbar.addAction(action)
        if key:
            self.actions[key] = action

    def _create_gui(self):
        self.window = QtWidgets.QMainWindow()
        game_name = self.context.game_plugin.name
        self.window.setWindowTitle("omaha - {0}".format(game_name))

        vbox = QtWidgets.QWidget()
        vboxlayout = QtWidgets.QVBoxLayout(vbox)
        self.window.setCentralWidget(vbox)

        # toolbar
        self.toolbar = QtWidgets.QToolBar("main toolbar", self.window)
        self.window.addToolBar(QtCore.Qt.TopToolBarArea, self.toolbar)
        # tool buttons
        self.__add_toolbutton("&Close", QtGui.QIcon.fromTheme("window-close"),
                              shortcut=QtGui.QKeySequence.Close,
                              triggered=self._quit_hook)
        self.__add_toolbutton("&Save", QtGui.QIcon.fromTheme("document-save"),
                              shortcut=QtGui.QKeySequence.Save,
                              triggered=self._saveas_hook)
        self.__add_toolbutton("Undo", QtGui.QIcon.fromTheme("edit-undo"),
                              key='undo', triggered=self._undo_hook)
        self.__add_toolbutton("Redo", QtGui.QIcon.fromTheme("edit-redo"),
                              key='redo', triggered=self._redo_hook)
        self.__add_toolbutton("Refuse", QtGui.QIcon.fromTheme("process-stop"),
                              key='refuse', triggered=self._refuse_hook)
        self.__add_toolbutton("Resign", QtGui.QIcon.fromTheme("edit-delete"),
                              triggered=self._resign_hook)
        self.__add_toolbutton("Preferences", QtGui.QIcon.fromTheme("preferences-other"),
                              triggered=self._prefs_hook)
        windows_toolbutton = QtWidgets.QToolButton(self.toolbar)
        windows_toolbutton.setText("Windows")
        windows_toolbutton.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        windows_menu = QtWidgets.QMenu("Windows", self.window)
        windows_toolbutton.setMenu(windows_menu)
        self.toolbar.addWidget(windows_toolbutton)

        # hbox for canvas and gameinfo
        hbox = QtWidgets.QWidget()
        hboxlayout = QtWidgets.QHBoxLayout(hbox)
        vboxlayout.addWidget(hbox, 1)

        # canvas
        self.canvas = Canvas(app_ui=self, default_size=QtCore.QSize(800, 600))
        self.canvas.setMinimumSize(100, 100)
        hboxlayout.addWidget(self.canvas, 1)
        if self.game.has_tooltip:
            self.canvas.set_tooltip_func(self.tooltip)

        # move history
        self.widgets['history'] = QtWidgets.QListWidget()
        movelist_dock = QtWidgets.QDockWidget(self.window)
        movelist_dock.setWindowTitle("Move History")
        movelist_dock.setWidget(self.widgets['history'])
        self.window.addDockWidget(QtCore.Qt.LeftDockWidgetArea, movelist_dock,
                                  QtCore.Qt.Vertical)

        if self.game.moves.nmoves_done:
            self._history_listener(self.game.moves, 0, self.game.moves.nmoves_done)
        self.game.moves.register_listener(self._history_listener)

        history_toggle = windows_menu.addAction("Move history")
        history_toggle.setCheckable(True)
        history_toggle.setChecked(True)
        history_toggle.toggled.connect(
            lambda checked: movelist_dock.setVisible(checked))
        movelist_dock.visibilityChanged.connect(
            lambda visible: history_toggle.setChecked(visible))

        # gameinfo panel if GameRenderer needs it
        if not self.game_renderer.handles_player_display:
            infopanel = QtWidgets.QWidget()
            infopanellayout = QtWidgets.QVBoxLayout(infopanel)
            turnindicator_group = QtWidgets.QButtonGroup(infopanel)
            for player in self.game.players:
                playerbox = QtWidgets.QWidget()
                playerboxlayout = QtWidgets.QHBoxLayout(playerbox)
                turnindicator = QtWidgets.QRadioButton(playerbox)
                turnindicator_group.addButton(turnindicator)
                turnindicator.setEnabled(False)
                playerboxlayout.addWidget(turnindicator)
                self.widgets['playerturn_' + player.name] = turnindicator

                genericname = QtWidgets.QLabel("<b><big>%s</big></b>" % player.name)
                playerboxlayout.addWidget(genericname)

                playername = QtWidgets.QLabel()
                playerboxlayout.addWidget(playername)
                self.widgets['playername_' + player.name] = playername

                drivertype = QtWidgets.QLabel()
                playerboxlayout.addWidget(drivertype)
                self.widgets['playerdrivertype_' + player.name] = drivertype

                infopanellayout.addWidget(playerbox)

                drv = self.match._player_drivers[player]
                drvcls_param = self.match._playerdrivers_classes[player]
                assert drvcls_param.value.Class is type(drv), \
                    "%s is not %s" % (drvcls_param.value.Class, type(drv))
                self.fill_from_playerdriver(player, drv)
            infopanellayout.addStretch(1)

            hboxlayout.addWidget(infopanel)

        # textfield for manually-enterered moves (not shown initially)
        self.moveentry_hbox = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout(self.moveentry_hbox)
        layout.addWidget(QtWidgets.QLabel("Move:"))
        self.move_entry = QtWidgets.QLineEdit()
        layout.addWidget(self.move_entry)
        vboxlayout.addWidget(self.moveentry_hbox, 0, QtCore.Qt.AlignBottom)
        self.moveentry_hbox.hide()

        self.window.show()

    def release_backrefs(self):
        self.game.moves.unregister_listener(self._history_listener)
        super(QtPlay, self).release_backrefs()

    def set_player_turn(self, player):
        if self.game_renderer.handles_player_display or not self.widgets:
            return              # UI not created when parameter is first set
        playerturn_radio = self.widgets['playerturn_' + player.name]
        playerturn_radio.setChecked(True)

    def set_playerdriver_type(self, player, driver_text):
        if self.game_renderer.handles_player_display or not self.widgets:
            return              # UI not created when parameter is first set
        drivertype_label = self.widgets['playerdrivertype_' + player.name]
        drivertype_label.setText(driver_text)

    def set_playerdriver_name(self, player, name):
        if self.game_renderer.handles_player_display or not self.widgets:
            return              # UI not created when parameter is first set
        playername_label = self.widgets['playername_' + player.name]
        playername_label.setText(name)

    def tooltip(self, pos):
        location = self.game_renderer.point_to_location(pos.x(), pos.y())
        return self.game_renderer.location_tooltip_text(self.game.scratch_state, location)

    @property
    def canvas_size(self):
        size = self.canvas.size()
        return (size.width(), size.height())

    def _add_game_action(self, label, callback):
        action = QAction(label, self.window, triggered=callback)
        self.toolbar.addAction(action)
        return action

    def _remove_game_action(self, game_action):
        self.toolbar.removeAction(game_action)

    @property
    def main_window(self):
        return self.window

    ###

    def _enable_undo(self):
        for btn_name in ('undo', 'redo', 'refuse'):
            self.actions[btn_name].setEnabled(True)
    def _disable_undo(self):
        for btn_name in ('undo', 'redo', 'refuse'):
            self.actions[btn_name].setEnabled(False)

    ###

    def ack_moveentry(self):
        # nothing to do here for Qt
        pass

    def set_manual_move_entry_callback(self, callback):
        def wrapped_callback():
            text = self.move_entry.text()
            if not text: return
            callback(text)
        self.move_entry.editingFinished.connect(wrapped_callback)
        self.__moveentry_callbacks[callback] = wrapped_callback
        self.moveentry_hbox.show()
    def unset_manual_move_entry_callback(self, callback):
        self.move_entry.editingFinished.disconnect(self.__moveentry_callbacks[callback])
        del self.__moveentry_callbacks[callback]
        if len(self.__moveentry_callbacks) == 0:
            self.moveentry_hbox.hide()
    def clear_manual_move_entry(self):
        self.move_entry.clear()

    ###

    def _history_append(self, movestr):
        self.widgets['history'].addItem(movestr)

    def _history_size(self):
        if 'history' not in self.widgets:
            return 0
        return self.widgets['history'].count()

    def _history_delete(self, first, last):
        for i in reversed(range(first, last)):
            self.widgets['history'].takeItem(i)
