# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import os

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf

from Omaha.Toolkits.Gtk import Gtk as Toolkit
from ..abstract.Launcher import Launcher as AbstractLauncher
import Overlord

class Launcher(AbstractLauncher):

    def __init__(self, **kwargs):
        self.window = None
        self.widgets = {}
        super(Launcher, self).__init__(toolkit=Toolkit(), **kwargs)

    def _create_gui(self):
        """Create a launcher window.

        A game is selected through a Gtk.TreeView."""

        # The container window and vbox
        self.window = Gtk.Window()
        self.window.connect("destroy", lambda w: Gtk.main_quit())
        self.window.set_title("omaha - launcher")
        self.window.set_default_size(200, 200)

        vbox = Gtk.VBox()
        self.window.add(vbox)

        # Toolbar
        self.widgets['launcher_toolbar'] = Gtk.Toolbar()
        vbox.pack_start(
            self.widgets['launcher_toolbar'], expand=False, fill=True, padding=0)
        self.widgets['launcher_toolbar'].set_style(Gtk.ToolbarStyle.BOTH)

        # quit
        button = Gtk.ToolButton(stock_id=Gtk.STOCK_QUIT)
        button.connect("clicked", self._quit_hook)
        self.widgets['launcher_toolbar'].add(button)

        # load
        button = Gtk.ToolButton(stock_id=Gtk.STOCK_OPEN)
        button.connect("clicked", self._load_hook)
        self.widgets['launcher_toolbar'].add(button)

        # The TreeView game launcher itself
        model = Gtk.ListStore(str, GdkPixbuf.Pixbuf, str, object)
        self.widgets['chooser'] = Gtk.TreeView(model=model)
        self.widgets['chooser'].set_headers_visible(False)
        self.widgets['chooser'].append_column(
            Gtk.TreeViewColumn('icon', Gtk.CellRendererPixbuf(), pixbuf=1))
        self.widgets['chooser'].append_column(
            Gtk.TreeViewColumn('name', Gtk.CellRendererText(), text=0))
        self.widgets['chooser'].set_tooltip_column(2)
        vbox.pack_start(self.widgets['chooser'], expand=True, fill=True, padding=0)

        # this has to be a real list since the hook subscripts into it
        pluginlist = sorted(
            (x for x in self.plugin_manager.get_plugins_list('X-Omaha-Game')),
            key=lambda p: p.name)
        self.widgets['chooser'].connect("row-activated",
                                        self._iconview_hook, pluginlist)


        # bottom buttons bar
        self.widgets['actionsbar'] = Gtk.HBox()
        vbox.pack_start(self.widgets['actionsbar'], expand=True, fill=True, padding=0)

        button = Gtk.Button(label="Play")
        button.connect("clicked", self._play_button_hook)
        self.widgets['actionsbar'].add(button)

        button = Gtk.Button(label="Study")
        button.set_sensitive(False)
        #button.connect("clicked", self._quit_hook)
        self.widgets['actionsbar'].add(button)

        button = Gtk.Button(label="Scribe")
        button.set_sensitive(False)
        #button.connect("clicked", self._quit_hook)
        self.widgets['actionsbar'].add(button)


        # New-game "buttons" within the Iconview
        for plugin in pluginlist:
            if plugin.icon != '':
                if os.path.exists(plugin.icon):
                    pixbuf = GdkPixbuf.Pixbuf.new_from_file(plugin.icon)
                else:
                    pixbuf = self.widgets['chooser'].render_icon(
                        Gtk.STOCK_MISSING_IMAGE, Gtk.ICON_SIZE_BUTTON)
            else:
                pixbuf = Gtk.IconTheme.get_default().load_icon("document-new", size=32, flags=0)
            model.append([plugin.name, pixbuf, plugin.comment or plugin.name, plugin])

        self.window.show_all()

    def _play_button_hook(self, w):
        model, selected = self.widgets['chooser'].get_selection().get_selected_rows()
        if not selected:
            self.tk.notify("error", "No game selected")
            return
        assert len(selected) == 1
        name, icon, tooltip, plugin = model[selected[0]]
        self._run_app(plugin)

    @staticmethod
    def _quit_hook(w):
        Gtk.main_quit()

    def _load_hook(self, w):
        self._loadgame()

    def _iconview_hook(self, treeview, path, column, game_plugins=None):
        """Hook run when a game is double-clicked for run in treeview."""
        self._run_app(game_plugins[path[0]])

    def gameapp_class(self):
        from Omaha.Apps.Gtk.Play import GtkPlay
        return GtkPlay
