# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.NotationParam import NotationParamMoveComposer
from Omaha import Core
import sys
import logging

logger = logging.getLogger(__name__)

class StdinMoveComposer(NotationParamMoveComposer):
    class StdinListener:
        def __init__(self, gui):
            self.__gui = gui
            self.__player_drivers = []
        def add_playerdriver(self, drv):
            assert drv not in self.__player_drivers
            if not self.__player_drivers:
                logger.debug("%s: first client, registering to toolkit", self)
                self.__gui.tk.set_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                                  sys.stdin, self.__handle_data)
            self.__player_drivers.append(drv)
        def remove_playerdriver(self, drv):
            assert drv in self.__player_drivers
            self.__player_drivers.remove(drv)
            if not self.__player_drivers:
                logger.debug("%s: last client, deregistering from toolkit", self)
                self.__gui.tk.unset_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                                    sys.stdin, self.__handle_data)
        def __handle_data(self, stream, arg): # pylint: disable=unused-argument
            data = stream.readline()
            if data == '': # EOF
                logger.error("EOF on standard input")
                for drv in self.__player_drivers:
                    drv.disconnect()
                return

            line = data.strip()

            for drv in self.__player_drivers:
                if drv.player is self.__gui.game.scratch_state.whose_turn:
                    drv.handle_line(line)
                    return
            else:
                logger.warning("recieved data while no player listened, ignoring")

    stdin_listener = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__connected = False
        # instanciate the (pseudo)singleton listener
        if not StdinMoveComposer.stdin_listener:
            StdinMoveComposer.stdin_listener = StdinMoveComposer.StdinListener(self.gui)

    def connect_to_gui(self):
        self.stdin_listener.add_playerdriver(self)
        self.__connected = True

    def disconnect_from_gui(self):
        if not self.__connected:
            logger.info("... StdinMoveComposer was not connected")
            return
        self.__connected = False
        self.stdin_listener.remove_playerdriver(self)

    def handle_line(self, line):
        gamestate = self.gui.game.last_state
        try:
            move = self.notation.move_deserialization(gamestate, line, gamestate.whose_turn)
        except Core.ParseError as ex:
            logger.error("Syntax error: %s", ex)
            return

        # send move to GUI
        try:
            self.send_move_to_playerdriver(move)
        except Core.OmahaException as ex:
            logger.error("Move refused: %s", ex)
                # refresh display

        # refresh display
        self.gui.tk.invalidate_canvas(self.gui.canvas)
