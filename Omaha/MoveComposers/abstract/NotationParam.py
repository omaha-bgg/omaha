# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import Overlord
from Overlord import plugin_pattern
import logging

logger = logging.getLogger(__name__)

class NotationParamMoveComposer(Core.MoveComposer):
    "ABC for move composers with a user-changeable notation"

    def __init__(self, **kwargs):
        super(NotationParamMoveComposer, self).__init__(**kwargs)
        self.__notation = None

    @Overlord.parameter
    def notation_class(cls, context):
        game_plugin = context.game_plugin
        game_class = game_plugin.Class
        if game_plugin.has_datafield("X-Omaha-DefaultNotation"):
            default_notation = game_plugin.datafield("X-Omaha-DefaultNotation")
            logger.info("Using %s as requested by %s",
                        default_notation, game_plugin.name)
        else:
            default_notation = None
        return Overlord.params.Plugin(
            label = 'Move notation for %s' % game_plugin.name,
            plugintype = 'X-Omaha-MoveNotation',
            context = context,
            pattern = plugin_pattern.ParentOf(
                'X-Omaha-Game-Categories', game_class),
            default_by_name = default_notation
            )

    @property
    def notation(self):
        notationcls = self.notation_class.Class
        # (re)instanciate notation if required
        if not isinstance(self.__notation, notationcls):
            self.__notation = notationcls(self.gui.game)
        return self.__notation
