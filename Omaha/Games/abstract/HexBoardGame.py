# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import sys

from Omaha import Core
from . import Euclidian2D
from . import GenericChesslike
from .SingleBoardGame import OnePiecePerLocSingleBoardGame, PlayerAreasGame
from Omaha.Holders.abstract.Euclidian2D import Xform, Vector
from Omaha.Holders.HexBoard import HexBoard, OnePiecePerLocHexBoard

from string import ascii_lowercase

if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

# FIXME does not fit well in existing framework
class HexBentlinesNotation(GenericChesslike.ChessLikeCoordinateNotation.LocationNotation):
    '''"Bent-lines" notation for hexagonal boards, not using 'j'.

    Notation making use of as many "bent lines" of matrix_width
    length as possible, and uses shorter lines for the rest'''

    locname = "hexagonal bent-lines chesslike coordinates"

    def location_from_match(self, match):
        col = self.col(match.group(1))
        row = self._bentlines_row_number(match.group(2), col)
        return self.game.board.Location(self.game.board, col, row)

    def colname(self, loc):
        if loc.col >= 9:
            col = loc.col + 1           # 'j' is not used
        else:
            col = loc.col
        return ascii_lowercase[col]
    def rowname(self, loc):
        # base calculation, correct for the right (horizontal) part of
        # the board
        row = loc.row + 1
        if loc.col > (self.game.board.matrix_width - self.game.boardwidth0):
            # slant correction
            row -= loc.col - (self.game.board.matrix_height - self.game.boardwidth1)
        return "%s" % row

    def col(self, colname):
        if colname < "j":
            return ascii_lowercase.index(colname)
        else:
            return ascii_lowercase.index(colname) - 1
    def _bentlines_row_number(self, string, col):
        row = int(string) - 1
        if col > (self.game.board.matrix_width - self.game.boardwidth0):
            row += col - (self.game.board.matrix_height - self.game.boardwidth1)
        return row

class HexChessLikeBentlinesCoordinateNotation(GenericChesslike.ChessLikeCoordinateNotation):
    movename = "hexagonal bent-lines chesslike coordinates move"
    LocationNotation = HexBentlinesNotation


class BaseHexBoardGame(Euclidian2D.Game):
    Board: TypeAlias = HexBoard
    DefaultMoveNotation = HexChessLikeBentlinesCoordinateNotation

    boardwidth0: int
    boardwidth1: int
    boardwidth2: int

    def create_holders(self):
        super().create_holders()
        self._holders['board'] = self.Board(game=self, parentcontext=self.context,
                                            width0=self.boardwidth0, width1=self.boardwidth1,
                                            width2=self.boardwidth2)

    def setup_players(self):
        super().setup_players()


class OnePiecePerLocHexBoardGame(BaseHexBoardGame, OnePiecePerLocSingleBoardGame):
    Board: TypeAlias = OnePiecePerLocHexBoard


class HexBoard2PCornerGame(BaseHexBoardGame, PlayerAreasGame):
    # pylint: disable=abstract-method

    def player_orientation(self, player: Core.Player) -> tuple[int,int]:
        res = self.player_to_board_rot(player) * Vector.make_vector(0, +1)
        return (res.x, res.y)

    @property
    def player_to_board_rotations(self):
        return (Xform.from_22array( ((1,  0),
                                     (0,  1)) ),
                Xform.from_22array( ((-1, 0),
                                     (0, -1)) ),
        )
    @property
    def board_to_player_rotations(self):
        # 2-player is fully symmetric
        return self.player_to_board_rotations

    @property
    def player_to_board_xlats(self):
        return (Xform.make_translation(0, 0),
                Xform.make_translation(self.boardwidth0 + self.boardwidth2 - 2,
                                       self.boardwidth1 + self.boardwidth2 - 2),
        )
    @property
    def board_to_player_xlats(self):
        return (Xform.make_translation(0, 0),
                Xform.make_translation(-(self.boardwidth0 + self.boardwidth2 - 2),
                                       -(self.boardwidth1 + self.boardwidth2 - 2)),
        )

    def player_relative_pos(self, player, col, row):
        v = self.player_to_board_pos(Vector.make_vector(col, row), player)
        assert isinstance(v, Vector)
        return (v.x, v.y)

    def player_relative_location(self, player, col, row):
        coords = self.player_relative_pos(player, col, row)
        return self.board.Location(self.board, *coords)
