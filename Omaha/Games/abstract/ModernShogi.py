# -*- coding: utf-8 -*-

# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .ShogiGame import ShogiPiece, ShogiGame
from ..piecetypes.shogi import ShogiFacetype
from .. import movelib
import logging

logger = logging.getLogger(__name__)

class Piece(ShogiPiece):
    # pylint: disable=abstract-method
    "Pieces used in Modern Shogi 9x9"
    MOVETYPES = {
        ShogiFacetype.KING: movelib.betza("K"),
        ShogiFacetype.GOLD: movelib.betza("WfF"),
        ShogiFacetype.SILVER: movelib.betza("FfW"),
        ShogiFacetype.KNIGHT: movelib.betza("fN"),
        ShogiFacetype.LANCE: movelib.betza("fR"),
        ShogiFacetype.BISHOP: movelib.betza("B"),
        ShogiFacetype.ROOK: movelib.betza("R"),
        ShogiFacetype.PAWN: movelib.betza("fW"),

        ShogiFacetype.NARIGIN: movelib.betza("WfF"),
        ShogiFacetype.NARIKEI: movelib.betza("WfF"),
        ShogiFacetype.NARIKYO: movelib.betza("WfF"),
        ShogiFacetype.DRAGONHORSE: movelib.betza("BW"),
        ShogiFacetype.DRAGONKING: movelib.betza("RF"),
        ShogiFacetype.TOKIN: movelib.betza("WfF"),
    }

    PROMOTIONS = {
        ShogiFacetype.KING: None,
        ShogiFacetype.GOLD: None,
        ShogiFacetype.SILVER: ShogiFacetype.NARIGIN,
        ShogiFacetype.KNIGHT: ShogiFacetype.NARIKEI,
        ShogiFacetype.LANCE: ShogiFacetype.NARIKYO,
        ShogiFacetype.BISHOP: ShogiFacetype.DRAGONHORSE,
        ShogiFacetype.ROOK: ShogiFacetype.DRAGONKING,
        ShogiFacetype.PAWN: ShogiFacetype.TOKIN,
    }

class Game(ShogiGame):
    "Game using ModernShogi.Piece"
    Piece = Piece
