# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import re
import sys

from Omaha import Core
from .SingleBoardGame import SingleBoardGame
from ...Holders.abstract.Euclidian2D import Euclidian2D as Euclidian2DBoard

if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

class Game(SingleBoardGame):
    Board: TypeAlias = Euclidian2DBoard
    board: Board

class CoordinateNotation(Core.LocationNotation):
    # pylint: disable=abstract-method
    game: Game
    locname = "single-board 2D coordinates location"
    FORMAT = "{column}{row}"
    LOCATION_REGEXP = r"(?P<column>[a-w]+)(?P<row>\d+)"
    def location_name(self, location: Core.Location) -> str:
        assert isinstance(location, Core.Location), f"{location} is not a Location"
        assert location.holder is self.game.board, (f"holder for {location} is {location.holder!r},"
                                                    f" not {self.game.board!r}")
        return self.FORMAT.format(column=self.colname(location),
                                  row=self.rowname(location))

    def location_from_match(self, match: re.Match[str]) -> Core.Location:
        return self.game.board.Location(self.game.board,
                                        self.col(match.group("column")),
                                        self.row(match.group("row")))

    def location(self, string: str) -> Core.Location:
        # only match exact string, don't leave any trailing chars behind
        m = re.match(self.LOCATION_REGEXP + "$", string)
        if m:
            return self.location_from_match(m)
        raise Core.ParseError(f"could not parse '{string}' as a {self.locname}")
