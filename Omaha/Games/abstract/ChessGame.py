# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from . import Euclidian2D, GenericChesslike
from .SingleBoardGame import OnePiecePerLocSingleBoardGame
from .RectBoardGame import RectBoardGame, OnePiecePerLocRectBoardGame, LetterColumnLTRNotation, NumberRowBTTNotation
from .HexBoardGame import OnePiecePerLocHexBoardGame
from ..piecetypes.chess import ChessFacetype
from Omaha import Core
from Omaha.Core import InvalidMove
import re, logging
from typing import ClassVar

logger = logging.getLogger(__name__)

class Capture(GenericChesslike.Game.Move.Capture):
    def __init__(self, piece):
        super().__init__(piece)
        self.location = None

class ChessMove(GenericChesslike.Game.Move):
    Capture = Capture
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.promotion = None      # face after promotion if promoting a pawn
        self.side_effect = None    # eg. move of castling rook
        self.piece_had_moved = None

    def assert_locations(self, holders):
        super().assert_locations(holders)
        if self.capture is not None:
            assert self.capture.location.holder in holders

    def store_captured_piece(self, piecestate):
        # store piece captured by normal capture, en-passant captures
        # are set directly by the game
        super().store_captured_piece(piecestate)
        self.capture.location = self.target

    def __str__(self):
        result = super().__str__()
        if self.capture:
            result += " [x%s]" % self.capture.piece.type
        return result

class PieceState(Core.FacetedPieceState):
    def __init__(self, piece):
        super().__init__(piece)
        self.__has_moved = False
        self.__promoted = None # replaced with face on promotion
    def clone(self):
        c = super().clone()
        c.__has_moved = self.__has_moved
        c.__promoted = self.__promoted
        return c
    def freeze(self):
        super().freeze()
        self.set_has_moved = NotImplemented
        self.promote = NotImplemented
        self.unpromote = NotImplemented

    @property
    def face(self):
        return self.__promoted or self.piece.type
    def promote(self, face): # pylint: disable=method-hidden
        "Promote to arbitrary face/piece type"
        assert not self.__promoted
        self.__promoted = face
    def unpromote(self): # pylint: disable=method-hidden
        assert self.__promoted
        self.__promoted = None
    @property
    def has_moved(self):
        return self.__has_moved
    def set_has_moved(self, has_moved): # pylint: disable=method-hidden
        self.__has_moved = has_moved

class ChessPiece(GenericChesslike.Game.Piece):
    @property
    def has_moved(self):
        raise SyntaxError("obsolete interface moved to PieceState")

class GameState(Core.OnePieceTypePerLocGameState, Core.AlternatingTurnsGame.State):
    PieceState = PieceState

class EnglishChessNotation(Core.PieceNotation):
    PIECE_TEXT = {
        ChessFacetype.KING   : "K",
        ChessFacetype.QUEEN  : "Q",
        ChessFacetype.BISHOP : "B",
        ChessFacetype.KNIGHT : "N",
        ChessFacetype.ROOK   : "R",
        ChessFacetype.PAWN   : "P"
    }
    def piece_name(self, piecestate):
        return self.PIECE_TEXT[piecestate.face]
    def piecename_bytype(self, typ):
        return self.PIECE_TEXT[typ]
    def piece_type(self, name):
        for typ, typname in self.PIECE_TEXT.items():
            if typname == name:
                return typ
        else:
            raise LookupError("Invalid piece type '%s'" % name)

class ChessCoordinateNotation(GenericChesslike.ChessLikeCoordinateNotation):
    movename = "chess coordinates move"
    PieceNotation = EnglishChessNotation
    PROMOTION_NOTATIONS = dict(q=ChessFacetype.QUEEN,
                               n=ChessFacetype.KNIGHT,
                               r=ChessFacetype.ROOK,
                               b=ChessFacetype.BISHOP)

    def move_serialization(self, move):
        if move.promotion:
            assert not isinstance(move.promotion, ChessPiece) # catch old idiom
            promotype = move.promotion
            letters = [ letter
                        for letter, piecetype in self.PROMOTION_NOTATIONS.items()
                        if piecetype == promotype ]
            assert len(letters) == 1, \
                "Cannot express promotion to %r" % promotype
            promotion = letters[0]
        else:
            promotion = ""
        return super().move_serialization(move) + promotion

    regexp = (r"(?:.?)" +
              GenericChesslike.ChessLikeCoordinateNotation.regexp +
              r"([a-w]?)")
    def move_from_match(self, gamestate, match, player):
        move = super().move_from_match(gamestate, match, player)
        if len(match.group(3)) == 0:
            move.promotion = None
        else:
            move.promotion = self.PROMOTION_NOTATIONS[match.group(3)]
        return move

CASTLING_KINGSIDE  = "o-o"
CASTLING_QUEENSIDE = "o-o-o"

class SANNotation(Core.MoveNotation):
    PieceNotation = EnglishChessNotation
    class LocationNotation(Euclidian2D.CoordinateNotation,
                           LetterColumnLTRNotation, NumberRowBTTNotation):
        "LocationNotation for ChessGame.SANNotation"
        game: RectBoardGame

    movename = "chess SAN move"
    SPECIALMOVE_TEXT = {
        CASTLING_KINGSIDE  : "O-O",
        CASTLING_QUEENSIDE : "O-O-O",
        }

    def move_serialization(self, move):
        # special cases
        if move.type in self.SPECIALMOVE_TEXT:
            return self.SPECIALMOVE_TEXT[move.type]
        if move.gamestate.piece_state(move.piece).face is ChessFacetype.PAWN:
            if move.capture is not None:
                s = "%sx%s" % (self.location_notation.colname(move.source),
                               self.location_notation.location_name(move.target))
            else:
                s = self.location_notation.location_name(move.target)
            # promotion
            if move.promotion:
                assert not isinstance(move.promotion, Core.Piece) # catch old idiom
                promotype = move.promotion
                return s + "=" + self.piece_notation.piecename_bytype(promotype)
            return s

        # FIXME: spec requires emitting a minimized source
        return (self.piece_notation.piece_name(move.gamestate.piece_state(move.piece))
                + self.location_notation.location_name(move.source)
                + ("x" if move.capture is not None else "")
                + self.location_notation.location_name(move.target))

    regexp = r"([A-Z]?)([a-w]*)(\d*)(x?)([a-w]+)(\d+)((?:=[A-Z])?)(?:[#\+]?)"
    def move_deserialization(self, gamestate, string, player):
        assert gamestate.is_frozen
        assert player is gamestate.whose_turn
        if string.startswith('O-O'):
            move = self.game.Move(gamestate=gamestate, player=player)
            if string in ('O-O', 'O-O+'):
                move.type = CASTLING_KINGSIDE
            elif string in ('O-O-O', 'O-O-O+'):
                move.type = CASTLING_QUEENSIDE
            else:
                raise Core.ParseError("Could not parse %r as a move" % (string,))

            self.game.supplement_and_check_move(gamestate,
                                                gamestate.whose_turn, move, anticipate=True)
            return move

        source_col = source_row = None
        match = re.match(self.regexp + "$", string)
        if not match:
            raise Core.ParseError("Could not parse %r as a move" % (string,))

        if match.group(1):
            piece_type = self.piece_notation.piece_type(match.group(1))
        else:
            piece_type = ChessFacetype.PAWN

        if match.group(2):
            source_col = self.location_notation.col(match.group(2))
        if match.group(3):
            source_row = self.location_notation.row(match.group(3))

        target = self.game.board.Location(self.game.board,
                                          self.location_notation.col(match.group(5)),
                                          self.location_notation.row(match.group(6)))

        try:
            target_piece = gamestate.piece_at(target)
        except IndexError:
            raise Core.ParseError("Move %r refers to non-existent square" % (string,))
        if match.group(4) and target_piece is None:
            raise Core.ParseError("Move to empty %s cannot be a capture" % (target,))

        if len(match.group(7)) == 0:
            promotes = None
        else:
            promotes = self.piece_notation.piece_type(match.group(7)[1])

        candidates = []
        for piece in gamestate.pieces:
            if player and gamestate.piece_owner(piece) is not player:
                continue
            if gamestate.piece_state(piece).face is not piece_type:
                continue
            loc = gamestate.piece_location(piece)
            if loc is None:     # not on board, probably captured
                continue
            if source_col is not None and source_col != loc.col:
                continue
            if source_row is not None and source_row != loc.row:
                continue
            #
            move = self.game.Move(gamestate=gamestate, player=player, source=loc, target=target)
            move.promotion = promotes
            #
            if piece_type is ChessFacetype.PAWN:
                try:
                    target_piece = gamestate.piece_at(target)
                except IndexError:
                    raise Core.ParseError("Move %r refers to non-existent square" % (string,))
                try:
                    self.game.check_standard_moves(gamestate, player, move, anticipate=True)
                except InvalidMove:
                    continue
            #
            assert player is gamestate.whose_turn
            try:
                self.game.supplement_and_check_move(gamestate,
                                                    gamestate.whose_turn, move, anticipate=True)
            except InvalidMove as ex:
                logger.debug("%s not suitable for %s: %s", loc, string, ex.message)
                continue
            candidates.append(move)
        if len(candidates) == 0:
            # too detailed ...
            raise Core.ParseError("Found no suitable %s for %s" % (piece_type, string))
        if len(candidates) > 1:
            raise Core.ParseError("Could not disambiguate %s between: %s" %
                                  (string, ' or '.join(str(c.source) for c in candidates)))
        move = candidates[0]

        return move

class ChessGame(OnePiecePerLocSingleBoardGame, GenericChesslike.Game):
    # pylint: disable=abstract-method
    COLOR_WHITE = "white"
    COLOR_BLACK = "black"

    CASTLING_KINGSIDE  = CASTLING_KINGSIDE
    CASTLING_QUEENSIDE = CASTLING_QUEENSIDE
    ENPASSANT          = "enpassant"
    STARTUP_BOOST      = "startup boost"

    Move = ChessMove
    Piece = ChessPiece
    State = GameState
    DefaultMoveNotation = ChessCoordinateNotation

    player_names: ClassVar[tuple[str, ...]] = ("White", "Black")

    # no handicap supported yet
    handicap = ()

    def setup_players(self):
        super().setup_players()
        self.players[0].color = self.COLOR_WHITE
        self.players[1].color = self.COLOR_BLACK

    def do_move(self, gamestate, player, move):
        assert isinstance(move, ChessMove)
        super().do_move(gamestate, player, move)

        move.piece_had_moved = gamestate.piece_state(move.piece).has_moved
        gamestate.piece_state(move.piece).set_has_moved(True)

        # promotion
        if move.promotion:
            assert not isinstance(move.promotion, self.Piece)
            gamestate.piece_state(move.piece).promote(move.promotion)

        # put piece at target location
        gamestate.put_piece(move.target, move.piece)
        assert gamestate.piece_at(move.target) is move.piece

        if move.side_effect is not None:
            self.do_move(gamestate, player, move.side_effect)

    def _undo_capture(self, move):
        self.scratch_state.put_piece(move.capture.location, move.capture.piece)

    def _undo_move(self, move):
        # undo side-effects first
        if move.side_effect is not None:
            self._undo_move(move.side_effect)

        # demote promoting move before moving pawn back
        if move.promotion:
            self.scratch_state.piece_state(move.piece).unpromote()

        self.scratch_state.piece_state(move.piece).set_has_moved(move.piece_had_moved)

        super()._undo_move(move)

Core.UndoableGame.register(ChessGame)

class RectboardChessGame(ChessGame, # type: ignore[misc] # FIXME mypy#14279
                         OnePiecePerLocRectBoardGame):
    pass

class HexboardChessGame(ChessGame, # type: ignore[misc] # FIXME mypy#14279
                        OnePiecePerLocHexBoardGame):
    pass
