# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Holders.Pool import PiecePool
from Overlord.misc import classproperty
from Omaha import Core

class PlayerPools(Core.Game):
    # pylint: disable=abstract-method

    def _player_poolname(self, player):
        return 'pool-%s' % player.name

    def create_holders(self):
        super().create_holders()
        for player in self.players:
            pool = PiecePool(game=self, parentcontext=self.context,
                             piecegroup_func=self.pool_group)
            self._holders[self._player_poolname(player)] = pool

    def pool_group(self, piece):
        "Group pieces by type by default."
        return piece.type

    def pool_of(self, player):
        return self._holders[self._player_poolname(player)]

    @classproperty
    @classmethod
    def holder_classes(cls) -> tuple[type[Core.PieceHolder], ...]:
        holder_classes = super().holder_classes
        return holder_classes + (PiecePool,)

    def piece_location_by_type_inplayerpool(self, gamestate, piece_type, player):
        holder_state = gamestate.holder_state(self.pool_of(player))
        return holder_state.piece_location_by_group(piece_type)
