# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract import Euclidian2D
from .abstract.RectBoardGame import (RectBoardGame, OnePiecePerLocRectBoardGame,
                                     RectBoard2PSideGame,
                                     LetterColumnLTRNotation, NumberRowBTTNotation)
from .abstract.SingleBoardGame import OnlySingleBoardGame
from Omaha.Games.Chess import Chess
from Omaha.Core.misc import sign
from Omaha.Core import InvalidMove
from Omaha import Core
import Overlord

import logging

logger = logging.getLogger(__name__)

class CheckersMove(Core.WaypointDisplaceMove):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.captured_locs = [] # locations of captured pieces

# non-standard notation closer to the chess one
class CheckersCoordsNotation(Core.MoveNotation):
    class LocationNotation(Euclidian2D.CoordinateNotation,
                           LetterColumnLTRNotation,
                           NumberRowBTTNotation):
        "LocationNotation for CheckersCoordsNotation"
        game: RectBoardGame

    def move_serialization(self, move):
        result = self.location_notation.location_name(move.source)
        for step in move.waypoints:
            result += "%s" % self.location_notation.location_name(step)
        return result
    # FIXME: no move_deserialization yet

class PieceState(Core.FacetedPieceState):
    def __init__(self, piece):
        super().__init__(piece)
        self.__promoted = False
    @property
    def face(self):
        if self.__promoted:
            return Checkers.PIECETYPE_KING
        else:
            return Checkers.PIECETYPE_MAN
    @property
    def promoted(self):
        return self.__promoted
    def promote(self): # pylint: disable=method-hidden
        self.__promoted = True
    def unpromote(self): # pylint: disable=method-hidden
        self.__promoted = False
    def clone(self):
        c = super().clone()
        c.__promoted = self.__promoted
        return c
    def freeze(self):
        super().freeze()
        self.promote = NotImplemented
        self.unpromote = NotImplemented

class GameState(Core.OnePieceTypePerLocGameState, Core.AlternatingTurnsGame.State):
    PieceState = PieceState

class Checkers(RectBoard2PSideGame, # type: ignore[misc] # FIXME mypy#14279
               OnlySingleBoardGame, Core.AlternatingTurnsGame, OnePiecePerLocRectBoardGame):
    """Checkers game, implementing only international rules for now."""

    PIECETYPE_MAN  = "man"
    PIECETYPE_KING = "king"

    Move = CheckersMove
    State = GameState
    DefaultMoveNotation = CheckersCoordsNotation

    player_names = ("White", "Black")

    @Overlord.parameter
    def boardsize(cls, context):
        return Overlord.params.Int(label = 'Board size',
                                   default = 10,
                                   minval = 4, maxval = 20)
    @Overlord.parameter
    def rowcount(cls, context):
        # FIXME: instead of rowcount.max, fix INT in GUI.Gtk
        return Overlord.params.Int(label = 'Number of pawn rows',
                                   default = 4,
                                   minval = 1, maxval = 9,
                                   depends_on = dict(
                                       boardsize=cls.__update_rowcount))

    @staticmethod
    def __max_rowcount(height):
        return height // 2 - 1

    @staticmethod
    def __update_rowcount(param,            # boardsize
                          oldvalue,
                          dependant_param): # rowcount
        dependant_param.decl.max = Checkers.__max_rowcount(param.value)

    def set_params(self, params):
        super().set_params(params)

        # __check_parameters__
        if not hasattr(self, 'rowcount') or not hasattr(self, 'boardsize'):
            # not all involved params have been set yet, nothing to check
            return
        if self.rowcount > Checkers.__max_rowcount(self.boardsize):
            raise Overlord.ParameterError(
                "Row count ({0.rowcount}) must be less"
                " than half of board size ({0.boardsize})".format(self))
        # propagate
        self.boardwidth, self.boardheight = self.boardsize, self.boardsize

    def setup_players(self):
        super().setup_players()
        self.players[0].color = Chess.COLOR_WHITE
        self.players[1].color = Chess.COLOR_BLACK

    def set_start_position(self):
        for player in self.players:
            for row in range(0, self.rowcount):
                for col in range(row%2, self.boardwidth, 2):
                    self.new_piece_on_board(
                        self.scratch_state,
                        self.player_relative_pos(player, col, row),
                        player, None)


    def supplement_and_check_move(self, gamestate, player, move, anticipate):
        super().supplement_and_check_move(gamestate, player, move, anticipate)

        if move.source is None:
            raise InvalidMove("move must have a source location")
        if move.target is None:
            # FIXME: should be IncompleteMove ?
            raise InvalidMove("move must have a target location")

        # source location must have a piece to move
        piece = gamestate.piece_at(move.source)
        if piece is None:
            raise InvalidMove("no piece to move")

        piece_owner = gamestate.piece_owner(piece)
        assert piece_owner in self.players, \
            "%s has bad owner, expecting one of %s" % (piece, self.players)
        if piece_owner is not player:
            raise InvalidMove("player %s can only move his own pieces, not %s" %
                              (player, gamestate.piece_description(move.piece)))

        more_steps_allowed = True
        last_step_captured = False
        for step_src, step_dst in move.steps:
            if not more_steps_allowed:
                raise InvalidMove("No more steps allowed")

            delta_x = step_dst.col - step_src.col
            delta_y = step_dst.row - step_src.row

            if abs(delta_x) != abs(delta_y):
                raise InvalidMove("Invalid move")

            # stepped location must be free
            if gamestate.piece_at(step_dst) is not None:
                raise InvalidMove("stepped location is occupied")

            piece_face = gamestate.piece_state(piece)

            if piece_face is self.PIECETYPE_MAN:
                if abs(delta_x) > 2:
                    raise InvalidMove("Too far for a man")

                if  (abs(delta_y) == 1 and
                     sign(delta_y) != sign(self.player_orientation(piece_owner)[1])):
                    raise InvalidMove("Wrong direction")

            if abs(delta_x) >= 2:
                jumped_over = self.board.Location(self.board,
                                                  step_dst.col - sign(delta_x),
                                                  step_dst.row - sign(delta_y))
                jumped_piece = gamestate.piece_at(jumped_over)

                # check no piece is blocking a king's move
                if piece_face is self.PIECETYPE_KING:
                    testloc = self.board.Location(self.board,
                                                  step_src.col, step_src.row)
                    while True: # indeed do...while
                        testloc.col += sign(delta_x)
                        testloc.row += sign(delta_y)
                        if testloc.col == jumped_over.col: break
                        if gamestate.piece_at(testloc):
                            raise InvalidMove("Blocked by piece at %s" %
                                              testloc)

                if jumped_piece is None:
                    if piece_face is self.PIECETYPE_MAN:
                        raise InvalidMove("Cannot jump over empty square")
                else:
                    if gamestate.piece_owner(jumped_piece) is piece_owner:
                        raise InvalidMove("Cannot jump over one's own pieces")

                    # this is a capture
                    move.captured_locs.append(jumped_over)
                    more_steps_allowed = True
                    last_step_captured = True
                    logger.debug("was a capture")
                    continue

            # not a capture
            if last_step_captured:
                raise InvalidMove("Additional steps must capture")
            more_steps_allowed = False
            logger.debug("was not a capture")

        if move.target.row == self.player_homerow(piece_owner.next):
            move.promotes = True
        else:
            move.promotes = False

    def do_move(self, gamestate, player, move):
        assert move.source.holder is self.board
        assert move.target.holder is self.board

        piece = gamestate.piece_at(move.source)
        gamestate.take_piece(piece)
        gamestate.put_piece(move.target, piece)

        for capture in move.captured_locs:
            gamestate.take_piece(gamestate.piece_at(capture))

        if move.promotes:
            gamestate.piece_state(piece).promote()
