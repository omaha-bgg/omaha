# -*- coding: utf-8 -*-

# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.GenericChesslike import PreciousKings
from .MoveNotations import ShogiGameMN
from .abstract.ShogiGame import ShogiGame, ShogiGameWithDrops, OptionPromotionZoneShogiGame
from .abstract.RectBoardGame import RectBoard2PSideGame
from .MoveNotations import ShogiGameMN
from .PieceNotations import ShogiGamePN
from .piecetypes.torishogi import ToriShogiFacetype
from Omaha import Core
from . import movelib
import logging

logger = logging.getLogger(__name__)

class Piece(ShogiGame.Piece):
    MOVETYPES = {
        ToriShogiFacetype.PHOENIX: movelib.betza("K"),
        ToriShogiFacetype.FALCON: movelib.betza("FsfW"),
        ToriShogiFacetype.CRANE: movelib.betza("FvW"),
        ToriShogiFacetype.PHEASANT: movelib.betza("fDbF"),
        ToriShogiFacetype.LQUAIL: movelib.betza("fRbrBblF"),
        ToriShogiFacetype.RQUAIL: movelib.betza("fRblBbrF"),
        ToriShogiFacetype.SWALLOW: movelib.betza("fW"),

        # wikipedia "fBbRWbB2"
        ToriShogiFacetype.EAGLE: movelib.betza("fBbRWbF2"),
        ToriShogiFacetype.GOOSE: movelib.betza("fAbD"),
    }

    PROMOTIONS = {
        ToriShogiFacetype.PHOENIX: None,
        ToriShogiFacetype.FALCON: ToriShogiFacetype.EAGLE,
        ToriShogiFacetype.CRANE: None,
        ToriShogiFacetype.PHEASANT: None,
        ToriShogiFacetype.LQUAIL: None,
        ToriShogiFacetype.RQUAIL: None,
        ToriShogiFacetype.SWALLOW: ToriShogiFacetype.GOOSE,
    }

class EnglishPieceNotation(ShogiGamePN.WesternShogiPieceNotation):
    PIECE_TEXT = {
        ToriShogiFacetype.PHOENIX  : "Ph",
        ToriShogiFacetype.CRANE    : "Cr",
        ToriShogiFacetype.PHEASANT : "Pt",
        ToriShogiFacetype.RQUAIL   : "RQ",
        ToriShogiFacetype.LQUAIL   : "LQ",
        ToriShogiFacetype.FALCON   : "Fa",
        ToriShogiFacetype.SWALLOW  : "Sw",
    }

class EnglishToriNotation(ShogiGameMN.WesternShogiWithDropsNotation):
    PieceNotation = EnglishPieceNotation

class XboardPieceNotation(ShogiGamePN.WesternShogiPieceNotation):
    PIECE_TEXT = {
        ToriShogiFacetype.PHOENIX  : "K",
        ToriShogiFacetype.CRANE    : "C",
        ToriShogiFacetype.PHEASANT : "P",
        ToriShogiFacetype.RQUAIL   : "R",
        ToriShogiFacetype.LQUAIL   : "L",
        ToriShogiFacetype.FALCON   : "F",
        ToriShogiFacetype.SWALLOW  : "S",
    }

class SANNotation(ShogiGameMN.BaseSANNotation):
    PieceNotation = XboardPieceNotation
    default_piece_type = ToriShogiFacetype.SWALLOW

class ToriShogi(ShogiGameWithDrops, PreciousKings, # type: ignore[misc] # FIXME mypy#14279
                RectBoard2PSideGame, OptionPromotionZoneShogiGame):
    """Pieces, start position, and handicap for Tori Shogi.
    """

    Piece = Piece
    DefaultMoveNotation = EnglishToriNotation

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.boardwidth, self.boardheight = 7, 7
        self.promotion_rows_count = 2

    def has_king_status(self, piecestate):
        return piecestate.face is ToriShogiFacetype.PHOENIX

    # no handicap supported yet
    handicap = ()

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""

        for player in self.players:
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 0, 0),
                                    player, ToriShogiFacetype.LQUAIL)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 1, 0),
                                    player, ToriShogiFacetype.PHEASANT)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 2, 0),
                                    player, ToriShogiFacetype.CRANE)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 3, 0),
                                    player, ToriShogiFacetype.PHOENIX)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 4, 0),
                                    player, ToriShogiFacetype.CRANE)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 5, 0),
                                    player, ToriShogiFacetype.PHEASANT)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 6, 0),
                                    player, ToriShogiFacetype.RQUAIL)

            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 3, 1),
                                    player, ToriShogiFacetype.FALCON)

            for i in range(0, self.boardwidth):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, i, 2),
                                        player, ToriShogiFacetype.SWALLOW)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 4, 3),
                                    player, ToriShogiFacetype.SWALLOW)

    def get_writer_class(self, filename):
        assert filename.endswith(".pgn")
        from Omaha.GameIO.PGNWriter import PGNGameWriter
        return PGNGameWriter

class OneKanjiNotation(ShogiGamePN.OneKanjiShogiPieceNotation):
    PIECE_TEXT = {
        ToriShogiFacetype.PHOENIX  : "鵬",
        ToriShogiFacetype.CRANE    : "鶴",
        ToriShogiFacetype.PHEASANT : "雉",
        ToriShogiFacetype.RQUAIL   : "鶉", # FIXME
        ToriShogiFacetype.LQUAIL   : "鶉", # FIXME
        ToriShogiFacetype.FALCON   : "雉",
        ToriShogiFacetype.SWALLOW  : "燕",
        
        ToriShogiFacetype.EAGLE    : "鵰",
        ToriShogiFacetype.GOOSE    : "鴈",
    }
