# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from enum import Enum

class ToriShogiFacetype(Enum):
    PHOENIX  = "Phoenix"
    FALCON   = "Falcon"
    CRANE    = "Crane"
    PHEASANT = "Pheasant"
    LQUAIL   = "Left Quail"
    RQUAIL   = "Right Quail"
    SWALLOW  = "Swallow"

    EAGLE    = "Mountain Hawk Eagle"
    GOOSE    = "Wild Goose"
