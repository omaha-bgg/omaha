# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import logging
from typing import ClassVar

import Overlord
from Omaha.Core import InvalidMove, IncompleteMove
from .abstract.ChessGame import ChessGame, RectboardChessGame
from .abstract.GenericChesslike import PreciousKings
from .abstract.RectBoardGame import RectBoard4PCornerGame
from .abstract.SingleBoardGame import OnlySingleBoardGame
from . import Chess
from .piecetypes.chess import ChessFacetype
from . import movelib

logger = logging.getLogger(__name__)

class BaseForchess(RectBoard4PCornerGame, # type: ignore[misc] # FIXME mypy#14279
                   RectboardChessGame, PreciousKings, OnlySingleBoardGame):
    class Piece(Chess.Piece):
        MOVETYPES = {
            ChessFacetype.KING: movelib.betza("K"),
            ChessFacetype.QUEEN: movelib.betza("Q"),
            ChessFacetype.BISHOP: movelib.betza("B"),
            ChessFacetype.ROOK: movelib.betza("R"),
            ChessFacetype.KNIGHT: movelib.betza("N"),
    # FIXME wrong pawn moves
            ChessFacetype.PAWN: (
                (movelib.replace_capture, (((0, 1), 1), ((1, 0), 1)), None),
                (movelib.unoccupied_target, (((1, 1), 1),), None))
        }

    COLOR_YELLOW = "yellow"
    COLOR_RED = "red"

    player_names: ClassVar[tuple[str, ...]] = ChessGame.player_names + ("Yellow", "Red")

    def setup_players(self):
        super().setup_players()
        self.players[2].color = self.COLOR_YELLOW
        self.players[3].color = self.COLOR_RED

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.boardwidth, self.boardheight = 8, 8

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""
        for player in self.players:
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 0, 0),
                                    player, ChessFacetype.KING)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 0, 1),
                                    player, ChessFacetype.ROOK)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 1, 0),
                                    player, ChessFacetype.ROOK)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 2, 0),
                                    player, ChessFacetype.BISHOP)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 1, 1),
                                    player, ChessFacetype.QUEEN)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 0, 2),
                                    player, ChessFacetype.KNIGHT)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 2, 1),
                                    player, ChessFacetype.KNIGHT)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 1, 2),
                                    player, ChessFacetype.BISHOP)

            for i in range(0, 3):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, i, 3),
                                        player, ChessFacetype.PAWN)
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 3, i),
                                        player, ChessFacetype.PAWN)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 3, 3),
                                    player, ChessFacetype.PAWN)

    def has_king_status(self, piecestate):
        return piecestate.face is ChessFacetype.KING

    def __in_promotion_zone(self, player, loc):
        homecorner = self.player_homecorner(player)
        return (loc.col == self.boardwidth - 1 - homecorner[0] or
                loc.row == self.boardheight - 1 - homecorner[1])

    def supplement_and_check_move(self, gamestate, player, move, anticipate):
        super().supplement_and_check_move(gamestate, player, move, anticipate)

        try:
            # generic move-checking
            self.check_standard_moves(gamestate, player, move, anticipate)

        except InvalidMove:
            raise
        else:
            ## after move-validation, detect event-triggering moves

            # handle pawn promotion
            if  (gamestate.piece_state(move.piece).face is ChessFacetype.PAWN and
                 self.__in_promotion_zone(player, move.target)):
                if move.promotion == None:
                    raise IncompleteMove({
                        'promotion': Overlord.params.Choice(
                            label = "Promote to",
                            alternatives = dict(queen = ChessFacetype.QUEEN,
                                                rook = ChessFacetype.ROOK,
                                                bishop = ChessFacetype.BISHOP,
                                                knight = ChessFacetype.KNIGHT),
                            default = ChessFacetype.QUEEN) })
                if move.promotion not in (
                        ChessFacetype.QUEEN, ChessFacetype.KNIGHT,
                        ChessFacetype.ROOK, ChessFacetype.BISHOP ):
                    raise InvalidMove(f"pawn cannot promote to {move.promotion}")
            elif move.promotion is not None:
                raise InvalidMove(f"illegal promotion request {move.promotion!r}")

    def _ensure_move_is_complete(self, gamestate, move):
        super()._ensure_move_is_complete(gamestate, move)
        piece = gamestate.piece_at(move.source)
        if  (gamestate.piece_state(piece).face is ChessFacetype.PAWN and
             self.__in_promotion_zone(gamestate.piece_owner(move.piece), move.target)):
            move.promotion = ChessFacetype.QUEEN

    def get_writer_class(self, filename):
        assert filename.endswith(".pgn")
        from Omaha.GameIO.PGNWriter import PGNGameWriter
        return PGNGameWriter


class CutthroatForchess(BaseForchess):
    pass
