# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.ChessGame import ChessPiece, ChessGame, RectboardChessGame
from .abstract.GenericChesslike import PreciousKings
from .abstract.RectBoardGame import RectBoard2PSideGame
from .abstract.SingleBoardGame import OnlySingleBoardGame
from Omaha.Core import InvalidMove, IncompleteMove
from .piecetypes.chess import ChessFacetype
from . import movelib
import Overlord

import logging
logger = logging.getLogger(__name__)

class Piece(ChessPiece):
    MOVETYPES = {
        ChessFacetype.KING: movelib.betza("K"),
        ChessFacetype.QUEEN: movelib.betza("Q"),
        ChessFacetype.BISHOP: movelib.betza("B"),
        ChessFacetype.ROOK: movelib.betza("R"),
        ChessFacetype.KNIGHT: movelib.betza("N"),
        ChessFacetype.PAWN: (movelib.betza("fcFfmW") +
                             [(movelib.And(movelib.never_moved,
                                           movelib.unoccupied_target,
                                           movelib.range_free),
                               (((0, 1), (2, 2)),), ChessGame.STARTUP_BOOST)]
        )
    }

    PROMOTIONS = {
        ChessFacetype.KING: None,
        ChessFacetype.QUEEN: None,
        ChessFacetype.BISHOP: None,
        ChessFacetype.ROOK: None,
        ChessFacetype.KNIGHT: None,
        ChessFacetype.PAWN: None, # handled for now in special code
    }

class Chess(RectBoard2PSideGame, RectboardChessGame, # type: ignore[misc] # FIXME mypy#14279
            PreciousKings, OnlySingleBoardGame):
    Piece = Piece

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.boardwidth, self.boardheight = 8, 8

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""
        for player in self.players:
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 0, 0),
                                    player, ChessFacetype.ROOK)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 1, 0),
                                    player, ChessFacetype.KNIGHT)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 2, 0),
                                    player, ChessFacetype.BISHOP)
            self.new_piece_on_board(self.scratch_state,
                                    (3, self.player_relative_row(player, 0)),
                                    player, ChessFacetype.QUEEN)
            self.new_piece_on_board(self.scratch_state,
                                    (4, self.player_relative_row(player, 0)),
                                    player, ChessFacetype.KING)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 5, 0),
                                    player, ChessFacetype.BISHOP)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 6, 0),
                                    player, ChessFacetype.KNIGHT)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 7, 0),
                                    player, ChessFacetype.ROOK)

            for i in range(0, self.boardwidth):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, i, 1),
                                        player, ChessFacetype.PAWN)

    def has_king_status(self, piecestate):
        return piecestate.face is ChessFacetype.KING

    def supplement_and_check_move(self, gamestate, player, move, anticipate):

        # preprocess explicit castling
        if move.type in (ChessGame.CASTLING_KINGSIDE,
                         ChessGame.CASTLING_QUEENSIDE):
            rookdelta = {ChessGame.CASTLING_KINGSIDE: 3,
                         ChessGame.CASTLING_QUEENSIDE: 4}[move.type]
            rooks = []
            for piece in gamestate.pieces:
                if gamestate.piece_owner(piece) is not player:
                    continue
                piece_face = gamestate.piece_state(piece).face
                if piece_face is ChessFacetype.KING:
                    if gamestate.piece_state(piece).has_moved:
                        raise InvalidMove("cannot castle king after moving")
                    move.piece = piece
                    move.set_source(gamestate.piece_location(piece))
                elif (piece_face is ChessFacetype.ROOK and
                      not gamestate.piece_state(piece).has_moved):
                    rooks.append(piece)
            for rookloc in [gamestate.piece_location(rook) for rook in rooks]:
                delta = rookloc - move.source
                if abs(delta.dcol) == rookdelta:
                    # found correct rook: move 2 steps in its direction
                    delta = delta.normalized
                    move.set_target(move.source + delta + delta)
                    break
            else:
                raise InvalidMove("no rook for castling")

        super().supplement_and_check_move(gamestate, player, move, anticipate)

        try:
            ## check for special moves
            # en passant
            if   (gamestate.piece_state(move.piece).face is ChessFacetype.PAWN and
                  move.target.row - move.source.row == self.player_orientation(player)[1] and
                  abs(move.target.col - move.source.col) == 1 and
                  move.capture is None and
                  self.moves.nmoves_done > 0):
                captured_pos = self.board.Location(
                    self.board, move.target.col, move.source.row)
                captured_piece = gamestate.piece_at(captured_pos)
                prev_move = self.moves.last
                if  (captured_piece is None or
                     gamestate.piece_state(captured_piece).face is not ChessFacetype.PAWN or
                     gamestate.piece_owner(captured_piece) is player or
                     prev_move.target != captured_pos or
                     prev_move.type is not self.STARTUP_BOOST):
                    raise InvalidMove("illegal attempt at en-passant")
                move.type = self.ENPASSANT
                move.capture = move.Capture(captured_piece)
                move.capture.location = captured_pos

            # castling
            elif (gamestate.piece_state(move.piece).face is ChessFacetype.KING and
                  not gamestate.piece_state(move.piece).has_moved and
                  move.target.row == move.source.row and
                  abs(move.target.col - move.source.col) == 2):
                # FIXME: check that no square crossed by king are under attack
                move_direction = (move.target - move.source).normalized

                if gamestate.piece_at(move.source + move_direction):
                    raise InvalidMove("another piece blocks castling")
                maybe_rook_pos = move.target + move_direction
                maybe_rook = gamestate.piece_at(maybe_rook_pos)
                if  (maybe_rook is not None and
                     gamestate.piece_state(maybe_rook).face is ChessFacetype.ROOK and
                     not gamestate.piece_state(maybe_rook).has_moved):
                    move.type = self.CASTLING_KINGSIDE
                    move.side_effect = self.Move(
                        gamestate=gamestate.frozen(), player=move.player,
                        source=maybe_rook_pos,
                        target=move.source + move_direction)
                else:
                    if gamestate.piece_at(move.target + move_direction):
                        raise InvalidMove("another piece blocks castling")
                    maybe_rook_pos = (move.target + move_direction +
                                      move_direction)
                    maybe_rook = gamestate.piece_at(maybe_rook_pos)
                    if  (maybe_rook is not None and
                         gamestate.piece_state(maybe_rook).face is ChessFacetype.ROOK and
                         not gamestate.piece_state(maybe_rook).has_moved):
                        move.type = self.CASTLING_QUEENSIDE
                        move.side_effect = self.Move(
                            gamestate=gamestate.frozen(), player=move.player,
                            source=maybe_rook_pos,
                            target=move.source + move_direction)
                    else:
                        raise InvalidMove("no suitable rook for castling")

            # generic move-checking
            else:
                self.check_standard_moves(gamestate, player, move, anticipate)

        except InvalidMove:
            raise
        else:
            ## after move-validation, detect event-triggering moves

            # handle pawn promotion
            if  (gamestate.piece_state(move.piece).face is ChessFacetype.PAWN and
                 move.target.row == self.player_homerow(player.next)):
                if move.promotion is None:
                    raise IncompleteMove({
                        'promotion': Overlord.params.Choice(
                            label = "Promote to",
                            alternatives = dict(queen = ChessFacetype.QUEEN,
                                                rook = ChessFacetype.ROOK,
                                                bishop = ChessFacetype.BISHOP,
                                                knight = ChessFacetype.KNIGHT),
                            default = ChessFacetype.QUEEN) })
                else:
                    if move.promotion not in (
                            ChessFacetype.QUEEN, ChessFacetype.KNIGHT,
                            ChessFacetype.ROOK, ChessFacetype.BISHOP ):
                        raise InvalidMove("pawn cannot promote to %s" %
                                          move.promotion)
            elif move.promotion is not None:
                raise InvalidMove("illegal promotion request %r" % (move.promotion,))

    def _ensure_move_is_complete(self, gamestate, move):
        super()._ensure_move_is_complete(gamestate, move)
        piece = gamestate.piece_at(move.source)
        if  (gamestate.piece_state(piece).face is ChessFacetype.PAWN and
             move.target.row == self.player_homerow(gamestate.piece_owner(piece).next)):
            # must select a promotion for (capture) move to be valid, any will do
            move.promotion = ChessFacetype.QUEEN

    def get_writer_class(self, filename):
        assert filename.endswith(".pgn")
        from Omaha.GameIO.PGNWriter import PGNGameWriter
        return PGNGameWriter
