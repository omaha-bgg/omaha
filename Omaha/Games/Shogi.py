# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.GenericChesslike import PreciousKings
from .abstract.ShogiGame import ShogiGameWithDrops, OptionPromotionZoneShogiGame
from .abstract import ModernShogi
from .abstract.RectBoardGame import RectBoard2PSideGame
from .MoveNotations import StandardShogiMN
from .piecetypes.shogi import ShogiFacetype
import Overlord

class Shogi(ModernShogi.Game, RectBoard2PSideGame, # type: ignore[misc] # FIXME mypy#14279
            ShogiGameWithDrops, PreciousKings, OptionPromotionZoneShogiGame):
    DefaultMoveNotation = StandardShogiMN.EnglishShogiWithDropsNotation

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.boardwidth, self.boardheight = 9,9
        self.promotion_rows_count = 3

    @Overlord.parameter
    def handicap(cls, context):
        return Overlord.params.Choice(
            label = 'Handicap',
            alternatives = {
                "0 - None": (),
                "1 - Left lance": (ShogiFacetype.LANCE,),
                "2 - Bishop": (ShogiFacetype.BISHOP,),
                "3 - Rook": (ShogiFacetype.ROOK,),
                "4 - Rook an left lance": (ShogiFacetype.ROOK,
                                           ShogiFacetype.LANCE),
                "5 - Rook and bishop": (ShogiFacetype.ROOK,
                                        ShogiFacetype.BISHOP),
                "6 - Rook, bishop, lances": (ShogiFacetype.ROOK,
                                             ShogiFacetype.BISHOP,
                                             ShogiFacetype.LANCE), # *2
                "7 - Rook, bishop, lances, knights": (
                    ShogiFacetype.ROOK, ShogiFacetype.BISHOP,
                    ShogiFacetype.LANCE, ShogiFacetype.KNIGHT), # *2
                "8 - Rook, bishop, lances, knights, silvers": (
                    ShogiFacetype.ROOK, ShogiFacetype.BISHOP,
                    ShogiFacetype.LANCE, ShogiFacetype.KNIGHT, ShogiFacetype.SILVER), # *2
                },
            default = ())

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""

        for player in self.players:
            if  (player is self.players[0] or
                 ShogiFacetype.LANCE not in self.handicap):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 0, 0),
                                        player, ShogiFacetype.LANCE)
            if  (player is self.players[0] or
                 ShogiFacetype.KNIGHT not in self.handicap):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 1, 0),
                                        player, ShogiFacetype.KNIGHT)
            if  (player is self.players[0] or
                 ShogiFacetype.SILVER not in self.handicap):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 2, 0),
                                        player, ShogiFacetype.SILVER)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 3, 0),
                                    player, ShogiFacetype.GOLD)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 4, 0),
                                    player, ShogiFacetype.KING)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 5, 0),
                                    player, ShogiFacetype.GOLD)
            if  (player is self.players[0] or
                 ShogiFacetype.SILVER not in self.handicap):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 6, 0),
                                        player, ShogiFacetype.SILVER)
            if  (player is self.players[0] or
                 ShogiFacetype.KNIGHT not in self.handicap):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 7, 0),
                                        player, ShogiFacetype.KNIGHT)
            if  (player is self.players[0] or
                 not(ShogiFacetype.LANCE in self.handicap and
                     ShogiFacetype.BISHOP in self.handicap)):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 8, 0),
                                        player, ShogiFacetype.LANCE)

            if  (player is self.players[0] or
                 ShogiFacetype.BISHOP not in self.handicap):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 1, 1),
                                        player, ShogiFacetype.BISHOP)
            if  (player is self.players[0] or
                 ShogiFacetype.ROOK not in self.handicap):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 7, 1),
                                        player, ShogiFacetype.ROOK)

            for i in range(0, self.boardwidth):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, i, 2),
                                        player, ShogiFacetype.PAWN)

    def has_king_status(self, piecestate):
        return piecestate.face is ShogiFacetype.KING

    def get_writer_class(self, filename):
        assert filename.endswith(".pgn")
        from Omaha.GameIO.PGNWriter import PGNGameWriter
        return PGNGameWriter
