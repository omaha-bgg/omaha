# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.ChessGame import ChessPiece, ChessGame, HexboardChessGame, ChessCoordinateNotation
from .abstract.GenericChesslike import PreciousKings
from .abstract.HexBoardGame import HexBoard2PCornerGame, HexChessLikeBentlinesCoordinateNotation
from .abstract.SingleBoardGame import OnlySingleBoardGame
from .piecetypes.chess import ChessFacetype
from . import movelib

class Piece(ChessPiece):
    MOVETYPES = {
        ChessFacetype.KING: (
            (movelib.default_move, movelib.MOVETYPE_HEXCHESS_KING, None),),
        ChessFacetype.QUEEN: (
            (movelib.range_free, movelib.MOVETYPE_HEXCHESS_QUEEN, None),),
        ChessFacetype.BISHOP: (
            (movelib.range_free, movelib.MOVETYPE_HEXCHESS_BISHOP, None),),
        ChessFacetype.ROOK: (
            (movelib.range_free, movelib.MOVETYPE_HEXCHESS_ROOK, None),),
        ChessFacetype.KNIGHT: (
            (movelib.default_move, movelib.MOVETYPE_HEXCHESS_KNIGHT, None),),
        ChessFacetype.PAWN: (
            (movelib.replace_capture,
             movelib.MOVETYPE_GLINSKICHESS_PAWNCAPTURE, None),
            # FIXME: BOOST does not express need of not being blocked
            (movelib.And(movelib.never_moved, movelib.unoccupied_target,
                         movelib.range_free),
             (((0, 1), 2),), ChessGame.STARTUP_BOOST),
            (movelib.unoccupied_target, (((0, 1), 1),), None))
    }

    PROMOTIONS = {
        ChessFacetype.KING: None,
        ChessFacetype.QUEEN: None,
        ChessFacetype.BISHOP: None,
        ChessFacetype.ROOK: None,
        ChessFacetype.KNIGHT: None,
        ChessFacetype.PAWN: None, # handled for now in special code
    }

class HexChessBentlinesCoordinateNotation(HexChessLikeBentlinesCoordinateNotation,
                                          ChessCoordinateNotation):
    pass

class GlinskiHexChess(HexBoard2PCornerGame, # type: ignore[misc] # FIXME mypy#14279
                      HexboardChessGame, PreciousKings, OnlySingleBoardGame):
    Piece = Piece
    DefaultMoveNotation = HexChessBentlinesCoordinateNotation

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.boardwidth0, self.boardwidth1, self.boardwidth2 = 6, 6, 6

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""

        # first, symetrically-placed pieces
        for player in self.players:
            for i in range(5):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player,
                                                                 1 + i, i),
                                        player, ChessFacetype.PAWN)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 2, 0),
                                    player, ChessFacetype.ROOK)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 3, 0),
                                    player, ChessFacetype.KNIGHT)
            for i in range(3):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 5, i),
                                        player, ChessFacetype.BISHOP)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 7, 2),
                                    player, ChessFacetype.KNIGHT)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 8, 3),
                                    player, ChessFacetype.ROOK)
            for i in range(4):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player,
                                                                 6 + i, 4),
                                        player, ChessFacetype.PAWN)

        # then, king and queen facing their counterparts
        player = self.players[0]
        self.new_piece_on_board(self.scratch_state,
                                self.player_relative_pos(player, 6, 1),
                                player, ChessFacetype.KING)
        self.new_piece_on_board(self.scratch_state,
                                self.player_relative_pos(player, 4, 0),
                                player, ChessFacetype.QUEEN)
        player = self.players[1]
        self.new_piece_on_board(self.scratch_state,
                                self.player_relative_pos(player, 4, 0),
                                player, ChessFacetype.KING)
        self.new_piece_on_board(self.scratch_state,
                                self.player_relative_pos(player, 6, 1),
                                player, ChessFacetype.QUEEN)

    def supplement_and_check_move(self, gamestate, player, move, anticipate):
        super().supplement_and_check_move(gamestate, player, move, anticipate)
        # FIXME: only allow standard moves, no side-effects supported
        self.check_standard_moves(gamestate, player, move, anticipate)

    def has_king_status(self, piecestate):
        return piecestate.face is ChessFacetype.KING
