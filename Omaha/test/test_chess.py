# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games.Chess import Chess
from Omaha.Games.abstract import ChessGame
from Omaha import Core
import Overlord

import unittest
from . import lib

class test00_chess(unittest.TestCase):
    def test00_basics(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        g = Chess(parentcontext=app.context)
        app.game = g # emulates GameApp
        g.setup()
        g.change_phase(Core.GamePhase.Playing) # FIXME!
        self.assertEqual(g.phase, Core.GamePhase.Playing)

        # replay a small partial game
        moves = "e2e4 e7e5 d2d4 e5d4".split(' ')
        for move_str in moves:
            lib.make_move(g, move_str)
        # compare effective moves with original list
        self.assertEqual([g.default_notation.move_serialization(m)
                          for m in g.moves.played], moves)

        moves = "Nf3 Nc6 Bb5 Qe7 O-O Qxe4 Re1".split(' ')
        san = ChessGame.SANNotation(g)
        for move_str in moves:
            lib.make_move(g, move_str, notation=san)

        # check pinning
        with self.assertRaises(Core.InvalidMove) as cm:
            lib.make_move(g, "e4f5")
        ex = cm.exception
        self.assertTrue(ex.message.startswith("player's king would be in check"),
                        "Pinned piece should be forbidden to move")

        # check undo
        g.undo_move()
        lib.make_move(g, "Qe1", notation=san)
        g.undo_move()
        g.redo_move()

class test20_loadpgn_fullgame(lib.PGNLoad, unittest.TestCase):
    filename = 'Omaha/test/data/chess1.pgn'
class test20_loadpgn_checkinducing_queensidecastling(lib.PGNLoad, unittest.TestCase):
    filename = 'Omaha/test/data/chess_O-O-O+.pgn'
class test20_loadpgn_knight_promotion(lib.PGNLoad, unittest.TestCase):
    filename = 'Omaha/test/data/chess_promotion.pgn'
class test20_loadpgn_move_disambiguation(lib.PGNLoad, unittest.TestCase):
    filename = 'Omaha/test/data/chess_disambiguation.pgn'

class test30_frompgn(lib.FromPGN, unittest.TestCase):
    filename = 'Omaha/test/data/chess1.pgn'
