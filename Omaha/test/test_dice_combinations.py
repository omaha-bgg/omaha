# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import unittest

from Omaha.Core.misc import dice_combinations

class setOf2(unittest.TestCase):
    def test_full(self):
        self.assertEqual(dice_combinations((1,2), 2), {((1, 2),)})

class setOf4(unittest.TestCase):
    def test_full(self):
        self.assertEqual(dice_combinations((1,2,3,4), 2), {((1, 2), (3, 4)),
                                                           ((1, 3), (2, 4)),
                                                           ((1, 4), (2, 3))})
    def test_double_lower(self):
        self.assertEqual(dice_combinations((1,1,3,4), 2), {((1, 1), (3, 4)),
                                                           ((1, 3), (1, 4))})
    def test_double_middle(self):
        self.assertEqual(dice_combinations((1,2,2,4), 2), {((1, 2), (2, 4)),
                                                           ((1, 4), (2, 2))})
    def test_double_higher(self):
        self.assertEqual(dice_combinations((1,2,4,4), 2), {((1, 2), (4, 4)),
                                                           ((1, 4), (2, 4))})
    def test_triple_lower(self):
        self.assertEqual(dice_combinations((1,1,1,4), 2), {((1, 1), (1, 4))})
    def test_triple_upper(self):
        self.assertEqual(dice_combinations((1,2,2,2), 2), {((1, 2), (2, 2))})

class setOf6(unittest.TestCase):
    def test_full(self):
        self.assertEqual(len(dice_combinations((1,2,3,4,5,6), 2)), 15)
