# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games.abstract.GenericChesslike import (MOVERANGE_INF, MoveDescription,
                                                   MovePredicate, MoveTypeId)
from Omaha.Games import movelib
from Omaha import Core
import Overlord

import unittest
from .lib import StubToolkit, StubRectGame

class BetzaExpandTemplate:
    @property
    def predicate(self):
        return movelib.range_free
    string: str
    allmoves: tuple[tuple[MovePredicate,MoveDescription,MoveTypeId],...]

    def test_betza(self):
        self.assertEqual(frozenset(movelib.betza(self.string)),
                         movelib.regroup_by_predicate(self.allmoves))

class OnePredicateBetzaExpandTemplate(BetzaExpandTemplate):
    "Test for simple move types with a single predicate"
    predmoves: MoveDescription
    @property
    def predicate(self):
        return movelib.range_free
    @property
    def allmoves(self):
        return ((self.predicate, self.predmoves, None),)

class ExpectedFail_OnePredicateBetzaExpandTemplate(OnePredicateBetzaExpandTemplate,
                                                   unittest.TestCase):
    @unittest.expectedFailure
    def test_betza(self):
        super(ExpectedFail_OnePredicateBetzaExpandTemplate, self).test_betza()

# basics

class test_knight(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "N"
    predmoves = (((-1, 2), 1), ((1, 2), 1),
             ((-2, 1), 1), ((2, 1), 1),
             ((-2, -1), 1), ((2, -1), 1),
             ((-1, -2), 1), ((1, -2), 1))

# basic aliases

class test_king(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "K"
    predmoves = (((-1, 1), 1), ((0, 1), 1), ((1, 1), 1),
             ((-1, 0), 1), ((1, 0), 1),
             ((-1, -1), 1), ((0, -1), 1), ((1, -1), 1))

class test_queen(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "Q"
    predmoves = (((0, 1), MOVERANGE_INF), ((0, -1), MOVERANGE_INF),
             ((-1, 0), MOVERANGE_INF), ((1, 0), MOVERANGE_INF),
             ((-1, 1), MOVERANGE_INF), ((1, 1), MOVERANGE_INF),
             ((-1, -1), MOVERANGE_INF), ((1, -1), MOVERANGE_INF))

class test_bishop(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "B"
    predmoves = (((-1, 1), MOVERANGE_INF), ((1, 1), MOVERANGE_INF),
             ((-1, -1), MOVERANGE_INF), ((1, -1), MOVERANGE_INF))

class test_rook(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "R"
    predmoves = (((0, 1), MOVERANGE_INF), ((0, -1), MOVERANGE_INF),
             ((-1, 0), MOVERANGE_INF), ((1, 0), MOVERANGE_INF))

# combos

class test_shogi_dragonking(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "RF"
    predmoves = (((0, 1), MOVERANGE_INF), ((0, -1), MOVERANGE_INF),
             ((-1, 0), MOVERANGE_INF), ((1, 0), MOVERANGE_INF),
             ((-1, 1), 1), ((1, 1), 1),
             ((-1, -1), 1), ((1, -1), 1))
class test_shogi_dragonhorse(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "BW"
    predmoves = (((0, 1), 1), ((0, -1), 1),
             ((-1, 0), 1), ((1, 0), 1),
             ((-1, 1), MOVERANGE_INF), ((1, 1), MOVERANGE_INF),
             ((-1, -1), MOVERANGE_INF), ((1, -1), MOVERANGE_INF))

# direction modifiers

class test_shogi_pawn(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fW"
    predmoves = (((0, 1), 1),)
class test_shogi_lance(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fR"
    predmoves = (((0, 1), MOVERANGE_INF),)
class test_shogi_silver(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "FfW"
    predmoves = (((-1, 1), 1), ((0, 1), 1), ((1, 1), 1),
             ((-1, -1), 1), ((1, -1), 1))

class test_forward_ferz(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fF"
    predmoves = (((-1, 1), 1), ((1, 1), 1))
class test_shogi_gold(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "WfF"
    predmoves = (((-1, 1), 1), ((0, 1), 1), ((1, 1), 1),
             ((-1, 0), 1), ((1, 0), 1),
             ((0, -1), 1))

class test_shogi_knight(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fN"
    predmoves = (((-1, 2), 1), ((1, 2), 1))

# combined-restriction direction modifiers

class test_forwardleft_ferz(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "flF"
    predmoves = (((-1, 1), 1),)

# combined-addition direction modifiers

class test_forchess_pawn(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "frW"
    predmoves = (((0, 1), 1), ((1, 0), 1))

class test_chu_drunken(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "FsfW"
    predmoves = (((-1, 1), 1), ((0, 1), 1), ((1, 1), 1),
             ((-1, 0), 1),              ((1, 0), 1),
             ((-1, -1), 1),             ((1, -1), 1))

class test_chu_leopard(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "FvW"
    predmoves = (((-1, 1), 1),  ((0, 1), 1),  ((1, 1), 1),
             ((-1, -1), 1), ((0, -1), 1), ((1, -1), 1))
class test_chu_leopard2(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "FfbW"
    predmoves = (((-1, 1), 1),  ((0, 1), 1),  ((1, 1), 1),
             ((-1, -1), 1), ((0, -1), 1), ((1, -1), 1))
class test_chu_leopard3(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fbWF"
    predmoves = (((-1, 1), 1),  ((0, 1), 1),  ((1, 1), 1),
             ((-1, -1), 1), ((0, -1), 1), ((1, -1), 1))

# combined-substraction modifiers

class test_rfF(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "rfF"
    predmoves = (((1, 1), 1),)

# combined addition and substraction modifiers

# rflF == rfFlF
class test_rflF(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "rflF"
    predmoves = (((1, 1), 1), ((-1, 1), 1), ((-1, -1), 1))

# combined-addition direction modifiers on leapers

class test_fsN(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fsN"
    predmoves = (((-2, 1), 1), ((2, 1), 1))

# modifiers on K atom-not-alias

# fK == fW
class test_fK(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fK"
    predmoves = (((0, 1), 1),)

# frK == frF
class test_frK(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "frK"
    predmoves = (((1, 1), 1),)

# ffrrK == fKrK == fWrW
class test_fWrW(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fWrW"
    predmoves = (((0, 1), 1), ((1, 0), 1))
class test_fKrK(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fKrK"
    predmoves = (((0, 1), 1), ((1, 0), 1))
class test_ffrrK(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "ffrrK"
    predmoves = (((0, 1), 1), ((1, 0), 1))

# inclusion

class test_minimization(unittest.TestCase):
    def test_KF(self):
        self.assertEqual(frozenset(movelib.betza("KF")),
                         frozenset(movelib.betza("K")))
    def test_BF(self):
        self.assertEqual(frozenset(movelib.betza("BF")),
                         frozenset(movelib.betza("B")))
    @unittest.expectedFailure
    def test_B2(self):
        # notation variation used in Wikipedia, but not implemented
        self.assertEqual(frozenset(movelib.betza("B2")),
                         frozenset(movelib.betza("F2")))
    def test_ToriEagle_fBbRWbF2_fBbRbF2fsW(self):
        # adjusted from Wikipedia vs Crazywa
        self.assertEqual(frozenset(movelib.betza("fBbRWbF2")),
                         frozenset(movelib.betza("fBbRbF2fsW")))
    def test_ToriEagle_fBbRWbF2_KbRfBbF2(self):
        # adjusted from Wikipedia vs SjaakII
        self.assertEqual(frozenset(movelib.betza("fBbRWbF2")),
                         frozenset(movelib.betza("KbRfBbF2")))

# misc combos

class test_chess_pawn(BetzaExpandTemplate, unittest.TestCase):
    string = "fcFfmW"
    allmoves = ((movelib.replace_capture, (((-1, 1), 1), ((1, 1), 1)), None),
                (movelib.unoccupied_target, (((0, 1), 1),), None),
    )

class test_wa_fox(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fbWFfbDA"
    predmoves = (((-1, 1), 1),  ((0, 1), 1),  ((1, 1), 1),
             ((-1, -1), 1), ((0, -1), 1), ((1, -1), 1),
             ((0, 2), 1),  ((0, -2), 1),
             ((-2, 2), 1), ((2, 2), 1),  ((2, -2), 1), ((-2, -2), 1))
class test_wa_fox2(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "vWFvDA"
    predmoves = (((-1, 1), 1),  ((0, 1), 1),  ((1, 1), 1),
             ((-1, -1), 1), ((0, -1), 1), ((1, -1), 1),
             ((0, 2), 1),  ((0, -2), 1),
             ((-2, 2), 1), ((2, 2), 1),  ((2, -2), 1), ((-2, -2), 1))

class test_tori_pheasant(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fDbF"
    predmoves = (((0, 2), 1), ((-1, -1), 1), ((1, -1), 1))

class test_tori_lquail(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fRbrBblF"
    predmoves = (((0, 1), MOVERANGE_INF),
            ((-1, -1), 1), ((1, -1), MOVERANGE_INF))

class test_tori_rquail(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fRblBbrF"
    predmoves = (((0, 1), MOVERANGE_INF),
             ((1, -1), 1), ((-1, -1), MOVERANGE_INF))

class test_tori_eagle(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fBbRWbF2"
    predmoves = (((-1, 1), MOVERANGE_INF),
             ((1, 1), MOVERANGE_INF),
             ((0, -1), MOVERANGE_INF),
             ((0, -1), 1), # FIXME we do not minimize
             ((0, 1), 1), ((-1, 0), 1), ((1, 0), 1),
             ((-1, -1), 2), ((1, -1), 2))

class test_tori_goose(OnePredicateBetzaExpandTemplate, unittest.TestCase):
    string = "fAbD"
    predmoves = (((-2, 2), 1), ((2, 2), 1), ((0, -2), 1))
