# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games.Chess5x5 import Chess5x5
from Omaha.Games.piecetypes.chess import ChessFacetype
from Omaha import Core
import Overlord

import unittest
from . import lib

class test(unittest.TestCase):
    def setUp(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        g = Chess5x5(parentcontext=app.context)
        app.game = g # emulates GameApp
        g.setup()
        g.change_phase(Core.GamePhase.Playing) # FIXME!
        self.game = g

    def test_promotion_queen(self):
        self.assertEqual(self.game.phase, Core.GamePhase.Playing)
        moves = "c2c3 d4c3 d2d4 a4a3 d4c5q b4b3".split(' ')
        for move_str in moves:
            lib.make_move(self.game, move_str)
        lib.make_move(self.game, "c5a3") # capture as a queen
    def test_promotion_knight(self):
        self.assertEqual(self.game.phase, Core.GamePhase.Playing)
        moves = "c2c3 d4c3 d2d4 a4a3 d4c5n b4b3".split(' ')
        for move_str in moves:
            lib.make_move(self.game, move_str)
        lib.make_move(self.game, "c5b3") # capture as a knight

    def test_enpassant(self):
        self.assertEqual(self.game.phase, Core.GamePhase.Playing)

        # 
        moves = "b2b3 c4b3 c2c4 b4c3".split(' ')
        for move_str in moves:
            lib.make_move(self.game, move_str)
        self.assertTrue(self.game.moves.played[-1].capture != None,
                        "en-passant should result in a capture")
    def test_enpassant_nonboost(self):
        self.assertEqual(self.game.phase, Core.GamePhase.Playing)

        # 
        moves = "b2b3 c4c3 d2d3".split(' ')
        for move_str in moves:
            lib.make_move(self.game, move_str)
        with self.assertRaises(Core.InvalidMove) as cm:
            lib.make_move(self.game, "c3d2")
        ex = cm.exception
        self.assertTrue(ex.message == "illegal attempt at en-passant",
                        "en-passant can only capture after a startup boost")
    def test_enpassant_delayed(self):
        self.assertEqual(self.game.phase, Core.GamePhase.Playing)

        # 
        moves = "b2b3 c4b3 c2c4 a4a3 c1a3".split(' ')
        for move_str in moves:
            lib.make_move(self.game, move_str)
        with self.assertRaises(Core.InvalidMove) as cm:
            lib.make_move(self.game, "b4c3")
        ex = cm.exception
        self.assertTrue(ex.message == "illegal attempt at en-passant",
                        "en-passant can only capture immediately after startup boost")

    # FIXME: lacks test for en-passant with a pinned pawn

    def test_pawn_check(self):
        moves = "c2c3 b4c3 b2b4".split(' ')
        for move_str in moves:
            lib.make_move(self.game, move_str)
        with self.assertRaises(Core.InvalidMove) as cm:
            lib.make_move(self.game, "a4a3")
        ex = cm.exception
        self.assertTrue(ex.message.startswith("player's king would be in check"),
                        "threat from promoting pawn should be seen as any threat")
    def test_undo_promotion(self):
        g = self.game
        moves = "d2d3 e4d3 d1e3 d3d2 e3d5".split(' ')
        for move_str in moves:
            lib.make_move(g, move_str)
        # before propotion move, we have a pawn here
        pawnloc = g.board.Location(g.board, 3, 1)
        pawn = g.scratch_state.piece_at(pawnloc)
        self.assertEqual(g.scratch_state.piece_state(pawn).face, ChessFacetype.PAWN)
        # promote, we have no more pawn, and a promoted queen
        lib.make_move(g, "d2d1q")
        queenloc = g.board.Location(g.board, 3, 0)
        self.assertEqual(g.scratch_state.piece_at(pawnloc), None)
        self.assertEqual(g.scratch_state.piece_state(g.scratch_state.piece_at(queenloc)).face,
            ChessFacetype.QUEEN)
        # undo, we have a pawn back
        g.undo_move()
        self.assertEqual(g.scratch_state.piece_at(queenloc), None)
        self.assertEqual(g.scratch_state.piece_at(pawnloc), pawn)
