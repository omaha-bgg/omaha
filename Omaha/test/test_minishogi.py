# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games.MiniShogi import MiniShogi
from Omaha.Games.piecetypes.shogi import ShogiFacetype
from Omaha import Core
import Overlord

import unittest
from . import lib

class test00_normal(unittest.TestCase):
    def setUp(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        g = MiniShogi(parentcontext=app.context)
        app.game = g # emulates GameApp
        g.setup()
        g.change_phase(Core.GamePhase.Playing) # FIXME!
        self.game = g
    def test00_minishogi(self):
        g = self.game
        self.assertEqual(len(g.last_state.pieces), 12)
        self.assertEqual(g.phase, Core.GamePhase.Playing)

        # replay a small partial game
        moves = "B2e-1d B4ax1d R1ex1d B*2e R1d-1e B2e-5b+".split(' ')
        for move_str in moves:
            lib.make_move(g, move_str)

        # checking move
        lib.make_move(g, "B*3c")

        # check refusal of a move that does not protect the king
        with self.assertRaises(Core.InvalidMove) as cm:
            lib.make_move(g, "+B5b-4c")
        ex = cm.exception
        self.assertTrue(ex.message.startswith("player's king would be in check"),
                        "Non-protecting move should be forbidden while in check")

        lib.make_move(g, "G2a-2b")
        lib.make_move(g, "B3c-4d")

        # check pinning
        with self.assertRaises(Core.InvalidMove) as cm:
            lib.make_move(g, "G2b-2c")
        ex = cm.exception
        self.assertTrue(ex.message.startswith("player's king would be in check"),
                        "Non-protecting move should be forbidden while in check")

        # undo the 3 moves used for in-check tests
        extramoves_num = 3
        for _ in range(extramoves_num): g.undo_move()

        # compare effective moves with original list
        self.assertEqual([g.default_notation.move_serialization(m)
                          for m in g.moves.played], moves)

        # check promotion status of bishop
        bishop = g.scratch_state.piece_at(g.board.Location(g.board, 0, 3))
        assert bishop
        assert g.scratch_state.piece_state(bishop).promoted

    def test10_undo(self):
        g = self.game

        lib.make_move(g, "R1ex1b")
        g.undo_move()

        # check restored state
        rook = g.scratch_state.piece_at(g.board.Location(g.board, 4, 0))
        assert rook
        self.assertEqual(g.scratch_state.piece_state(rook).face, ShogiFacetype.ROOK)
        pawn = g.scratch_state.piece_at(g.board.Location(g.board, 4, 3))
        assert pawn
        self.assertEqual(g.scratch_state.piece_state(pawn).face, ShogiFacetype.PAWN)

    def test20_undo_promoted_capture(self):
        g = self.game

        # setup situation
        moves = "B2e-1d P1b-1c B1dx4a+".split(' ')
        for move_str in moves:
            lib.make_move(g, move_str)

        horse = g.scratch_state.piece_at(g.board.Location(g.board, 1, 4))
        assert horse
        self.assertEqual(g.scratch_state.piece_state(horse).face, ShogiFacetype.DRAGONHORSE)

        # capture and undo
        lib.make_move(g, "R5ax4a")
        g.undo_move()

        horse = g.scratch_state.piece_at(g.board.Location(g.board, 1, 4))
        assert horse
        self.assertEqual(g.scratch_state.piece_state(horse).face, ShogiFacetype.DRAGONHORSE)

# FIXME running before test00 makes them fail - likely parameter badly
class test10(unittest.TestCase):
    def test10_handicap(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        g = MiniShogi(parentcontext=app.context,
                      paramvalues=dict(handicap=(ShogiFacetype.ROOK,
                                                 ShogiFacetype.BISHOP)))
        app.game = g # emulates GameApp
        g.setup()
        self.assertEqual(len(g.last_state.pieces), 10)

class test20_loadpgn_fullgame(lib.PGNLoad):
    filename = 'Omaha/test/data/minishogi.pgn'
