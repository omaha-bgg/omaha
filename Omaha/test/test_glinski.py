# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games.GlinskiHexChess import GlinskiHexChess
from Omaha.Games.abstract.HexBoardGame import HexBentlinesNotation
from Omaha import Core
import Overlord

import unittest
from . import lib

class _HexbentNotationTemplate:
    locstr: str
    def setUp(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        self.game = GlinskiHexChess(parentcontext=app.context)
        self.game.setup()
        self.notation = HexBentlinesNotation(self.game)
    def test_backnforth(self):
        loc = self.notation.location(self.locstr)
        locstr2 = self.notation.location_name(loc)
        self.assertEqual(self.locstr, locstr2)

# test de/serializing of all 6 hex corners is at least consistent
class test00_hexbent_notation_a1(_HexbentNotationTemplate, unittest.TestCase):
    locstr = "a1"
class test00_hexbent_notation_f1(_HexbentNotationTemplate, unittest.TestCase):
    locstr = "f1"
class test00_hexbent_notation_l1(_HexbentNotationTemplate, unittest.TestCase):
    locstr = "l1"
class test00_hexbent_notation_a6(_HexbentNotationTemplate, unittest.TestCase):
    locstr = "a6"
class test00_hexbent_notation_f11(_HexbentNotationTemplate, unittest.TestCase):
    locstr = "f11"
class test00_hexbent_notation_l6(_HexbentNotationTemplate, unittest.TestCase):
    locstr = "l6"

class GlinskiTestCase(unittest.TestCase):
    def test10_glinski(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        g = GlinskiHexChess(parentcontext=app.context)
        app.game = g # emulates GameApp
        g.setup()
        g.change_phase(Core.GamePhase.Playing) # FIXME!
        self.assertEqual(g.phase, Core.GamePhase.Playing)

        # replay a small partial game
        # http://play.chessvariants.org/pbm/play.php?game=Glinski%27s+Hexagonal+Chess&log=rlavieri2003-ben_good-2004-80-035
        # uses non-bent notation :(
        #moves = ("Ph3-h5 Ph7-h6 Pg4-g5 Pe8-e7 Pi2-i4 Pi7-i5 Bf1-h3 Pj7-j5 Bf2-h4 Nh9-i6" +
        # " Qe2-k2 f9-h8 Qk2-k5 f10-h9 Nh1-h3 Bh8-j7 Qk5-e11 Kg10-e11 Nd3-g2 j7-k5" +
        # " Bf3-h2 f11-e10 Bh4-e10 e11-e10 Ni3-h1 Bk5-h2 Ri1-h2 Ri8-k6 Ng2-h4 Ni6-h4" +
        # " Rc4-h4 c11-f8 Pd5-d6 d9-d7 Pj1-j3 f8-b8 Pb5-b6 b8-a8 Rh2-g2 d11-c9" +
        # " Pc5-c6 Bh9-f10 Pg5-g6 Ra8-a6 Pg6-f7 Pg7-f7 Ph5-i5 Pj5-i5 Nh1-g4 e10-f8").split(' ')
        moves = ("h3h5 h7h6 g4g5 e7e6 i2i4 i7i5 f1h3 k7k5 f2h4 h9i6" +
                 " e1l2 f9h8 l2l5 f10h9 h1i3 h8k7 l5e10 g10e10 d1g2 k7l5" +
                 " f3h2 f11e9 h4e9 e10e9 i3h1 l5h2 i1h2 i8l6 g2h4 i6h4" +
                 " c1h4 c8f8 d3d4 d7d5 k1k3 f8b4 b1b2 b4a3 h2g2 d9c6" +
                 " c2c3 h9f10 g5g6 a3a1 g6f7 g7f7 h5i5 k5i5 h1g4 e9f8").split(' ')
        for move_str in moves:
            lib.make_move(g, move_str)
        # compare effective moves with original list
        self.assertEqual([g.default_notation.move_serialization(m)
             for m in g.moves.played], moves)
