# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.GameIO.PGNReader import PGNGameReader
from Omaha.Games.abstract.RectBoardGame import RectBoardGame
from Omaha import Core
import Overlord

import logging
logger = logging.getLogger(__name__)

class StubToolkit(Core.UI.Toolkit):
    def notify(self, title, text):
        raise RuntimeError("%s: %s" % (title, text))

class StubRectGame(RectBoardGame):
    player_names = ()

def make_move(g, move_str, notation=None):
    if not notation:
        notation = g.default_notation
    g.make_move(g.scratch_state.whose_turn,
                notation.move_deserialization(g.scratch_state.frozen(), move_str, g.scratch_state.whose_turn))

class PGNLoad:
    filename: str
    def test(self):
        app = Core.UI.App(plugin_manager=None,
                          toolkit=StubToolkit(), parentcontext=Core.Context(None))
        plugin_manager = Overlord.PluginManager("Omaha.plugins")
        with open(self.filename) as f:
            reader = PGNGameReader(plugin_manager,
                                   parentcontext=app.context)
            reader.parse(f)
            g = reader.game()

class FromPGN:
    filename: str
    def setUp(self):
        app = Core.UI.App(plugin_manager=None,
                          toolkit=StubToolkit(), parentcontext=Core.Context(None))
        plugin_manager = Overlord.PluginManager("Omaha.plugins")
        with open(self.filename) as f:
            reader = PGNGameReader(plugin_manager,
                                   parentcontext=app.context)
            reader.parse(f)
            self.game = reader.game()
    def test_undoredo(self):
        nmoves = self.game.moves.nmoves_done
        self.assertNotEqual(nmoves, 0)
        self.game.change_phase(Core.GamePhase.Playing) # FIXME!
        for i in range(nmoves):
            self._undoredo_nmoves(i+1)
    def _undoredo_nmoves(self, n):
        logger.debug('undo/redoing %s moves', n)
        for i in range(n):
            self.game.undo_move()
        for i in range(n):
            self.game.redo_move()
