# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Omaha.Core import UnsatisfiableConstraint
import Overlord
from Overlord.misc import pretty

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Pango', '1.0')
gi.require_version('PangoCairo', '1.0')
gi.require_version('Rsvg', '2.0')
from gi.repository import Gtk as giGtk
from gi.repository import GObject, Gdk, Pango, GLib, PangoCairo
from gi.repository import Rsvg  # type: ignore[attr-defined]
import cairo                    # type: ignore[import]

import os, math, logging
from functools import wraps
import inspect

logger = logging.getLogger(__name__)

class BitmapImage(Core.UI.Toolkit.BitmapImage):
    def __init__(self, width, height):
        self.__img = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
    @property
    def _img(self):
        return self.__img
    @property
    def width(self):
        return self.__img.get_width()
    @property
    def height(self):
        return self.__img.get_height()

    def draw_sized_text(self, x, y, width, height, text, **attrs):
        _draw_sized_text(cairo.Context(self._img),
                         x, y, width, height, text, **attrs)

    def fill_circle(self, center_x, center_y, radius, **attrs):
        _fill_circle(cairo.Context(self._img),
                     center_x, center_y, radius, **attrs)

class VectorImage(Core.UI.VectorImage):
    def __init__(self, svgfilename):
        if not os.path.exists(svgfilename):
            raise RuntimeError("File not found '%s'" % svgfilename)
        self.__svg = Rsvg.Handle.new_from_file(svgfilename)
    @property
    def width(self):
        return self.__svg.props.width
    @property
    def height(self):
        return self.__svg.props.height
    def render_to_bitmap(self, bitmap, x, y, width, height,
                         rotation=None):
        """
        Render vector graphics in bbox, after possible rotation.
        """
        scale = min(width / self.width, height / self.height)
        ctx = cairo.Context(bitmap._img)
        if rotation is not None:
            Gtk.context_rotate(ctx, rotation)
        ctx.scale(scale, scale)
        ctx.translate(x, y)
        self.__svg.render_cairo(ctx)

class SolidFillPattern(Core.UI.SolidFillPattern):
    def __init__(self, color):
        self.pattern = cairo.SolidPattern(color.red, color.green, color.blue, color.alpha)

def _pango_layout_settext(pango_layout, text, color=None, bold=False):
    """Set text in pango layout with given attributes."""
    if color is not None:
        # WTF Gtk, RGBA.to_string() output incompatible with pango!
        rgba_string = "".join(f"{int(255 * value):02X}"
                              for value in (color.red, color.green, color.blue, color.alpha))
        text = f'<span foreground="#{rgba_string}">{text}</span>'
    if bold:
        text = f"<b>{text}</b>"

    # compute text rendering
    pango_layout.set_markup(text)

class GtkStringWidget(Core.UI.ParamWidget):
    def widget(self):
        self.__entry = giGtk.Entry()
        self.__entry.set_text(self.param.value)
        self.__entry.set_property('activates-default', True)
        self.__entry.connect("changed", self.retrieve_value)
        self.__entry.show_all()
        return self.__entry
    def retrieve_value(self, entry=None):
        self.param.value = self.__entry.get_text()

class GtkIntWidget(Core.UI.ParamWidget):
    def widget(self):
        adj = giGtk.Adjustment(step_increment=1, page_increment=2)
        self.__spinbtn = giGtk.SpinButton(adjustment=adj)
        self.__spinbtn.set_numeric(True)
        self.__spinbtn.set_property('activates-default', True)
        self.__spinbtn.connect("value-changed", self.retrieve_value)
        self.update()
        self.__spinbtn.show_all()
        return self.__spinbtn
    def update(self):
        adj = self.__spinbtn.get_adjustment()
        if self.param.decl.has_min:
            adj.set_lower(self.param.decl.min)
        if self.param.decl.has_max:
            adj.set_upper(self.param.decl.max)
        adj.set_value(self.param.value)
    def retrieve_value(self, spinbtn=None):
        self.param.value = self.__spinbtn.get_value_as_int()

class GtkIntWidget2(Core.UI.ParamWidget):
    def widget(self):
        self.__entry = giGtk.Entry()
        self.__entry.set_text(
            "%d" % self.param.value)
        self.__entry.set_property('activates-default', True)
        self.__entry.connect("changed", self.retrieve_value)
        self.__entry.show_all()
        return self.__entry
    def retrieve_value(self, entry=None):
        try:
            self.param.value = int(self.__entry.get_text())
        except ValueError as ex:
            raise Overlord.ParameterError(str(ex))

class GtkFloatWidget(Core.UI.ParamWidget):
    def widget(self):
        self.__entry = giGtk.Entry()
        self.__entry.set_text(
            "%g" % self.param.value)
        self.__entry.set_property('activates-default', True)
        self.__entry.connect("changed", self.retrieve_value)
        self.__entry.show_all()
        return self.__entry
    def retrieve_value(self, entry=None):
        text = self.__entry.get_text()
        if not text:
            self.param.value = 0.
        try:
            self.param.value = float(text)
        except ValueError as ex:
            raise Overlord.ParameterError(str(ex))

class GtkChoiceWidget(Core.UI.ChoiceWidget):
    def widget(self):
        # tell early when we have nothing to choose from
        if len(self.param.decl.alternatives) == 0:
            raise UnsatisfiableConstraint('No choice for "%s".' %
                                          self.param.decl.label)

        self.__liststore = giGtk.ListStore(str, bool)
        self.__combo = giGtk.ComboBox.new_with_model(self.__liststore)
        cell = giGtk.CellRendererText()
        self.__combo.pack_start(cell, True)
        self.__combo.add_attribute(cell, 'text', 0)
        self.__combo.add_attribute(cell, 'sensitive', 1)

        vbox = giGtk.VBox()
        vbox.pack_start(self.__combo, expand=False, fill=True, padding=0)

        # insert items while detecting the index of the current one
        current_value = self.param.value
        for i, (choice, value) in enumerate(self.param.decl.alternatives.items()):
            item = self.__liststore.append([choice, True])
            if value is None:
                self.__liststore.set(item, 1, False)
            if current_value == value:
                # found current, make it active
                self.__combo.set_active(i)
        self.__combo.connect("changed", self.retrieve_value)
        self.__combo.show_all()

        self._add_subparams(self.__combo, vbox)

        return vbox

    def _update_one_subparam_visibility(self, combo, parent_choice, subparam_box, subparam):
        active_iter = self.__combo.get_active_iter()
        if parent_choice == self.__liststore.get_value(active_iter, 0):
            subparam_box.show()
        else:
            subparam_box.hide()

    def retrieve_value(self, combo=None):
        active_iter = self.__combo.get_active_iter()
        active_text = self.__liststore.get_value(active_iter, 0)
        self.param.value = self.param.decl.alternatives[active_text]
        self._retrieve_subparam_values(active_text)

    def connect_changed(self, handler):
        self.__combo.connect("changed", handler)

class GtkPluginChoiceWidget(Core.UI.PluginChoiceWidget):
    def widget(self):
        w = self._choice_widget.widget()
        self._choice_widget.connect_changed(self.retrieve_value)
        return w
    def retrieve_value(self, combo=None):
        self._choice_widget.retrieve_value() # FIXME: now useless ?
        self.param.value = self._choice_widget.param.value

class GtkMultiChoiceWidget(Core.UI.ParamWidget):
    def widget(self):
        ## create the data model
        lstore = giGtk.ListStore(bool, str)
        for choice in self.param.decl.alternatives.keys():
            itor = lstore.append([False, choice])
        ## create the view
        # FIXME: put a ScrolledWindow around ?
        self.__treeview = giGtk.TreeView(model=lstore)
        ## create the columns
        # selected
        def __toggled(cell, path, lstore):
            """Toggle the boolean in the liststore."""
            itor = lstore.get_iter((int(path),))
            lstore.set(itor, 0, not lstore.get_value(itor, 0))
        renderer = giGtk.CellRendererToggle()
        renderer.connect('toggled', __toggled, lstore)
        column = giGtk.TreeViewColumn('Use', renderer, active=0)
        self.__treeview.append_column(column)
        # name
        column = giGtk.TreeViewColumn('Player driver', # FIXME!
                                    giGtk.CellRendererText(),
                                    text=1)
        self.__treeview.append_column(column)
        self.__treeview.show_all()
        return self.__treeview
    def retrieve_value(self, treeview=None):
        self.param.value = []
        def __handle_node(lstore, path, itor):
            """If node was selected, append its label to param.value."""
            if lstore.get_value(itor, 0):
                self.param.value.append(self.param.decl.alternatives[
                    lstore.get_value(itor, 1)])
        self.__treeview.get_model().foreach(__handle_node)

class Gtk(Core.UI.Toolkit):
    """
    Omaha abstraction of the Gtk toolkit.
    """

    BitmapImage = BitmapImage
    VectorImage = VectorImage
    SolidFillPattern = SolidFillPattern

    # parameter handling
    RawLabelWidget = giGtk.Label
    StringWidget = GtkStringWidget
    IntWidget = GtkIntWidget
    FloatWidget = GtkFloatWidget
    ChoiceWidget = GtkChoiceWidget
    MultiChoiceWidget = GtkMultiChoiceWidget
    PluginChoiceWidget = GtkPluginChoiceWidget

    def __init__(self):
        super(Gtk, self).__init__()
        self.__event_sources = {}

        self.__mouse_release_callbacks = {}
        self.__mouse_press_callbacks = {}
        self.__mouse_move_callbacks = {}
        self.__colors = {}

    # generic GUI elements

    def notify(self, title, text):
        dlg = giGtk.Dialog(title="omaha - " + title)
        dlg.add_buttons(giGtk.STOCK_OK, giGtk.ResponseType.ACCEPT)
        dlg.vbox.pack_start(giGtk.Label(label=text), expand=True, fill=True, padding=0)
        dlg.vbox.show_all()
        dlg.run()
        dlg.destroy()

    def askuser_yesno(self, text):
        dlg = giGtk.Dialog(title="omaha - confirm")
        dlg.add_buttons(giGtk.STOCK_YES, giGtk.ResponseType.ACCEPT,
                        giGtk.STOCK_NO, giGtk.ResponseType.REJECT)
        dlg.vbox.pack_start(giGtk.Label(text), expand=True, fill=True, padding=0)
        dlg.vbox.show_all()
        try:
            answer = dlg.run()
            return answer == giGtk.ResponseType.ACCEPT
        finally:
            dlg.destroy()

    def select_load_file(self, callback):
        def wrapped_callback(dialog, response_id):
            if response_id == giGtk.ResponseType.OK:
                callback(dialog.get_filename())
            dialog.destroy()
        dlg = giGtk.FileChooserDialog(title="omaha - load game",
                                      action=giGtk.FileChooserAction.OPEN)
        dlg.add_buttons(giGtk.STOCK_CANCEL, giGtk.ResponseType.REJECT,
                        giGtk.STOCK_OPEN, giGtk.ResponseType.OK)
        dlg.connect("response", wrapped_callback)
        dlg.show()

    def select_save_file(self, callback):
        def wrapped_callback(dialog, response_id):
            filename = dialog.get_filename() if response_id == giGtk.ResponseType.OK else None
            callback(filename)
            dialog.destroy()
        dlg = giGtk.FileChooserDialog(title="omaha - save game",
                                      action=giGtk.FileChooserAction.SAVE)
        dlg.add_buttons(giGtk.STOCK_CANCEL, giGtk.ResponseType.REJECT,
                        giGtk.STOCK_OK, giGtk.ResponseType.OK)
        dlg.set_do_overwrite_confirmation(True)
        dlg.connect("response", wrapped_callback)
        dlg.show()

    def open_params_dialog(self, title, params, app_ui, accept_cb=None, refuse_cb=None,
                           parent_plugin=None, parent_window=None,
                           destroy=True, synchronous=False):
        dlg = giGtk.Dialog(title=title)
        dlg.add_buttons(giGtk.STOCK_CANCEL, giGtk.ResponseType.REJECT,
                        giGtk.STOCK_OK, giGtk.ResponseType.ACCEPT)
        dlg.set_default_response(giGtk.ResponseType.ACCEPT)
        scroll = giGtk.ScrolledWindow()
        dlg_space = giGtk.VBox()
        scroll.add(dlg_space)
        scroll.set_policy(giGtk.PolicyType.NEVER, giGtk.PolicyType.AUTOMATIC)
        scroll.set_propagate_natural_height(True)
        scroll.show_all()
        dlg.vbox.pack_start(scroll, expand=False, fill=True, padding=0)

        paramwidgets = {}
        def walk_action(param):
            hbox, paramwidgets[param] = self.create_param_widget(param, app_ui, params)
            dlg_space.pack_start(hbox, expand=False, fill=True, padding=0)
        Overlord.walk_params(params, parent_plugin, walk_action)
        requires_interaction = (len(paramwidgets) > 0)

        def __response_handler(dlg, response_id,
                               params, paramwidgets, accept_cb,
                               parent_window, destroy):
            def __cleanup(dlg, parent_window):
                if destroy:
                    dlg.destroy()
                    if parent_window:
                        parent_window.set_sensitive(True)

            if response_id in (giGtk.ResponseType.REJECT, giGtk.ResponseType.DELETE_EVENT):
                logger.debug("cancelled")
                __cleanup(dlg, parent_window)
                if refuse_cb:
                    refuse_cb()
                return
            if response_id != giGtk.ResponseType.ACCEPT:
                __cleanup(dlg, parent_window)
                raise RuntimeError("Gtk.Dialog responded unexpected %r" %
                                   giGtk.ResponseType(response_id))

            # Retrieve values from Widget objects, for params which
            # were allocated a Widget.
            for param in params.values():
                if param in paramwidgets:
                    paramwidgets[param].retrieve_value()

            if accept_cb:
                try:
                    accept_cb(params)
                except Overlord.ParameterError as ex:
                    # FIXME: should notify user
                    logger.error("Bad parameter value: {0}".format(ex))
                    return

            __cleanup(dlg, parent_window)

        if requires_interaction:
            if parent_window:
                dlg.set_transient_for(parent_window)
            if synchronous:
                response_id = dlg.run()
                __response_handler(dlg, response_id,
                                   params, paramwidgets, accept_cb,
                                   parent_window, destroy)
                return {
                    giGtk.ResponseType.ACCEPT: True,
                    giGtk.ResponseType.REJECT: False,
                    giGtk.ResponseType.DELETE_EVENT: False,
                }[response_id]
            else:
                dlg.connect("response", __response_handler,
                            params, paramwidgets, accept_cb,
                            parent_window, destroy)
                dlg.show()
        else:
            __response_handler(dlg, giGtk.ResponseType.ACCEPT,
                               params, paramwidgets, accept_cb,
                               parent_window, destroy)

        return dlg

    def create_param_widget(self, param, app_ui, params):
        """Create widget for editing param in the context of app_ui.

        Both are included in a frame, which is returned so caller can place
        it where it wants.  Returns a tuple of frame and ParamWidget instance.
        """
        assert isinstance(param, Overlord.Param)
        box = giGtk.Frame.new(param.label)
        box.set_property("border_width", 4)
        w = self._create_widget(param, app_ui, params)
        wdg = w.widget()
        wdg.show()
        box.add(wdg)
        box.show()
        return box, w

    # FIXME: hackish things
    @staticmethod
    def _vbox_add(vbox, widget):
        vbox.pack_start(widget, expand=False, fill=True, padding=0)
    @staticmethod
    def _widget_hide(widget: object) -> None:
        assert isinstance(widget, giGtk.Widget)
        widget.hide()
    @staticmethod
    def _combo_setchanged_hook(combo, callback, param):
        combo.connect("changed", callback, param)

    ## main event loop

    def run(self):
        giGtk.main()

    def set_stream_callback(self, cbtype, stream, callback, arg=None):
        key = (cbtype, stream, callback)
        assert key not in self.__event_sources
        def wrapped_callback(stream, condition):
            callback(stream, arg)
            return True
        mask = {Core.UI.Toolkit.CallbackType.READ: GLib.IO_IN,
                Core.UI.Toolkit.CallbackType.ERROR: GLib.IO_NVAL|GLib.IO_ERR|GLib.IO_HUP,
        }[cbtype]
        self.__event_sources[key] = \
            GLib.io_add_watch(stream, GLib.PRIORITY_DEFAULT, mask, wrapped_callback)
    def unset_stream_callback(self, cbtype, stream, callback):
        key = (cbtype, stream, callback)
        GLib.source_remove(self.__event_sources[key])
        del self.__event_sources[key]

    def set_timer_callback(self, timeout, callback):
        key = (Core.UI.Toolkit.CallbackType.TIMER, callback)
        assert key not in self.__event_sources
        def wrapped_callback():
            logger.debug("timer expired: %r sec, %r", timeout, callback)
            callback()
            # do not autoremove
            return True
        self.__event_sources[key] = \
            GLib.timeout_add_seconds(timeout, wrapped_callback)
    def unset_timer_callback(self, callback):
        key = (Core.UI.Toolkit.CallbackType.TIMER, callback)
        GLib.source_remove(self.__event_sources[key])
        del self.__event_sources[key]

    ## canvas interaction

    def set_mouse_release_callback(self, canvas, callback):
        def wrapped_callback(w, evt):
            if callback(int(evt.x), int(evt.y)):
                canvas.stop_emission("button_release_event")
                self.invalidate_canvas(canvas)
        self.__mouse_release_callbacks[callback] = canvas.connect(
            "button_release_event", wrapped_callback)
    def unset_mouse_release_callback(self, canvas, callback):
        if callback not in self.__mouse_release_callbacks:
            logger.error("unset_mouse_release_callback: %s not in %s",
                         callback, self.__mouse_release_callbacks)
            return
        canvas.disconnect(self.__mouse_release_callbacks[callback])
        del self.__mouse_release_callbacks[callback]

    def set_mouse_press_callback(self, canvas, callback):
        def wrapped_callback(w, evt):
            if callback(int(evt.x), int(evt.y)):
                canvas.stop_emission("button_press_event")
                self.invalidate_canvas(canvas)
        self.__mouse_press_callbacks[callback] = canvas.connect(
            "button_press_event", wrapped_callback)
    def unset_mouse_press_callback(self, canvas, callback):
        if callback not in self.__mouse_press_callbacks:
            logger.error("unset_mouse_press_callback: %s not in %s",
                         callback, self.__mouse_press_callbacks)
            return
        canvas.disconnect(self.__mouse_press_callbacks[callback])
        del self.__mouse_press_callbacks[callback]

    def set_mouse_move_callback(self, canvas, callback):
        def wrapped_callback(w, evt):
            if evt.is_hint:
                _, x, y, state = evt.window.get_pointer() # first is "GdkWindow or None"
            else:
                x, y, state = evt.x, evt.y, evt.state
            if callback(int(x), int(y),
                        pressed=(evt.state & Gdk.ModifierType.BUTTON1_MASK)):
                canvas.stop_emission("motion_notify_event")
                self.invalidate_canvas(canvas)
        self.__mouse_move_callbacks[callback] = canvas.connect(
            "motion_notify_event", wrapped_callback)
    def unset_mouse_move_callback(self, canvas, callback):
        if callback not in self.__mouse_move_callbacks:
            logger.error("unset_mouse_move_callback: %s not in %s",
                         callback, self.__mouse_move_callbacks)
            return
        canvas.disconnect(self.__mouse_move_callbacks[callback])
        del self.__mouse_move_callbacks[callback]

    ## canvas drawing

    @staticmethod
    def context_rotate(drawing_context, rotation):
        """
        Apply rotation around a point to drawing_context's CTM.

        Rotation parameter must be a tuple of (center_x, center_y, radians).
        """
        cx, cy, angle = rotation
        drawing_context.translate(cx, cy)
        drawing_context.rotate(angle)
        drawing_context.translate(-cx, -cy)
    @staticmethod
    def context_scale(drawing_context, factor, center):
        """
        Apply scaling around a point to drawing_context's CTM.

        Center parameter must be a tuple of (center_x, center_y).
        """
        cx, cy = center
        drawing_context.translate(cx, cy)
        drawing_context.scale(factor, factor)
        drawing_context.translate(-cx, -cy)

    def clear(self, drawing_context):
        drawing_context.fill()

    def invalidate_canvas(self, canvas):
        canvas.get_window().invalidate_rect(canvas.get_allocation(), True)

    def color(self, canvas, colorname):
        if colorname not in self.__colors:
            color = Gdk.RGBA()
            color.parse(colorname)
            self.__colors[colorname] = color
        return self.__colors[colorname]

    def color_transparent(self, color, alpha):
        ret = color.copy()
        ret.alpha *= alpha
        return ret

    def draw_centered_text(self, drawing_context,
                           x, y, text, color=None, bold=False, height=None):
        pango_layout = PangoCairo.create_layout(drawing_context)
        _pango_layout_settext(pango_layout, text, color, bold)
        if height:
            fontdesc = Pango.FontDescription('Sans')
            fontdesc.set_absolute_size(height * Pango.SCALE)
            pango_layout.set_font_description(fontdesc)
        markup_rect = pango_layout.get_pixel_extents()[1]
        markup_width, markup_height = markup_rect.width, markup_rect.height

        # render at given position
        drawing_context.move_to(x - markup_width // 2, y - markup_height // 2)
        PangoCairo.show_layout(drawing_context, pango_layout)

    def draw_sized_text(self, drawing_context, x, y, width, height, text, **attrs):
        _draw_sized_text(drawing_context,
                         x, y, width, height, text, **attrs)

    def draw_image(self, drawing_context, image, x, y):
        drawing_context.set_source_surface(image._img, x, y)
        drawing_context.paint()
        drawing_context.stroke()

    def draw_line(self, drawing_context, x0, y0, x1, y1, **attrs):
        _draw_line(drawing_context, x0, y0, x1, y1, **attrs)
    def draw_rectangle(self, drawing_context, x, y, w, h, **attrs):
        _draw_rectangle(drawing_context, x, y, w, h, **attrs)
    def fill_rectangle(self, drawing_context, x, y, w, h, **attrs):
        _fill_rectangle(drawing_context, x, y, w, h, **attrs)
    def draw_rounded_rectangle(self, drawing_context, x, y, w, h, radius, **attrs):
        _draw_rounded_rectangle(drawing_context, x, y, w, h, radius, **attrs)
    def fill_rounded_rectangle(self, drawing_context, x, y, w, h, radius, **attrs):
        _fill_rounded_rectangle(drawing_context, x, y, w, h, radius, **attrs)
    def draw_circle(self, drawing_context, center_x, center_y, radius, **attrs):
        _draw_circle(drawing_context, center_x, center_y, radius, **attrs)
    def fill_circle(self, drawing_context, center_x, center_y, radius, **attrs):
        _fill_circle(drawing_context, center_x, center_y, radius, **attrs)
    def draw_polygon(self, drawing_context, pointlist, **attrs):
        _draw_polygon(drawing_context, pointlist, **attrs)
    def fill_polygon(self, drawing_context, pointlist, **attrs):
        _fill_polygon(drawing_context, pointlist, **attrs)

# poor man's (ie. gtk's...) nesting
def __canvas_begin_subcontext(parent):
    parent.save()
    return parent
def __canvas_end_subcontext(context):
    context.restore()

def __context_set_attributes(cairo_context, attrs):
    for aname, avalue in attrs.items():
        if aname == 'color':
            cairo_context.set_source_rgba(
                avalue.red, avalue.green, avalue.blue, avalue.alpha)
        elif aname == 'width':
            cairo_context.set_line_width(avalue)
        elif aname == 'rotation':
            if avalue != None:
                Gtk.context_rotate(cairo_context, avalue)
        elif aname == 'fillpattern':
            cairo_context.set_source(avalue.pattern)
        elif aname in {'bold'}:
            pass # not handled at this level, but not rejected
        else:
            raise KeyError("drawing attribute '%s' not supported" % (aname,))

def cairo_operation(func):
    "Decorator to spare cairo boilerplate code"
    assert callable(func)
    sig = inspect.signature(func)
    first_argname = next(iter(sig.parameters))
    assert first_argname == 'drawing_context'
    last_arg = next(reversed(sig.parameters.values()))
    assert last_arg.kind == inspect.Parameter.VAR_KEYWORD
    assert last_arg.name == 'attrs'
    @wraps(func)
    def wrapper(drawing_context, *args, **attrs):
        __canvas_begin_subcontext(drawing_context)
        __context_set_attributes(drawing_context, attrs)
        func(drawing_context, *args, **attrs) # pylint: disable=not-callable
        __canvas_end_subcontext(drawing_context)
    return wrapper

@cairo_operation
def _draw_sized_text(drawing_context, x, y, width, height,
                     text, **attrs):
    if not text:
        return

    color = attrs['color'] if 'color' in attrs else None
    bold = attrs['bold'] if 'bold' in attrs else False
    assert isinstance(bold, bool)

    pango_layout = PangoCairo.create_layout(drawing_context)
    _pango_layout_settext(pango_layout, text, color, bold)

    # get size of default-size rendering of text
    markup_width, markup_height = pango_layout.get_pixel_size()

    # determine scaling factor to get into requested bounding-box
    scale = min(width / markup_width, height / markup_height)

    # center text inside bounding box
    x += (width - scale * markup_width) / 2
    y += (height - scale * markup_height) / 2

    # render using computed position and scale
    drawing_context.move_to(x, y)
    drawing_context.scale(scale, scale)
    PangoCairo.show_layout(drawing_context, pango_layout)

@cairo_operation
def _draw_line(drawing_context, x0, y0, x1, y1, **attrs):
    drawing_context.move_to(x0, y0)
    drawing_context.line_to(x1, y1)
    drawing_context.stroke()
@cairo_operation
def _draw_rectangle(drawing_context, x, y, w, h, **attrs):
    drawing_context.rectangle(x, y, w, h)
    drawing_context.stroke()
@cairo_operation
def _fill_rectangle(drawing_context, x, y, w, h, **attrs):
    drawing_context.rectangle(x, y, w, h)
    drawing_context.fill()

def __rounded_rectangle_path(drawing_context, x, y, w, h, radius):
    drawing_context.new_sub_path();
    drawing_context.arc(x + w - radius, y + radius, radius, -math.pi / 2, 0);
    drawing_context.arc(x + w - radius, y + h - radius, radius, 0, math.pi / 2);
    drawing_context.arc(x + radius, y + h - radius, radius, math.pi / 2, math.pi);
    drawing_context.arc(x + radius, y + radius, radius, math.pi, 3 * math.pi / 2);
    drawing_context.close_path();
@cairo_operation
def _draw_rounded_rectangle(drawing_context, x, y, w, h, radius, **attrs):
    __rounded_rectangle_path(drawing_context, x, y, w, h, radius)
    drawing_context.stroke()
@cairo_operation
def _fill_rounded_rectangle(drawing_context, x, y, w, h, radius, **attrs):
    __rounded_rectangle_path(drawing_context, x, y, w, h, radius)
    drawing_context.fill()

@cairo_operation
def _draw_circle(drawing_context, center_x, center_y, radius, **attrs):
    drawing_context.arc(center_x, center_y, radius,
                        0, math.radians(360))
    drawing_context.stroke()
@cairo_operation
def _fill_circle(drawing_context, center_x, center_y, radius, **attrs):
    drawing_context.arc(center_x, center_y, radius,
                        0, math.radians(360))
    drawing_context.fill()
@cairo_operation
def _draw_polygon(drawing_context, pointlist, **attrs):
    x0, y0 = pointlist[0]
    drawing_context.move_to(x0, y0)
    for x, y in pointlist[1:]:
        drawing_context.line_to(x, y)
    drawing_context.close_path()
    drawing_context.stroke()
@cairo_operation
def _fill_polygon(drawing_context, pointlist, **attrs):
    x0, y0 = pointlist[0]
    drawing_context.move_to(x0, y0)
    for x, y in pointlist[1:]:
        drawing_context.line_to(x, y)
    drawing_context.close_path()
    drawing_context.fill()
