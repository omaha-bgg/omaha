# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import logging
import numpy
import sys
if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

from Omaha.Core.misc import sign
from Omaha import Core
from frozendict import frozendict

logger = logging.getLogger(__name__)

class _LocationDelta:
    def __init__(self, dcol, drow):
        assert isinstance(dcol, int)
        assert isinstance(drow, int)
        self.dcol, self.drow = dcol, drow

    def __mul__(self, scalar):
        if not isinstance(scalar, int):
            return NotImplemented
        return _LocationDelta(scalar * self.dcol, scalar * self.drow)

    @property
    def normalized(self):
        """Normalize self to "one step"."""
        # Only implemented for orthogonal moves
        if self.dcol != 0 and self.drow != 0:
            raise NotImplementedError("Not orthogonal Delta.")
        return type(self)(sign(self.dcol), sign(self.drow))

    def xformed(self, xform):
        v = xform * Vector.from_location_delta(self)
        return type(self)(v.x, v.y)

    def __str__(self):
        return "Euclidian2DLocation.Delta(%s, %s)" % (self.dcol, self.drow)

class Euclidian2DLocation(Core.PieceHolder.Location):
    Delta = _LocationDelta
    def __init__(self, holder, col, row):
        assert isinstance(holder, Euclidian2D), "%r is not a Euclidian2D" % holder
        assert isinstance(col, int)
        assert isinstance(row, int)
        super().__init__(holder)
        self.__col = col
        self.__row = row
    @property
    def col(self):
        return self.__col
    @property
    def row(self):
        return self.__row
    def __str__(self):
        return "(%d,%d)" % (self.col, self.row)
    def __hash__(self):
        "Return unique value for identical locations."
        return (self.col, self.row).__hash__()
    def __eq__(self, other):
        if type(other) is not type(self):
            return NotImplemented
        return self.col == other.col and self.row == other.row
    def __ne__(self, other):
        if type(other) is not type(self):
            return NotImplemented
        return self.col != other.col or self.row != other.row

    def __sub__(self, other):
        if not isinstance(other, Euclidian2DLocation):
            return NotImplemented
        return self.Delta(self.col - other.col, self.row - other.row)
    def __add__(self, other):
        if not isinstance(other, self.Delta):
            return NotImplemented
        return Euclidian2DLocation(self.holder,
                                   self.col + other.dcol,
                                   self.row + other.drow)

class Vector():
    def __init__(self, vector):
        "Create Vector from a numpy array"
        assert isinstance(vector, numpy.ndarray)
        assert vector.shape == (3,)
        self.vector = vector
    @classmethod
    def make_vector(cls, x, y):
        return cls(numpy.array([x, y, 1]))
    @classmethod
    def from_location_delta(cls, delta):
        assert isinstance(delta, _LocationDelta)
        return cls.make_vector(delta.dcol, delta.drow)
    @classmethod
    def from_location(cls, location):
        assert isinstance(location, Euclidian2DLocation)
        return cls.make_vector(location.col, location.row)

    @property
    def x(self):
        return self.vector.item(0)
    @property
    def y(self):
        return self.vector.item(1)

class Xform():
    def __init__(self, matrix):
        "Create Xform from a numpy array"
        assert isinstance(matrix, numpy.ndarray)
        assert matrix.shape == (3, 3)
        self.matrix = matrix
    @classmethod
    def from_array(cls, array):
        "New Xform from a 3x3 array matrix"
        return cls(numpy.array(array))
    @classmethod
    def from_22array(cls, array22):
        "New Xform from a 2x2 array matrix"
        return cls.from_array([(x, y, 0) for x, y in array22] + [(0, 0, 1)])
    @classmethod
    def make_translation(cls, tx, ty):
        return cls.from_array( ((1, 0, tx),
                                (0, 1, ty),
                                (0, 0, 1 )) )

    def __mul__(self, other):
        "Multiplication by a matrix or vector."
        if isinstance(other, Xform):
            return Xform(self.matrix @ other.matrix)
        if isinstance(other, Vector):
            return Vector(self.matrix @ other.vector)
        return NotImplemented
    def __add__(self, other):
        if isinstance(other, Xform):
            return Xform(self.matrix + other.matrix)
        return NotImplemented
    def __rmul__(self, other):
        # fully delegate to numpy
        return Xform(other @ self.matrix)

    def __str__(self):
        return str(self.matrix)

class HolderState(Core.HolderState):
    def __init__(self, holder):
        super().__init__(holder)
        self.__array = None # array holding pieces hashed by location
        self.__piece_locations = {} # reverse mapping
        self.clear()

    def clone(self):
        c = super().clone()
        c.clear()
        c.__array = [ [contents.clone() for contents in row]
                      for row in self.__array]
        c.__piece_locations = dict(self.__piece_locations)
        return c
    def freeze(self):
        super().freeze()
        self.__array = tuple(tuple(contents.frozen() for contents in row)
                               for row in self.__array)
        self.__piece_locations = frozendict(self.__piece_locations)

    def clear(self):
        """Set all board squares as empty."""
        self.__array = [ [self.LocationContents() for j in range(self.holder.matrix_width)]
                           for i in range(self.holder.matrix_height) ]

    def put_piece(self, location, piece):
        super().put_piece(location, piece)
        self.__array[location.row][location.col].add_piece(piece)
        self.__piece_locations[piece] = location
    def take_piece(self, piece):
        super().take_piece(piece)
        location = self.piece_location(piece)
        self.__array[location.row][location.col].remove_piece(piece)
        del self.__piece_locations[piece]

    def valid_location(self, location):
        # check bounds first, as upcall may then use the Location object
        if  (location.col >= self.holder.matrix_width or location.col < 0 or
             location.row >= self.holder.matrix_height or location.row < 0):
            return False
        if not super().valid_location(location):
            return False
        return True

    def contents_at(self, location):
        return self.__array[location.row][location.col]
    def piece_location(self, piece):
        """Returns location if piece is in board, else None."""
        if piece not in self.__piece_locations:
            return None
        return self.__piece_locations[piece]

    @property
    def all_locations(self):
        for column in range(self.holder.matrix_width):
            for row in range(self.holder.matrix_height):
                loc = self.holder.Location(self.holder, column, row)
                if self.valid_location(loc):
                    yield loc

    @property
    def pieces(self):
        return self.__piece_locations.keys()

    def __str__(self):
        gamestate = self.holder.game.scratch_state
        piece_notation = self.holder.game.default_notation.piece_notation
        return "[ " + (" / ".join(" ".join((piece_notation.piece_name(gamestate.piece_state(piece))
                                         if piece else ".")
                                        for piece in row)
                               for row in self.__array)) + " ]"

class Euclidian2D(Core.PieceHolder):
    """
    Base class for 2D matrix-based boards.
    """
    @property
    def matrix_width(self):
        raise NotImplementedError()
    @property
    def matrix_height(self):
        raise NotImplementedError()

    Location: TypeAlias = Euclidian2DLocation
    State: TypeAlias = HolderState

    def post_init(self, **kwargs):
        super().post_init(**kwargs)
        self.__locdescs = [ [self.LocationDesc() for j in range(self.matrix_width)]
                            for i in range(self.matrix_height) ]
        # FIXME should freeze __locdescs in __new__ if this could be in __init__

    def location_desc(self, location):
        return self.__locdescs[location.row][location.col]

    # FIXME: does not deal correctly with "no move"
    def move_follows_direction(self, move, direction):
        """
        Checks whether the given move follows the given direction.
        """
        assert move.source.holder is self
        assert move.target.holder is self
        assert isinstance(direction, self.Location.Delta)

        #print("checking %s against direction %s" % (move, direction))
        delta = move.target - move.source

        def _samesign(x, y):
            return not (x > 0) ^ (y > 0)

        # vertical move ?
        if direction.dcol == 0:
            if delta.dcol != 0:
                return False
            return _samesign(delta.drow, direction.drow)
        elif delta.dcol == 0:
            return False

        # here we already know there is a non-zero horiz component,
        # filter by sign early
        if not _samesign(delta.dcol, direction.dcol):
            return False

        # horizontal move ?
        if direction.drow == 0:
            return delta.drow == 0 # delta.dcol sign already checked
        elif delta.drow == 0:
            return False

        return ((delta.dcol % direction.dcol == 0) and
                (delta.drow % direction.drow == 0) and
                (delta.dcol // direction.dcol == delta.drow // direction.drow))

    def nunits_in_direction(self, move, direction):
        """
        Returns the number of units made by move in given direction.

        Assumes self.has_direction(move, direction).  Returns None if
        not an integer multiple of direction vector.
        """
        assert move.source.holder is self
        assert move.target.holder is self
        assert isinstance(direction, self.Location.Delta)

        delta = move.target - move.source

        if direction.dcol == 0:
            q, r = divmod(delta.drow, direction.drow)
        else:
            q, r = divmod(delta.dcol, direction.dcol)

        if r == 0:
            return q
        else:
            return None
