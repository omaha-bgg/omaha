# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from enum import Enum
import logging
import sys
if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

from .abstract.Euclidian2D import Euclidian2D
from Omaha import Core
import Overlord

logger = logging.getLogger(__name__)

class HolderState(Euclidian2D.State):

    @property
    def leftmost_row(self):
        """Find the 1st row of hex that will touch the left side."""
        # iterate on the tiles in 1st column, last first
        for i in reversed(range(self.holder.matrix_height)):
            if self.valid_location(self.holder.Location(self.holder, 0, i)):
                return i
        assert False, "at least one row should reach col 0"
    @property
    def topmost_col(self):
        """Find the 1st column of hex that will touch the top side."""
        # iterate on the tiles in 1st row, last first
        for i in reversed(range(self.holder.matrix_width)):
            if self.valid_location(self.holder.Location(self.holder, i, 0)):
                return i
        assert False, "at least one column should reach row 0"

    def __ascii_dump(self):
        """For debugging or understanding the internal representation of
        the board."""
        for vcol in range(self.holder.matrix_width):
            for vrow in range(self.holder.matrix_height):
                if self.__hexes[vcol][vrow] is None:
                    # empty hex
                    print("O")
                else:
                    # not an hex
                    print(".")

class HexBoard(Euclidian2D):
    """
    Hexagonal board made of hexagonal tiles.

    The Hex board is stored into a square matrix, as rows shifted by
    progressive multiples of half squares.
    """
    State: TypeAlias = HolderState

    def __init__(self, *, width0: int, width1: int, width2: int, **kwargs):
        super().__init__(**kwargs)
        self.__width0, self.__width1, self.__width2 = width0, width1, width2

    def post_init(self, **kwargs):
        """Set all board hexes as empty, and out-of-hex locations as not available."""
        super().post_init(**kwargs)
        for vcol in range(self.matrix_width):
            for vrow in range(self.matrix_height):
                if vrow >= self.__width1 + vcol or vrow <= vcol - self.__width0:
                    location = self.Location(self, vcol, vrow)
                    self.location_desc(location).set_available(False)

    @property
    def width0(self) -> int:
        return self.__width0
    @property
    def width1(self) -> int:
        return self.__width1
    @property
    def width2(self) -> int:
        return self.__width2

    @property
    def matrix_width(self) -> int:
        """Width of the storage matrix."""
        return self.__width0 + self.__width2 - 1

    @property
    def matrix_height(self) -> int:
        """Width of the storage matrix."""
        return self.__width1 + self.__width2 - 1

    VHexSide = Enum('HexBoard.VHexSide', 'TOPRIGHT RIGHT BOTRIGHT BOTLEFT LEFT TOPLEFT')
    HHexSide = Enum('HHexSide.HHexSide', 'TOP TOPRIGHT BOTRIGHT BOT BOTLEFT TOPLEFT')


class OnePiecePerLocHexBoard(HexBoard, Core.OnePiecePerLocHolder):
    class State(HexBoard.State, Core.OnePiecePerLocHolder.State):
        pass
