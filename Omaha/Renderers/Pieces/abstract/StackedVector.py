# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Overlord import plugin_pattern
import Overlord
import os.path

class _ImageSet:
    """
    A set of images of a given size, indexed by their name.

    Used to cache bitmap-rendered version of SVG images.
    """
    def __init__(self, size, rotation=0):
        assert size > 0, "invalid size {0}".format(size)
        self.__size = size
        self.__rotation = rotation
        # a dict of (name => image)
        self.__images = {}
    @property
    def size(self):
        """The size of images in this set."""
        return self.__size
    @property
    def rotation(self):
        """The angle by which pieces are rotated when drawing."""
        return self.__rotation

    def set_img(self, name, image):
        """Set image for given name."""
        self.__images[name] = image
    def get_img(self, name):
        """Retrieve image for given name, or None if not set."""
        if name in self.__images:
            return self.__images[name]
        return None

# FIXME: the "path" parameter has no meaning for most layer ops
def svgLayer(tk, path, img, imgdesc, angle=0):
    desc_x, desc_y, desc_w, desc_h, desc_root = imgdesc
    fname = os.path.join(path, desc_root)

    # try default suffixes if necessary
    if not os.path.exists(fname):
        for suffix in ('.svg', '.svgz'):
            if not fname.endswith(suffix):
                try_fname = fname + suffix
                if os.path.exists(try_fname):
                    fname = try_fname

    vimg = tk.VectorImage(fname)
    vimg.render_to_bitmap(img,
                          desc_x * img.width // 100,  # x
                          desc_y * img.height // 100, # y
                          desc_w * img.width // 100,  # width
                          desc_h * img.height // 100, # height
                          rotation = (img.width // 2, img.height // 2, angle)
                          if angle != 0 else None
                         )

def textLayer(tk, path, img, imgdesc, angle=0):
    """imgdesc = (x, y, width, height, text, color)."""
    desc_x, desc_y, desc_w, desc_h, desc_text, desc_color = imgdesc
    # the requested bounding box
    x, y = desc_x * img.width // 100, desc_y * img.height // 100
    width, height = max(1, desc_w * img.width // 100), max(1, desc_h * img.height // 100)
    img.draw_sized_text(x, y, width, height,
                        desc_text,
                        color = tk.color(img, desc_color),
                        rotation = (img.width // 2, img.height // 2, angle)
                        if angle != 0 else None
                       )

def discLayer(tk, path, img, imgdesc, angle=0):
    """imgdesc = (xc, yc, radius, fillpattern)."""
    desc_x, desc_y, desc_r, desc_fillpattern = imgdesc
    # the requested bounding box
    x, y = desc_x * img.width / 100, desc_y * img.height / 100
    radius = max(0.5, desc_r * min(img.width, img.height) / 100)
    img.fill_circle(x, y, radius, fillpattern=desc_fillpattern)

class StackedVectorPieceRenderer(Core.PieceRenderer):
    """
    A piece renderer with one SVG drawing per piece.

    This class is driven by a game renderer which defines its own
    schema definition for piece-skins (filenames and such).

    Several sets of pieces may be renderer, they are distinguished by
    a set_id (may be anything hashable).  Each set is defined by the
    skin_schema, in which pieces are hashed in a 2-level dist by
    "kind" (ie. color or whatever) and type.

    FIXME: skin mechanism really assumes exactly one svgLayer.
    """

    def __init__(self, **kwargs):
        """
        Create a piece renderer using self.piece_skin_categ and self.skin_schema.
        """
        super(StackedVectorPieceRenderer, self).__init__(**kwargs)
        self.__invalidate_image_cache()

    def __invalidate_image_cache(self):
        self.__image_cache = {}

    def post_init(self, **kwargs):
        super().post_init(**kwargs)
        # invalidate image cache on skin change
        skin_param = type(self).skin.fget_param(self)
        skin_param.register_changed(lambda param, oldvalue: self.__invalidate_image_cache())

    @staticmethod
    def skin_min_colors(context):
        """Minimum number of colors for a skin to be considered.

        Defaults to the number of players."""
        return len(context.game_plugin.Class.player_names)

    @Overlord.parameter
    def skin(cls, context):
        cls_plugin = getattr(cls, 'x-plugin-desc')
        default = cls_plugin.datafield("X-Omaha-DefaultSkin") or None

        # FIXME: should use mro ?
        return Overlord.params.Plugin(
            label = 'Piece skin',
            plugintype = 'X-Omaha-PieceSkin',
            pythonplugin=False,
            context = context,
            pattern = plugin_pattern.And(
                plugin_pattern.ParentOf('X-Omaha-PieceRenderer-Categories', cls),
                plugin_pattern.NumericGreaterEqual('X-Omaha-PieceSkin-ColorCount',
                                                   cls.skin_min_colors(context))
            ),
            default_by_name = default,
        )

    def set_piece_size(self, set_id, newsize, angle=0):
        """
        Make _ImageSet with given set_id use given size.

        Previous _ImageSet is discarded if it did not have the same
        image size.
        """
        # invalidate cache of rendered SVG if size changed
        if   (not set_id in self.__image_cache
              or self.__image_cache[set_id].size != newsize
              or self.__image_cache[set_id].rotation != angle):
            self.__image_cache[set_id] = _ImageSet(newsize, angle)

    def draw_piece(self, drawing_context, gamestate, center, set_id, piece):
        """
        Draw image specified by key, centered around center.
        """
        self.gui.tk.draw_image(
            drawing_context,
            self.__image(set_id, self.skin_schema(self._piece_key(gamestate, piece))),
            center[0] - self.__image_cache[set_id].size // 2,
            center[1] - self.__image_cache[set_id].size // 2)

    def _piece_key(self, gamestate, piece):
        return NotImplementedError()

    def skin_schema(self, piece_key):
        return NotImplementedError()

    def __image(self, set_id, imgdesc):
        """Retrieve image for set, using the cache if possible."""
        if self.__image_cache[set_id].get_img(imgdesc) is None:
            # create bitmap image (empty)
            size = max(1, self.__image_cache[set_id].size)
            img = self.gui.tk.BitmapImage(int(size), int(size))

            # render svg
            for layer_desc in imgdesc:
                layer_func, layer_data = layer_desc
                layer_func(
                    self.gui.tk, os.path.join(self.skin.directory,
                                              self.skin.ID),
                    img, layer_data,
                    angle = self.__image_cache[set_id].rotation)

            # store in cache
            self.__image_cache[set_id].set_img(imgdesc, img)

        return self.__image_cache[set_id].get_img(imgdesc)
