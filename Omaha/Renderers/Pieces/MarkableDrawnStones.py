# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import Overlord

from Omaha import Core
from .SimpleDrawnStones import SimpleDrawnStones

# FIXME: find better
from Omaha.Games.Chess import Chess
from Omaha.Games.Checkers import Checkers

class MarkableDrawnStones(SimpleDrawnStones):
    "Modification of SimpleDrawnStones to add a circle marking to a Checkers' King"

    def draw_piece(self, drawing_context, gamestate, center, set_id, piece):
        super(MarkableDrawnStones, self).draw_piece(
            drawing_context, gamestate, center, set_id, piece)
        if gamestate.piece_state(piece).face is Checkers.PIECETYPE_KING:
            center_x, center_y = center
            offset = self.radius[set_id] // 4
            owner = gamestate.piece_owner(piece)
            game_renderer = self.gui.game_renderer
            assert isinstance(game_renderer, Core.ColoredPlayersGameRenderer)
            self.gui.tk.draw_circle(drawing_context, center_x, center_y,
                                    self.radius[set_id] - offset,
                                    color=self.gui.tk.color(self.gui.canvas,
                                                            game_renderer.colornames[owner.next]))
