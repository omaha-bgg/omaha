# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.StackedVector import StackedVectorPieceRenderer, \
     svgLayer, textLayer
from Omaha import Core
import Overlord

class TextOnSVGShogiPieceRenderer(StackedVectorPieceRenderer,
                                  Core.NotationBasedPieceRenderer):

    @staticmethod
    def skin_min_colors(context):
        # pieces are not different between players (only oriented)
        return 1

    def _piece_key(self, gamestate, piece):
        return gamestate.piece_state(piece)

    # skin interface definition for Shogi
    SVG_SHOGI_TILE = (0, 0, 100, 100, "tile")
    def skin_schema(self, piece_key):
        color = self.promoted_color if piece_key.promoted else self.color

        text = self.notation.piece_name(piece_key)
        return ( (svgLayer, self.SVG_SHOGI_TILE),
                 (textLayer, (15, 20, 70, 70, text, color)), )

    @Overlord.parameter
    def color(cls, context):
        return Overlord.params.String(label = 'Glyph color for unpromoted pieces',
                                      default = "black")
    @Overlord.parameter
    def promoted_color(cls, context):
        return Overlord.params.String(label = 'Glyph color for promoted pieces',
                                      default = "darkred")
