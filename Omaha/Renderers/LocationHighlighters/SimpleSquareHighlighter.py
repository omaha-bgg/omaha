# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core

class SimpleSquareHighlighter(Core.LocationHighlighter):
    def __init__(self, game_renderer, colorname="black"):
        # FIXME: assert renderer has tile_square
        self.__game_renderer = game_renderer
        self.__color = game_renderer.gui.tk.color(self.__game_renderer.gui.canvas, colorname)

    def highlight_location(self, drawing_context, location):
        # avoid drawing above nearby tiles, by shifting corners by 1/2 line-width
        halfwidth = 1
        sq = self.__game_renderer.holder_renderers[location.holder]. \
            tile_square(location)
        self.__game_renderer.gui.tk.draw_rectangle(
            drawing_context, sq.x + halfwidth, sq.y + halfwidth,
            sq.width - 2 * halfwidth, sq.height - 2 * halfwidth,
            color=self.__color, width=2*halfwidth)
