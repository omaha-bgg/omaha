# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.HexBoard import HexBoardRenderer, HorizHexBoard, VertHexBoard
import Overlord

# FIXME suboptimal, most edges drawn twice

class OutlinedHexes(HexBoardRenderer):
    @Overlord.parameter
    def outline_color(cls, context):
        return Overlord.params.String(label = 'Outline color',
                                      default = "black")
    @outline_color.xformer      # type: ignore[no-redef]
    def outline_color(self, value):
        return self.gui.tk.color(self.gui.canvas, value)

class HorizOutlinedHexes(OutlinedHexes, HorizHexBoard):
    def draw(self, drawing_context, rectangle, gamestate):
        """Draw board within specified rectangle of self.gui."""

        super(HorizOutlinedHexes, self).draw(drawing_context, rectangle, gamestate)

        for loc in gamestate.all_locations(self.holder):
            self.gui.tk.draw_polygon(
                drawing_context, (
                    self.hextile_topleft_corner(loc),
                    self.hextile_top_corner(loc),
                    self.hextile_topright_corner(loc),
                    self.hextile_botright_corner(loc),
                    self.hextile_bot_corner(loc),
                    self.hextile_botleft_corner(loc),
                ), color=self.outline_color)

class VertOutlinedHexes(OutlinedHexes, VertHexBoard):
    def draw(self, drawing_context, rectangle, gamestate):
        """Draw board within specified rectangle of self.gui."""

        super(VertOutlinedHexes, self).draw(drawing_context, rectangle, gamestate)

        for loc in gamestate.all_locations(self.holder):
            self.gui.tk.draw_polygon(
                drawing_context, (
                    self.hextile_left_corner(loc),
                    self.hextile_topleft_corner(loc),
                    self.hextile_topright_corner(loc),
                    self.hextile_right_corner(loc),
                    self.hextile_botright_corner(loc),
                    self.hextile_botleft_corner(loc),
                ), color=self.outline_color)
