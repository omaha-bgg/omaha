# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.RectBoard import RectBoard
import Overlord

class CheckeredRectBoard(RectBoard):
    @Overlord.parameter
    def black_color(cls, context):
        return Overlord.params.String(label = 'Dark-square color',
                                      default = "#999999")
    @Overlord.parameter
    def white_color(cls, context):
        return Overlord.params.String(label = 'Light-square color',
                                      default = "white")

    @black_color.xformer        # type: ignore[no-redef,arg-type]
    def black_color(self, value):
        return self.gui.tk.color(self.gui.canvas, value)
    @white_color.xformer        # type: ignore[no-redef,arg-type]
    def white_color(self, value):
        return self.gui.tk.color(self.gui.canvas, value)

    # FIXME: selecting which is white or black is wrong for odd-sized boards
    def draw(self, drawing_context, rectangle, gamestate):
        """Draw board within specified rectangle of self.gui."""

        super(CheckeredRectBoard, self).draw(drawing_context, rectangle, gamestate)

        fillpatterns = (self.gui.tk.SolidFillPattern(self.black_color),
                        self.gui.tk.SolidFillPattern(self.white_color))

        for col in range(0, self.holder.matrix_width):
            for row in range(0, self.holder.matrix_height):
                fillpattern = fillpatterns[(col + row) % 2]

                sq = self.tile_square(self.holder.Location(self.holder,
                                                           col, row))
                self.gui.tk.fill_rectangle(drawing_context,
                                           sq.x, sq.y, sq.width, sq.height,
                                           fillpattern=fillpattern)
