# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import Overlord
from Overlord import plugin_pattern

class Euclidian2DRenderer(Core.HolderRenderer):
    """
    Base class for 2-dimentional boardrenderers.
    """
    def __init__(self, **kwargs):
        super(Euclidian2DRenderer, self).__init__(**kwargs)
        self.rectangle = None
        self.onscreen_ratio = None
        self.delta_x, self.delta_y = None, None

    def draw(self, drawing_context, rectangle, gamestate):
        """Draw the holder.

        This implementation contains common actions to be taken on
        holder redraw.  When we get redrawn, we must record geometry
        information about the curently-drawn holder, to help placing
        pieces and translating pixel coordinates into holder
        coordinates.  Subclasses must therefore compute values here
        for delta_x and delta_y.
        """
        self.rectangle = rectangle
        super(Euclidian2DRenderer, self).draw(drawing_context, rectangle, gamestate)
        self._compute_deltas(gamestate)

    @property
    def tile_inscribed_radius(self):
        """Radius of a circle inscribed in a tile of the holder."""
        raise NotImplementedError()

    def _compute_deltas(self, gamestate):
        raise NotImplementedError()


class Euclidian2DRendererWithLocation(Euclidian2DRenderer):
    def __init__(self, **kwargs):
        super(Euclidian2DRendererWithLocation, self).__init__(**kwargs)
        self.__location_notation = None

    @Overlord.parameter
    def location_notation_class(cls, context):
        "A MoveNotation used to select LocationNotation"
        game_plugin = context.game_plugin
        # FIXME should a BoardNotation if any
        if game_plugin.has_datafield("X-Omaha-DefaultNotation"):
            default_notation = game_plugin.datafield("X-Omaha-DefaultNotation")
        else:
            default_notation = None
        return Overlord.params.Plugin(
            label = 'Row/column notation for %s board' % game_plugin.name,
            plugintype = 'X-Omaha-MoveNotation',
            context = context,
            pattern = plugin_pattern.ParentOf(
                'X-Omaha-Game-Categories', game_plugin.Class),
            default_by_name = default_notation
            )
    @property
    def location_notation(self):
        notationcls = self.location_notation_class.Class
        # (re)instanciate notation if required
        if not isinstance(self.__location_notation, notationcls):
            self.__location_notation = notationcls(self.gui.game)
        return self.__location_notation

    def location_tooltip_text(self, gamestate, loc):
        return self.location_notation.location_notation.location_name(loc)
