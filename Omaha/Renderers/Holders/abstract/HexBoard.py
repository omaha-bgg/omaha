# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .Euclidian2D import Euclidian2DRendererWithLocation
from Omaha.Holders.HexBoard import HexBoard
import math

class HexBoardRenderer(Euclidian2DRendererWithLocation):
    def __init__(self, **kwargs):
        super(HexBoardRenderer, self).__init__(**kwargs)
        assert isinstance(self.holder, HexBoard)
        self.rectangle = None

    def tile_center(self, location):
        return (self.hextile_center_x(location),
                self.hextile_center_y(location))

    def hextile_center_x(self, location):
        raise NotImplementedError()
    def hextile_center_y(self, location):
        raise NotImplementedError()


class HorizHexBoard(HexBoardRenderer):
    """
    HexBoard renderer with one board diagonal rendered horizontally.

    It is make of vertical hexes.
    """
    def __init__(self, **kwargs):
        super(HorizHexBoard, self).__init__(**kwargs)
        self.baserow = None
        self.__onscreen_width_in_dx = (
            # FIXME
            self.holder.width0 + (self.holder.width1 - 1 + self.holder.width2 - 1) / 2)
        # we have more than an integer number of dy: must add one height of wedges
        self.__onscreen_height_in_dy = (self.holder.matrix_height
                                        + 1/3) # (tile_radius/2) / dx
        self.onscreen_ratio = self.__onscreen_height_in_dy  / ((2 / math.sqrt(3)) * self.__onscreen_width_in_dx)
    def _compute_deltas(self, gamestate):
        self.baserow = gamestate.holder_state(self.holder).leftmost_row
        self.delta_x = self.rectangle.width / self.__onscreen_width_in_dx
        #self.delta_y = self.delta_x * math.sqrt(3) / 2
        self.delta_y = self.rectangle.height / self.__onscreen_height_in_dy

    @property
    def tile_radius(self):
        return self.delta_y * 2 / 3
    @property
    def tile_inscribed_radius(self):
        return self.delta_x / 2

    def hextile_center_x(self, loc):
        # adjustment for the hexagon slant
        adj = (loc.row - self.baserow) * 0.5
        return int(self.rectangle.x + (loc.col - adj + 0.5) * self.delta_x)
    def hextile_center_y(self, loc):
        return int(self.rectangle.y_max - (
            loc.row
            + 0.5 # offset center to show left half
            + 1/6 # offset to show corner == 1/2* (tile_radius/2) / dy
        ) * self.delta_y)

    # corners "owned" by the location
    def hextile_top_corner(self, loc):
        return (self.hextile_center_x(loc),
                self.hextile_center_y(loc) - self.tile_radius)
    def hextile_bot_corner(self, loc):
        return (self.hextile_center_x(loc),
                self.hextile_center_y(loc) + self.tile_radius)
    # corners "owned" by neighbours
    def hextile_topleft_corner(self, loc):
        return self.hextile_bot_corner(loc + loc.Delta( 0, +1))
    def hextile_topright_corner(self, loc):
        return self.hextile_bot_corner(loc + loc.Delta(+1, +1))
    def hextile_botleft_corner(self, loc):
        return self.hextile_top_corner(loc + loc.Delta(-1, -1))
    def hextile_botright_corner(self, loc):
        return self.hextile_top_corner(loc + loc.Delta( 0, -1))

    def draw_hextile_vert(self, drawing_context, loc, fillpattern):
        self.gui.tk.fill_polygon(
            drawing_context, (
                self.hextile_topleft_corner(loc),
                self.hextile_top_corner(loc),
                self.hextile_topright_corner(loc),
                self.hextile_botright_corner(loc),
                self.hextile_bot_corner(loc),
                self.hextile_botleft_corner(loc),
            ), fillpattern=fillpattern)

    def draw_vhex_side(self, drawing_context, location, side, **attrs):
        board = self.gui.game.board
        pointfuncs = {
            board.VHexSide.TOPRIGHT: (self.hextile_top_corner, self.hextile_topright_corner),
            board.VHexSide.RIGHT: (self.hextile_topright_corner, self.hextile_botright_corner),
            board.VHexSide.BOTRIGHT: (self.hextile_botright_corner, self.hextile_bot_corner),
            board.VHexSide.BOTLEFT: (self.hextile_bot_corner, self.hextile_botleft_corner),
            board.VHexSide.LEFT: (self.hextile_botleft_corner, self.hextile_topleft_corner),
            board.VHexSide.TOPLEFT: (self.hextile_topleft_corner, self.hextile_top_corner),
        }[side]
        x0, y0 = pointfuncs[0](location)
        x1, y1 = pointfuncs[1](location)
        self.gui.tk.draw_line(drawing_context, x0, y0, x1, y1, **attrs)

    def point_to_location(self, x, y):
        """Get logical square from physical coords within gui."""
        # Do as for a square board, but ajust horizontaly afterwards.
        # FIXME: adjustment near hex boundaries is needed for exactness.
        # Do float divisions in this case for simplicity.
        row = int((self.rectangle.y_max - y) / self.delta_y)
        # adjustment for the hexagon slant
        adj = (row - self.baserow) * 0.5
        col = int(adj + (x - self.rectangle.x) / self.delta_x)

        if  (col >= 0 and col < self.holder.matrix_width and
             row >= 0 and row < self.holder.matrix_height):
            loc = self.holder.Location(self.holder, col, row)
            return loc
        else:
            return None

class VertHexBoard(HexBoardRenderer):
    """
    HexBoard renderer with one board diagonal rendered vertically.

    It is make of horizontal hexes.
    """
    def __init__(self, **kwargs):
        super(VertHexBoard, self).__init__(**kwargs)
        self.basecol = None
        self.__onscreen_height_in_dy = (
            self.holder.width1 + (self.holder.width0 - 1 + self.holder.width2 - 1) / 2)
        # we have more than an integer number of dx: must add one side of wedges
        self.__onscreen_width_in_dx = (self.holder.matrix_width
                                       + 1/3) # (tile_radius/2) / dx
        self.onscreen_ratio = (2 / math.sqrt(3)) * self.__onscreen_height_in_dy  / self.__onscreen_width_in_dx
    def _compute_deltas(self, gamestate):
        self.basecol = gamestate.holder_state(self.holder).topmost_col
        self.delta_y = self.rectangle.height / self.__onscreen_height_in_dy
        #self.delta_x = self.delta_y * math.sqrt(3) / 2
        self.delta_x = self.rectangle.width / self.__onscreen_width_in_dx

    @property
    def tile_radius(self):
        return self.delta_x * 2 / 3
    @property
    def tile_inscribed_radius(self):
        return self.delta_y / 2

    def hextile_center_x(self, loc):
        return int(self.rectangle.x + (
            loc.col
            + 0.5 # offset center to show left half
            + 1/6 # offset to show corner == 1/2* (tile_radius/2) / dx
        ) * self.delta_x)
    def hextile_center_y(self, loc):
        # adjustment for the hexagon slant
        adj = (loc.col - self.basecol) * 0.5
        return int(self.rectangle.y_max - (loc.row - adj + 0.5) * self.delta_y)

    # corners "owned" by the location
    def hextile_left_corner(self, loc):
        return (self.hextile_center_x(loc) - self.tile_radius, self.hextile_center_y(loc))
    def hextile_right_corner(self, loc):
        return (self.hextile_center_x(loc) + self.tile_radius, self.hextile_center_y(loc))
    # corners "owned" by neighbours
    def hextile_topleft_corner(self, loc):
        return self.hextile_right_corner(loc + loc.Delta(-1,  0))
    def hextile_botleft_corner(self, loc):
        return self.hextile_right_corner(loc + loc.Delta(-1, -1))
    def hextile_topright_corner(self, loc):
        return self.hextile_left_corner(loc + loc.Delta(+1, +1))
    def hextile_botright_corner(self, loc):
        return self.hextile_left_corner(loc + loc.Delta(+1, 0))

    def draw_hextile_horiz(self, drawing_context, loc, fillpattern):
        self.gui.tk.fill_polygon(
            drawing_context, (
                self.hextile_left_corner(loc),
                self.hextile_topleft_corner(loc),
                self.hextile_topright_corner(loc),
                self.hextile_right_corner(loc),
                self.hextile_botright_corner(loc),
                self.hextile_botleft_corner(loc),
            ), fillpattern=fillpattern)

    def draw_hhex_side(self, drawing_context, location, side, **attrs):
        board = self.gui.game.board
        pointfuncs = {
            board.HHexSide.TOP: (self.hextile_topleft_corner, self.hextile_topright_corner),
            board.HHexSide.TOPRIGHT: (self.hextile_topright_corner, self.hextile_right_corner),
            board.HHexSide.BOTRIGHT: (self.hextile_right_corner, self.hextile_botright_corner),
            board.HHexSide.BOT: (self.hextile_botright_corner, self.hextile_botleft_corner),
            board.HHexSide.BOTLEFT: (self.hextile_botleft_corner, self.hextile_left_corner),
            board.HHexSide.TOPLEFT: (self.hextile_left_corner, self.hextile_topleft_corner),
        }[side]
        x0, y0 = pointfuncs[0](location)
        x1, y1 = pointfuncs[1](location)
        self.gui.tk.draw_line(drawing_context, x0, y0, x1, y1, **attrs)

    def point_to_location(self, x, y):
        """Get logical square from physical coords within gui."""
        # Do as for a square board, but ajust vertically afterwards.
        # FIXME: adjustment near hex boundaries is needed for exactness.
        # Do float divisions in this case for simplicity.
        col = int((x - self.rectangle.x) / self.delta_x)
        # adjustment for the hexagon slant
        adj = (col - self.basecol) * 0.5
        row = int(adj + (self.rectangle.y_max - y) / self.delta_y)

        if  (col >= 0 and col < self.holder.matrix_width and
             row >= 0 and row < self.holder.matrix_height):
            loc = self.holder.Location(self.holder, col, row)
            return loc
        else:
            return None
