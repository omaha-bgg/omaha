# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import Overlord
from .Euclidian2D import Euclidian2DRendererWithLocation

class RectBoard(Euclidian2DRendererWithLocation):
    """
    Base class for 2-dimentional boardrenderers for square boards.
    """
    def __init__(self, **kwargs):
        super(RectBoard, self).__init__(**kwargs)
        self.onscreen_ratio = self.holder.matrix_height / self.holder.matrix_width

    @Overlord.parameter
    def border_width_percent(cls, context):
        return Overlord.params.Int(label = 'Border width %',
                                   minval = 0, maxval = 20,
                                   default = 5)
    @Overlord.parameter
    def border_color(cls, context):
        return Overlord.params.String(label = 'Border color',
                                      default = "#A66D39")
    @border_color.xformer       # type: ignore[no-redef]
    def border_color(self, value):
        return self.gui.tk.color(self.gui.canvas, value)

    def point_to_location(self, x, y):
        """Get logical square from physical coords within gui."""
        # Approximate the tile coords by integer division...
        # (the real value would require a float division)
        col = int((x - self.board_x0) / self.delta_x)
        row = int((y - self.board_yn) / self.delta_y)

        # correct for screen clockwise orientation
        row = self.holder.matrix_height - 1 - row

        if  (col >= 0 and col < self.holder.matrix_width and
             row >= 0 and row < self.holder.matrix_height):
            return self.holder.Location(self.holder, col, row)
        else:
            return None

    def draw(self, drawing_context, rectangle, gamestate):
        """Draw the board.

        This implementation contains common actions to be taken on
        board redraw.  When we get redrawn, we record geometry
        information about the curently-drawn board, to help placing
        pieces and translating pixel coordinates into board
        coordinates.
        """

        # border
        if self.border_width_percent:
            self.gui.tk.fill_rectangle(drawing_context,
                                       rectangle.x, rectangle.y,
                                       rectangle.width, rectangle.height,
                                       fillpattern=self.gui.tk.SolidFillPattern(self.border_color))

        super(RectBoard, self).draw(drawing_context, rectangle, gamestate)

        # row/col names
        if self.border_width_percent:
            y0 = rectangle.y + rectangle.height * (100 - self.border_width_percent/2) / 100
            yn = rectangle.y + rectangle.height * self.border_width_percent/2 / 100
            for col in range(0, self.holder.matrix_width):
                # first line
                refloc = self.holder.Location(self.holder, col, 0)
                if gamestate.valid_location(refloc):
                    x = self.tile_center(refloc)[0]
                    colname = self.location_notation.location_notation.colname(refloc)
                    self.gui.tk.draw_centered_text( drawing_context, x, y0, colname, bold=True)
                # last line
                refloc = self.holder.Location(self.holder, col, self.holder.matrix_height - 1)
                if gamestate.valid_location(refloc):
                    x = self.tile_center(refloc)[0]
                    colname = self.location_notation.location_notation.colname(refloc)
                    self.gui.tk.draw_centered_text( drawing_context, x, yn, colname, bold=True)
            x0 = rectangle.x + rectangle.width * self.border_width_percent/2 / 100
            xn = rectangle.x + rectangle.width * (100 - self.border_width_percent/2) / 100
            for row in range(0, self.holder.matrix_height):
                # first column
                refloc = self.holder.Location(self.holder, 0, row)
                if gamestate.valid_location(refloc):
                    y = self.tile_center(refloc)[1]
                    rowname = self.location_notation.location_notation.rowname(refloc)
                    self.gui.tk.draw_centered_text(drawing_context, x0, y, rowname, bold=True)
                # last column
                refloc = self.holder.Location(self.holder, self.holder.matrix_width - 1, row)
                if gamestate.valid_location(refloc):
                    y = self.tile_center(refloc)[1]
                    rowname = self.location_notation.location_notation.rowname(refloc)
                    self.gui.tk.draw_centered_text(drawing_context, xn, y, rowname, bold=True)

    def _compute_deltas(self, gamestate):
        w = self.board_xn - self.board_x0
        h = self.board_y0 - self.board_yn
        assert w > 0
        assert h > 0
        self.delta_x = w / self.holder.matrix_width
        self.delta_y = h / self.holder.matrix_height

    # effective board limits, excluding borders

    @property
    def board_x0(self):
        return (self.rectangle.x +
                self.rectangle.width *
                (self.border_width_percent) // 100)
    @property
    def board_xn(self):
        return (self.rectangle.x +
                self.rectangle.width *
                (100 - self.border_width_percent) // 100)
    @property
    def board_y0(self):
        return (self.rectangle.y +
                self.rectangle.height *
                (100 - self.border_width_percent) // 100)
    @property
    def board_yn(self):
        return (self.rectangle.y +
                self.rectangle.height *
                (self.border_width_percent) // 100)

    # tile-positionning methods

    def tile_corner_x(self, col):
        return int(self.board_x0 + col * self.delta_x)
    def tile_corner_y(self, row):
        return int(self.board_y0 - row * self.delta_y)

    def tile_center_x(self, col):
        return int(self.board_x0 + (col + 0.5) * self.delta_x)
    def tile_center_y(self, row):
        return int(self.board_y0 - (row + 0.5) * self.delta_y)

    def tile_center(self, location):
        return (self.tile_center_x(location.col),
                self.tile_center_y(location.row))

    def tile_square(self, location):
        """Square where given location gets drawn."""
        assert location.holder is self.holder
        return Core.Rectangle(
            self.tile_corner_x(location.col),
            self.tile_corner_y(location.row + 1),
            (self.tile_corner_x(location.col + 1) -
             self.tile_corner_x(location.col)),
            (self.tile_corner_y(location.row) -
             self.tile_corner_y(location.row + 1)))

    @property
    def tile_inscribed_radius(self):
        return min(self.delta_x, self.delta_y) / 2
