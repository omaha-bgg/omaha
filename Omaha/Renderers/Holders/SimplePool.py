# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Omaha.Holders.Pool import PiecePool
from .abstract.Euclidian2D import Euclidian2DRenderer
import Overlord

class SimplePool(Euclidian2DRenderer):
    def __init__(self, **kwargs):
        super(SimplePool, self).__init__(**kwargs)
        assert isinstance(self.holder, PiecePool)
        self.rectangle = None
        self.areas_locations = set() # of (Rectangle, Location)

    @Overlord.parameter
    def background_color(cls, context):
        return Overlord.params.String(label = 'Background color',
                                      default = "#008800")
    @background_color.xformer   # type: ignore[no-redef]
    def background_color(self, value):
        return self.gui.tk.color(self.gui.canvas, value)

    @Overlord.parameter
    def border_color(cls, context):
        return Overlord.params.String(label = 'Border color',
                                      default = "#007000")
    @border_color.xformer       # type: ignore[no-redef]
    def border_color(self, value):
        return self.gui.tk.color(self.gui.canvas, value)

    @Overlord.parameter
    def default_step(cls, context):
        return Overlord.params.Float(label = 'Default step (percent)',
                                     default = 100.)

    def draw(self, drawing_context, rectangle, gamestate):
        self.rectangle = rectangle
        super(SimplePool, self).draw(drawing_context, rectangle, gamestate)
        self.gui.tk.fill_rectangle(
            drawing_context,
            rectangle.x, rectangle.y, rectangle.width, rectangle.height,
            fillpattern=self.gui.tk.SolidFillPattern(self.background_color))
        self.gui.tk.draw_rectangle(
            drawing_context,
            rectangle.x, rectangle.y, rectangle.width, rectangle.height,
            color=self.border_color)

        count_fillpattern = self.gui.tk.SolidFillPattern(self.gui.tk.color(self.gui.canvas, "red"))
        # FIXME: update only if changed
        self.areas_locations = list()
        for idx, piecegroup in enumerate(gamestate.holder_state(self.holder).piece_groups):
            location = self.holder.Location(self.holder, piecegroup)
            square = self.__tile_square(idx)
            self.areas_locations.append( (square, location) )
            # FIXME should be on an overlay above pieces
            numpieces = gamestate.holder_state(self.holder).num_pieces_of_group(piecegroup)
            if numpieces > 1:
                # FIXME positionning is not good for 2nd player, whole
                # pool should be rotated
                self.gui.tk.fill_rectangle(drawing_context, square.x, square.y,
                                           square.width / 3, square.height / 3,
                                           fillpattern=count_fillpattern)
                self.gui.tk.draw_sized_text(drawing_context, square.x, square.y,
                                            square.width / 3, square.height / 3,
                                            str(numpieces),
                                            color=self.gui.tk.color(self.gui.canvas,
                                                                    "black"),
                                            bold=True)

    def draw_pieces(self, drawing_context, gamestate):
        "Draw piece from left to right"
        holder_state = gamestate.holder_state(self.holder)
        for rect, loc in self.areas_locations:
            one_piece = holder_state.piece_at(loc)
            self.gui.game_renderer._draw_piece(drawing_context, gamestate, one_piece)

    def _compute_deltas(self, gamestate):
        self.delta_x = float(self.rectangle.height) * self.default_step / 100

    def __tile_topleft(self, index):
        return (self.rectangle.x + self.delta_x * index,
                self.rectangle.y)

    def tile_center(self, location):
        return self.tile_square(location).center

    def __tile_square(self, index):
        x, y = self.__tile_topleft(index)
        return (
            Core.Rectangle(
                x, y, self.rectangle.height, self.rectangle.height))

    def tile_square(self, location):
        for rect, loc in self.areas_locations:
            if loc == location:
                return rect
        raise IndexError("location %s not drawn: %s" % (location, self.areas_locations))

    def point_to_location(self, area, x, y):
        for rect, loc in reversed(self.areas_locations):
            if rect.contains(x, y):
                return loc
        return None

    @property
    def tile_inscribed_radius(self):
        return self.rectangle.height // 2

    def location_tooltip_text(self, gamestate, loc):
        text = super(SimplePool, self).location_tooltip_text(gamestate, loc)
        # today this is empty, not sure how to combine if that ever changes
        assert text == ""
        numpieces = gamestate.holder_state(self.holder).num_pieces_of_group(loc.piecegroup)
        if numpieces > 1:
            return "%d pieces" % numpieces
        return text
