# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.SingleBoard import SingleBoardGameRenderer
from Omaha import Core
import Overlord
from Overlord import plugin_pattern

# TODO: support incremental updates

# FIXME: font size should depend on canvas size

class AsciiChessRenderer(SingleBoardGameRenderer):

    def __init__(self, **kwargs):
        super(AsciiChessRenderer, self).__init__(**kwargs)
        self.__notation = None

    def _draw_piece(self, drawing_context, gamestate, piece):
        holder = gamestate.piece_holder(piece)
        xcenter, ycenter = self.holder_renderers[holder]. \
            tile_center(piece.location(gamestate))
        height = self.holder_renderers[holder].delta_y * 50 /100

        style = (dict(),                           # white
                 dict(bold=True),                  # black
                 dict(bold=True,
                      color=self.gui.tk.color(self.gui.canvas, "orange")),  # (Forchess) yellow
                 dict(bold=True,
                      color=self.gui.tk.color(self.gui.canvas, "darkred")), # (Forchess) red
        )[self.gui.game.players.index(gamestate.piece_owner(piece))]

        self.gui.tk.draw_centered_text(
            drawing_context,
            xcenter, ycenter,
            self.notation.piece_name(gamestate.piece_state(piece)),
            height=height, **style)

    # FIXME: the following is duplicated from
    # NotationBasedPieceRenderer because we're not a using a
    # PieceGameRenderer

    @Overlord.parameter
    def notation_class(cls, context):
        game_class = context.game_plugin.Class
        return Overlord.params.Plugin(
            label = 'Character set',
            plugintype = 'X-Omaha-PieceNotation',
            context = context,
            pattern = plugin_pattern.ParentOf(
                'X-Omaha-Game-Categories', game_class),
            #default_by_name = # FIXME: by luck, selects kanji
            )

    @property
    def notation(self):
        notationcls = self.notation_class.Class
        # (re)instanciate notation if required
        if not isinstance(self.__notation, notationcls):
            self.__notation = notationcls(self.gui.game)
        return self.__notation
