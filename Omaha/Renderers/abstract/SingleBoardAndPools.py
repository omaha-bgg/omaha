# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .SingleBoard import SingleBoardGameRenderer
from Omaha import Core

class SingleBoardAndPools(SingleBoardGameRenderer):
    # pylint: disable=abstract-method
    def __init__(self, **kwargs):
        super(SingleBoardAndPools, self).__init__(**kwargs)
        self.pool_height = None

    @property
    def margin(self):
        return 10

    @property
    def available_area_for_board(self):
        x, y, width, height = super().available_area_for_board
        return (x, y + self.pool_height + self.margin,
                width, height - 2 * (self.pool_height + self.margin))

    # FIXME: lacks support board rotations
    # FIXME: rather handle positionning with nested rectangles ?

    def recompute_areas(self):
        super(SingleBoardAndPools, self).recompute_areas()

        self.areas[self.game.pool_of(self.game.players[1])] = \
            Core.Rectangle(self.margin, self.margin,
                           self.current_board_width, self.pool_height)
        self.areas[self.game.pool_of(self.game.players[0])] = \
            Core.Rectangle(self.margin,
                           3*self.margin +
                           self.current_board_height + self.pool_height,
                           self.current_board_width, self.pool_height)

    def draw_holders(self, drawing_context, gamestate):
        super(SingleBoardAndPools, self).draw_holders(drawing_context, gamestate)
        for player in self.game.players:
            pool = self.game.pool_of(player)
            self.holder_renderers[pool].draw(drawing_context, self.areas[pool], gamestate)

    def point_to_location(self, x, y):
        # first check for the board
        loc = super(SingleBoardAndPools, self).point_to_location(x, y)
        if loc is not None:
            return loc

        # then check for the pools
        for player in self.game.players:
            pool = self.game.pool_of(player)
            area = self.areas[pool]
            if area.contains(x, y):
                return self.holder_renderers[pool].point_to_location(area, x, y)
