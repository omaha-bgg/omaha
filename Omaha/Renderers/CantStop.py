# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from typing import Any, Optional

from Omaha import Core
from Omaha.Core import misc
from Omaha.Games import CantStop
from .abstract.SingleBoard import SingleBoardGameRenderer

class CantStopRenderer(SingleBoardGameRenderer,
                       Core.SinglePieceSetGameRenderer, Core.ColoredPlayersGameRenderer):

    DICE_SIZE = 50
    DICE_MARGIN_PERCENT = 10
    DICE_CORNER_RADIUS_PERCENT = 4
    DICE_FACE_CORNER_RADIUS_PERCENT = 20

    handles_player_display = True

    def __init__(self, **kwargs: Any):
        super().__init__(**kwargs)
        self._move_areas: list[tuple[Core.Rectangle,dict[str,Any]]] = []

    def _dice_area_width(self) -> float:
        game = self.gui.game
        assert isinstance(game, CantStop.Game)
        MARGIN_WIDTH = self.DICE_SIZE * self.DICE_MARGIN_PERCENT / 100
        GROUP_WIDTH = (game.DICE_GROUP_SIZE * self.DICE_SIZE +
                       (game.DICE_GROUP_SIZE + 1) * MARGIN_WIDTH)
        return (game.NUM_DICE_GROUPS * GROUP_WIDTH +
                (game.NUM_DICE_GROUPS + 3) * MARGIN_WIDTH)

    @property
    def available_area_for_board(self) -> tuple[int,int,int,int]:
        x, y, width, height = super().available_area_for_board
        return (x, y, width - self._dice_area_width(), height)

    def recompute_areas(self) -> None:
        super().recompute_areas()
        width = self._dice_area_width()
        self.areas["dice"] = Core.Rectangle(self.gui.canvas_size[0] - width, 0,
                                            width, self.gui.canvas_size[1])

    def draw_holders(self, drawing_context: Core.UI.DrawingContext,
                     gamestate: Core.GameState) -> None:
        super().draw_holders(drawing_context, gamestate)

        # dice area is not really a holder, but conceptually close
        rectangle = self.areas["dice"]
        self.gui.tk.fill_rectangle(drawing_context,
                                   rectangle.x, rectangle.y,
                                   rectangle.width, rectangle.height,
                                   fillpattern=self.gui.tk.SolidFillPattern(self.background_color))

    def _draw_dice_combo(self, drawing_context: Core.UI.DrawingContext,
                         gamestate: CantStop.Game.State,
                         dice_combo: tuple[tuple[int,...],...], combo_y: float) -> None:
        area = self.areas["dice"]
        game = self.gui.game
        assert isinstance(game, CantStop.Game)
        MARGIN_WIDTH = self.DICE_SIZE * self.DICE_MARGIN_PERCENT / 100
        GROUP_WIDTH = (game.DICE_GROUP_SIZE * self.DICE_SIZE +
                       (game.DICE_GROUP_SIZE + 1) * MARGIN_WIDTH)

        # collect infos about dice groups in the set
        valid_choices = gamestate.valid_choices_in_combo(dice_combo)
        combo_is_valid = dice_combo in valid_choices

        combo_bgcolor = "#008800" if combo_is_valid else "lightgray"
        combo_rect = Core.Rectangle(
            area.x + MARGIN_WIDTH, combo_y + MARGIN_WIDTH,
            area.width - 2 * MARGIN_WIDTH, self.DICE_SIZE + 4 * MARGIN_WIDTH)

        # draw dice combinations and setup active _move_areas
        self.gui.tk.fill_rounded_rectangle(
            drawing_context,
            combo_rect.x, combo_rect.y, combo_rect.width, combo_rect.height,
            self.DICE_SIZE * self.DICE_FACE_CORNER_RADIUS_PERCENT / 100,
            fillpattern=self.gui.tk.SolidFillPattern(
                self.gui.tk.color(self.gui.canvas, combo_bgcolor)))
        if combo_is_valid:
            self._move_areas.append((combo_rect, dict(columns=dice_combo)))

        for i, dice_group in enumerate(dice_combo):
            dice_color, dice_bgcolor, group_color = (
                ("darkred", "#F2EB82", "#00AA00")
                if combo_is_valid or ((dice_group,) in valid_choices) else
                ("black", "white", "gray"))

            group_x = area.x + 2 * MARGIN_WIDTH + (GROUP_WIDTH + MARGIN_WIDTH) * i
            group_rect = Core.Rectangle(group_x, combo_y + 2 * MARGIN_WIDTH,
                                        GROUP_WIDTH, self.DICE_SIZE + 2 * MARGIN_WIDTH)

            self.gui.tk.fill_rounded_rectangle(
                drawing_context,
                group_rect.x, group_rect.y, group_rect.width, group_rect.height,
                self.DICE_SIZE * self.DICE_FACE_CORNER_RADIUS_PERCENT / 100,
                fillpattern=self.gui.tk.SolidFillPattern(
                    self.gui.tk.color(self.gui.canvas, group_color)))
            if (dice_group,) in valid_choices:
                self._move_areas.append((group_rect, dict(columns=(dice_group,))))

            for j, die in enumerate(dice_group):
                die_x = group_rect.x + MARGIN_WIDTH + (self.DICE_SIZE + MARGIN_WIDTH) * j
                die_y = group_rect.y + MARGIN_WIDTH
                self.gui.tk.fill_rounded_rectangle(
                    drawing_context,
                    die_x, die_y, self.DICE_SIZE, self.DICE_SIZE,
                    self.DICE_SIZE * self.DICE_CORNER_RADIUS_PERCENT / 100,
                    fillpattern=self.gui.tk.SolidFillPattern(
                        self.gui.tk.color(self.gui.canvas, dice_bgcolor)))
                self.gui.tk.draw_rounded_rectangle(
                    drawing_context,
                    die_x, die_y, self.DICE_SIZE, self.DICE_SIZE,
                    self.DICE_SIZE * self.DICE_CORNER_RADIUS_PERCENT / 100,
                    color=self.gui.tk.color(self.gui.canvas, "black"))
                self.gui.tk.draw_rounded_rectangle(
                    drawing_context,
                    die_x, die_y, self.DICE_SIZE, self.DICE_SIZE,
                    self.DICE_SIZE * self.DICE_FACE_CORNER_RADIUS_PERCENT / 100,
                    color=self.gui.tk.color(self.gui.canvas, "black"))

                self.gui.tk.draw_sized_text(drawing_context,
                                            die_x + self.DICE_SIZE / 4,
                                            die_y + self.DICE_SIZE / 4,
                                            self.DICE_SIZE / 2, self.DICE_SIZE / 2,
                                            str(die),
                                            color=self.gui.tk.color(self.gui.canvas, dice_color))

    def draw_pieces(self, drawing_context: Core.UI.DrawingContext, # type: ignore[override]
                    gamestate: CantStop.Game.State) -> None:
        super().draw_pieces(drawing_context, gamestate)
        # dice are not really pieces, but conceptually close

        MARGIN_WIDTH = self.DICE_SIZE * self.DICE_MARGIN_PERCENT / 100

        area = self.areas["dice"]
        playername_area = Core.Rectangle(area.x, area.y, area.width, self.DICE_SIZE)
        interact_area = Core.Rectangle(area.x, area.y + self.DICE_SIZE,
                                       area.width, area.height - self.DICE_SIZE)

        game_renderer = self.gui.game_renderer
        assert isinstance(game_renderer, Core.ColoredPlayersGameRenderer)

        self.gui.tk.draw_sized_text(
            drawing_context,
            playername_area.x + MARGIN_WIDTH, playername_area.y + MARGIN_WIDTH,
            playername_area.width - 2 * MARGIN_WIDTH, playername_area.height - 4 * MARGIN_WIDTH,
            f"{gamestate.whose_turn.name}:"
            f" {self.gui.match._player_drivers[gamestate.whose_turn].name}",
            color=self.gui.tk.color(self.gui.canvas,
                                    game_renderer.colornames[gamestate.whose_turn]))

        game = self.gui.game
        assert isinstance(game, CantStop.Game)

        self._move_areas = []
        if gamestate.dice_values:
            for i, dice_combo in enumerate(misc.dice_combinations(
                    tuple(sorted(gamestate.dice_values)), game.DICE_GROUP_SIZE)):
                self._draw_dice_combo(drawing_context, gamestate, dice_combo,
                                      interact_area.y + (self.DICE_SIZE + 5 * MARGIN_WIDTH) * i)

        # buttons for roll/stop
        elif gamestate.turn_state == game.State.TurnState.START:
            button_width = (interact_area.width - 3 * MARGIN_WIDTH) / 2
            for i, (label, bgcolor, move_args) in enumerate((
                    ("Roll", "green", dict(is_roll=1)),
                    ("Stop", "darkred", dict(is_stop=1)))):
                rect = Core.Rectangle(
                    interact_area.x + MARGIN_WIDTH + i * (button_width + MARGIN_WIDTH),
                    interact_area.y + MARGIN_WIDTH,
                    button_width, button_width / 3)
                self.gui.tk.fill_rounded_rectangle(
                    drawing_context,
                    rect.x, rect.y, rect.width, rect.height,
                    self.DICE_SIZE * self.DICE_FACE_CORNER_RADIUS_PERCENT / 100,
                    fillpattern=self.gui.tk.SolidFillPattern(
                        self.gui.tk.color(self.gui.canvas, bgcolor)))
                self.gui.tk.draw_sized_text(drawing_context,
                                            rect.x, rect.y + MARGIN_WIDTH,
                                            rect.width, rect.height - 2 * MARGIN_WIDTH,
                                            label)
                self._move_areas.append((rect, move_args))

        # locked columns
        board = game.board
        board_renderer = self.holder_renderers[board]
        delta_x, delta_y = board_renderer.delta_x, board_renderer.delta_y
        for col in range(game.NCOLUMNS):
            player = gamestate.is_locked(col)
            if player:
                color = self.gui.tk.color_transparent(
                    self.gui.tk.color(self.gui.canvas, game_renderer.colornames[player]),
                    0.5)
                bottom_loc = board.Location(board, col, 0)
                top_loc = board.Location(board, col, game.column_height(col) - 1)
                _, bottom_center_y = board_renderer.tile_center(bottom_loc)
                top_center_x, top_center_y = board_renderer.tile_center(top_loc)
                self.gui.tk.fill_rounded_rectangle(
                    drawing_context,
                    top_center_x - delta_x * 45 / 100, top_center_y - delta_y * 45 / 100,
                    delta_x * 90 / 100, (bottom_center_y - top_center_y) + delta_y * 90 / 100,
                    delta_x * self.DICE_FACE_CORNER_RADIUS_PERCENT / 100,
                    fillpattern=self.gui.tk.SolidFillPattern(color))

    def point_to_move_args(self, x: int, y: int) -> Optional[dict[str,Any]]:
        for rect, move_args in self._move_areas:
            if rect.contains(x, y):
                return move_args
        return None
