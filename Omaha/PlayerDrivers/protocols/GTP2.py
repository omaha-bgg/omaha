# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
A Base PlayerDriver class speaking the Go Text Protocol v2 for Omaha.
Spec for GTP v2 is at http://www.lysator.liu.se/~gunnar/gtp/.
"""

from Omaha import Core
from Omaha.Games.Go import GoHybridNotation
import re, logging

logger = logging.getLogger(__name__)

class GTP2(Core.PlayerDriver):

    def __init__(self, **kwargs):
        super(GTP2, self).__init__(**kwargs)
        self.__next_cmd_id = 0
        self.__callbacks = {}
        self.__notation = GoHybridNotation(self.gui.game)
        self.__color = { self.gui.game.players[0]: 'black',
                         self.gui.game.players[1]: 'white' }

    def send_command(self, command, callback=None):
        self.write("%d %s" % (self.__next_cmd_id, command))
        if callback is not None:
            self.__callbacks[self.__next_cmd_id] = callback
        self.__next_cmd_id += 1

    def handle_data(self, stream, arg): # pylint: disable=unused-argument
        line = stream.readline()
        if line == "":
            logger.warn("%s EOF", self.player.name)
            self.disconnect()
        line = line.strip()
        logger.debug("%s<%s", self.__color[self.player][0], line)

        # answer ?
        match = re.match(r'([=?])(\d+)( (.*))?', line)
        if match:
            success = (match.group(1) == '=')
            cmd_id = int(match.group(2))
            # start of answer: read till the end
            if match.group(3) == None:
                answer = None
            else:
                answer = [ match.group(4) ]
            while True:
                line = stream.readline().strip()
                if line == "":
                    break
                answer.append(line)
            # call callback if any
            if cmd_id in self.__callbacks:
                self.__callbacks[cmd_id](success, answer, cmd_id)
                del self.__callbacks[cmd_id]

            return

        logger.info("unhandled IA output: %r", line)

    def handle_event(self, event):
        if isinstance(event, Core.Game.MoveEvent):
            def _check_move_cb(success, answer, cmd_id):
                if not success:
                    logger.warning('AI refused move: "%s"', '; '.join(answer))
                    self.gui.game.refuse_move(self.gui.game.moves.last, self.player)
                    # refresh display
                    self.gui.tk.invalidate_canvas(self.gui.canvas)
                    return
                if self.gui.game.is_playing_phase():
                    self.request_move()
            if event.move.player is not self.player:
                movestr = self.__notation.move_serialization(event.move)
                self.send_command("play %s %s" % (self.__color[event.move.player],
                                                  movestr),
                                  _check_move_cb)

    def request_move(self):
        def _publish_move(success, answer, cmd_id):
            if not self._check_error(success, answer, cmd_id):
                return
            if answer[0].lower() == "resign":
                self.gui.game.declare_resignation(self.player)
                return

            move = self.__notation.move_deserialization(self.gui.game.last_state,
                                                        answer[0], self.player)
            try:
                self.gui.game.make_move(self.player, move)
            except Core.GameSuspended:
                logger.debug("move received while game is suspended, undoing")
                self.send_command("undo")
            except Core.InvalidMove as ex:
                logger.critical("Game refused AI move %s: %s", answer[0], ex)
                raise
            else:
                # refresh display
                self.gui.tk.invalidate_canvas(self.gui.canvas)
            return
        self.send_command("genmove %s" % self.__color[self.player],
                          _publish_move)

    def gtp_get_info(self):
        # check compatibility
        def _check_protocol_version(success, answer, cmd_id):
            if not success or answer[0] != "2":
                raise RuntimeError("Engine does not speak GTP v2")
        self.send_command("protocol_version", _check_protocol_version)

        # name, version
        def _accumulate_name(success, answer, cmd_id):
            if not self._check_error(success, answer, cmd_id):
                return
            if self.name is None:
                self.name = answer[0]
            else:
                self.name += " " + answer[0]
        self.send_command("name", _accumulate_name)
        self.send_command("version", _accumulate_name)

    def gtp_init_game(self):
        assert self.gui.game.boardwidth == self.gui.game.boardheight
        self.send_command("boardsize %d" % self.gui.game.boardwidth,
                          self._check_error)
        self.send_command("clear_board", # be paranoid
                          self._check_error)
        self.send_command("komi %g" % self.gui.game.komi,
                          self._check_error)
        if self.gui.game.handicap > 0:
            self.send_command("fixed_handicap %d" % self.gui.game.handicap,
                              self._check_error)
            # FIXME: should check we agree with engine
        # no time limit
        self.send_command("time_settings 0 1 0",
                          self._check_error)

    def set_game(self):
        gamestate = self.gui.game.scratch_state
        for p in self.gui.game.pieces:
            location = p.location(self.gui.game.scratch_state)
            if location.holder != self.gui.game.board:
                # this piece is (hopefully) captured in a pool
                # FIXME: this condition may not be correct for all games
                continue
            locstr = self.__notation.location_notation.location_name(location)
            self.send_command("play %s %s" % (self.__color[gamestate.piece_owner(p)],
                                              locstr),
                              self._check_error)

    def _check_error(self, success, answer, cmd_id):
        # FIXME: should signal refusal to play
        if not success:
            logger.error("%s|ERROR for command %d",
                         self.__color[self.player][0], cmd_id)
        return success

    @property
    def color(self):
        return self.__color

    def write(self, data):
        raise NotImplementedError()
