#!/usr/bin/env python3

from watchdog.observers import Observer as WatchdogObserver
from watchdog.events import FileSystemEventHandler

import argparse
import collections, os, signal, sys
import tempfile
import time
import threading

# Some dogtail config tuning must occur before importing the rest.
# See https://gitlab.com/dogtail/dogtail/-/issues/12
import dogtail.config
dogtail.config.config.searchCutoffCount = 10
dogtail.config.config.runTimeout = 2
dogtail.config.config.actionDelay = 0 # no need to wait

from dogtail.procedural import focus
import dogtail.tree
from dogtail.tree import predicate, SearchError

os.environ.update(LC_ALL='C', LANGUAGE='',
                  GTK_MODULES='gail:atk-bridge',
                  QT_LINUX_ACCESSIBILITY_ALWAYS_ON='1')

# some games to load
files = {'Omaha/test/data/go19a.sgf': 'Go',
         'Omaha/test/data/shogi1.pgn': 'Shogi',
         'Omaha/test/data/minishogi.pgn': 'Mini Shogi',
         'Omaha/test/data/gorogoroshogi.pgn': 'Goro Goro Shogi',
         'Omaha/test/data/judkinsshogi.pgn': 'Judkins Shogi',
         'Omaha/test/data/tori.pgn': 'Tori Shogi',
         'Omaha/test/data/forchess-cutthroat0.pgn': 'Cutthroat Forchess',
         }

# special params for games to be tested, None if no param dialog expected
games_params = {
    'Chess': ((None, None),
              (None, {'White': ('XBoard-compatible program',
                                './scripts/stubengine Omaha/test/data/chess-xboard-1-W.data',
                                ),
                      'Black': ('XBoard-compatible program',
                                './scripts/stubengine Omaha/test/data/chess-xboard-1-B.data',
                                ),
                      },
               ),
              ),
    'Chess 5x5': ((None, None),
                  ),
    "Gliński's Chess": ((None, None),
                        ),
    "Shogi": (({}, None),
              ({}, {'Sente': ('XShogi-compatible program',
                              './scripts/stubengine Omaha/test/data/shogi-xshogi-1-S.data',
                              ),
                    'Gote': ('XShogi-compatible program',
                             './scripts/stubengine Omaha/test/data/shogi-xshogi-1-G.data',
                             ),
                    },
               ),
              ({}, {'Sente': ('CSA-compatible program',
                              './scripts/stubengine Omaha/test/data/shogi-csa-1-S.data',
                              ),
                    'Gote': ('CSA-compatible program',
                             './scripts/stubengine Omaha/test/data/shogi-csa-1-G.data',
                             ),
                    },
               ),
              ),
    "Mini Shogi": (({}, None),
                   ({}, {'Sente': ('XBoard-compatible program',
                                   './scripts/stubengine Omaha/test/data/minishogi-xboard-1-W.data',
                                   ),
                         'Gote': ('XBoard-compatible program',
                                   './scripts/stubengine Omaha/test/data/minishogi-xboard-1-B.data',
                                  ),
                         },
                    ),
                   # handicap game
                   ({"Handicap": ("combo box", "3 - Rook and bishop")},
                    {'Sente': ('XBoard-compatible program',
                               './scripts/stubengine Omaha/test/data/minishogi-xboard-2-W.data',
                               ),
                     'Gote': ('XBoard-compatible program',
                              './scripts/stubengine Omaha/test/data/minishogi-xboard-2-B.data',
                              ),
                     },
                    ),
                   ),
    "Goro Goro Shogi": ((None, None),
                        ),
    "Judkins Shogi": ((None, None),
                      ),
    "Tori Shogi": ((None, None),
                   ),
    "Sho Shogi": ((None, None),
                  ),
    "Wa Shogi": ((None, None),
                 ),
    "Wa Shogi with drops": ((None, None),
                            ),
    'Go': ( ({ 'Board size': ("spin button", 19), 'Handicap stones': ("spin button", 5) },
             None),
            ({ 'Board size': ("spin button", 9), 'Handicap stones': ("spin button", 2) },
             None) ),
    "Go 19x19": (({}, None),
                 ),
    'Cutthroat Forchess': ((None, None),
                           ),
    "Checkers": (({}, None),
                 ),
    "Can't Stop, 2 players": ((None, None),
                ),
    "Can't Stop, 3 players": ((None, None),
                ),
    "Can't Stop, 4 players": ((None, None),
                ),
}

game_fileformats = {
    'Chess': ['.pgn'],
    'Shogi': ['.pgn'],
    'Mini Shogi': ['.pgn'],
    'Goro Goro Shogi': ['.pgn'],
    'Judkins Shogi': ['.pgn'],
    'Tori Shogi': ['.pgn'],

    'Go': ['.sgf'],
    'Go 19x19': ['.sgf'],
}

def my_click(ui, accessible):
    accessible.doActionNamed(ui.button_press_action)

class CreateMoveWatchdogEventHandler(FileSystemEventHandler):
    """Event handler for `watchdog`, to watch a 2-step savefile creation.

    `watchdog` runs in its own thread, so we use condition variables
    to communicate the "create tep file" and "move to final
    destination" events.
    """
    def __init__(self, tempname, finalname):
        self.tempname, self.finalname = tempname, finalname
        self.seen_create, self.seen_move = False, False
        self.created_cv = threading.Condition()
        self.moved_cv = threading.Condition()
    def on_created(self, event):
        if event.src_path == self.tempname:
            with self.created_cv:
                self.seen_create = True
                self.created_cv.notify()
    def on_moved(self, event):
        if event.src_path == self.tempname and event.dest_path == self.finalname:
            with self.moved_cv:
                self.seen_move = True
                self.moved_cv.notify()
    def wait_created(self):
        with self.created_cv:
            while not self.seen_create:
                if not self.created_cv.wait(timeout=5):
                    raise AssertionError("file %r not created after timeout" % self.tempname)
    def wait_moved(self):
        with self.moved_cv:
            while not self.seen_move:
                if not self.moved_cv.wait(timeout=5):
                    raise AssertionError("file %r not moved to %r after timeout" %
                                         (self.tempname, self.finalname))

def excercise_game(ui, app, game):
    # locate GUI items
    window = app.window("omaha - " + game)
    btn_prefs = window.button("Preferences")

    # open prefs dialog and test all renderers
    next_ones = None # don't know yet
    while next_ones != []:
        my_click(ui, btn_prefs)
        dlg = app.dialog("omaha - UI parameters")
        renderer_pane = dlg.findChild(predicate.GenericPredicate(name="Renderer for %s" % game))
        renderer_combo = renderer_pane.findChild(predicate.GenericPredicate(roleName="combo box"))
        if next_ones == None:
            default_choice = renderer_combo.name
            renderer_choices = renderer_combo.findChildren(predicate.GenericPredicate(roleName=ui.combo_item_role))
            # we will select all non-default choicesn, and then go to default again
            next_ones = ([ x.name for x in renderer_choices if x.name != default_choice ] +
                         [ default_choice ])
        next_choice = renderer_combo.child(roleName=ui.combo_item_role, name=next_ones.pop(0))
        next_choice.doActionNamed(ui.combo_item_select_action)
        my_click(ui, dlg.button("OK"))

    # write to file
    if game in game_fileformats:
        btn_save = window.button("Save")
        dirname = tempfile.mkdtemp(prefix="uitest_")
        for fmt in game_fileformats[game]:
            my_click(ui, btn_save)

            fname = os.path.join(dirname, game + fmt)
            print(" saving %r..." % fname)

            observer = WatchdogObserver()
            handler = CreateMoveWatchdogEventHandler(fname + ".part", fname)
            observer.schedule(handler, dirname, recursive=True)
            observer.start()

            ui.save_file(app, dlgname="omaha - save game", filename=fname)

            handler.wait_created()
            handler.wait_moved()

            observer.stop()
            observer.join()

            # FIXME check saved file: need to be accurate after loading

    # close window
    my_click(ui, window.button("Close"))

class TestUI:
    def check_toolkit(self, app):
        tkname = app.get_toolkit_name()
        assert tkname in self.toolkit_names
        self.tkversion = app.get_toolkit_version().split('.')

class GtkUI(TestUI):
    toolkit_names = [ "GAIL", "gtk" ]
    flags = "--toolkit=Gtk"
    button_press_action = "click"
    combo_item_select_action = "click"
    combo_item_role = "menu item"
    launcher_game_role = "table cell"
    def launcher_game_name(self, item):
        return item.text
    launcher_doubleclick_action = "activate"
    launcher_selectgame_action = None
    def open_file(self, app, dlgname, filename):
        self.selectfile(app, dlgname, filename)
    def selectfile(self, app, dlgname, filename):
        dlg = app.child(roleName="file chooser", name=dlgname)
        # open path textfield
        chooser_widget = dlg.child(roleName="file chooser", name="File Chooser Widget")
        chooser_widget.doActionNamed("show_location")
        # set path
        text_field = chooser_widget.child(name="Location Layer").child(roleName="text")
        text_field.text = filename
        time.sleep(0.1)
        my_click(self, dlg.button("Open"))
    def save_file(self, app, dlgname, filename):
        dlg = app.child(roleName="file chooser", name=dlgname)
        # open path textfield
        chooser_widget = dlg.child(roleName="file chooser", name="File Chooser Widget")
        chooser_widget.doActionNamed("show_location")
        # set path
        text_field = chooser_widget.child(roleName="text", label="Name:")
        text_field.text = filename
        time.sleep(0.1)
        my_click(self, dlg.button("OK"))


class QtUI(TestUI):
    toolkit_names = [ "Qt" ]
    flags = "--toolkit=Qt"
    button_press_action = "Press"
    combo_item_select_action = "Toggle"
    combo_item_role = "list item"
    launcher_game_role = "list item"
    def launcher_game_name(self, item):
        return item.name
    launcher_doubleclick_action = None
    @property
    def launcher_selectgame_action(self):
        if self.tkversion < ["5"]:
            # Qt4 does not provide a way to select
            return None
        return "Toggle"
    def open_file(self, app, dlgname, filename):
        self.selectfile(app, dlgname, filename, "Open")
    def save_file(self, app, dlgname, filename):
        self.selectfile(app, dlgname, filename, "Save")
    def selectfile(self, app, dlgname, filename, action):
        dlg = app.child(roleName="dialog", name=dlgname)
        btn = dlg.button(action)
        # set path
        text_field = dlg.child(roleName="text", name="File name:")
        text_field.text = filename

        #assert text_field.text == filename, "textfield reports %r != %r" % (text_field.text, filename)
        wanted = os.path.basename(filename)
        got = os.path.basename(text_field.text)
        assert got == wanted, "textfield reports %r != %r" % (got, wanted)

        my_click(self, btn)

TOOLKITS = dict(Gtk = GtkUI(),
                Qt = QtUI(),
)

def load_games(ui, app, launcher, files):
    num_files = len(files)
    for i, (f, game) in enumerate(files.items()):
        print(f" file {i+1}/{num_files}: {f}...")
        my_click(ui, launcher.button("Open"))
        ui.open_file(app, dlgname="omaha - load game",
                     filename=os.path.join(os.path.realpath(os.path.curdir), f))
        excercise_game(ui, app, game)

def dump_accessible_tree(acc, level=0):
    print(" " * level, acc)
    for c in acc.children:
        dump_accessible_tree(c, level+1)

def test_games(ui, app, launcher, games_params, btn_play):
    game_items = [(item, game)
                  for item, game in ((item, ui.launcher_game_name(item))
                                     for item in launcher.findChildren(
                                             predicate.GenericPredicate(roleName=ui.launcher_game_role)))
                  if game] # skip icons in gtk table <sigh>
    num_items = len(game_items)
    for i, (item, game) in enumerate(game_items):
        print(f" game {i+1}/{num_items}: {game}...")
        for paramset in games_params[game]:
            if paramset[0] is None:
                if ui.launcher_doubleclick_action:
                    for again in range(5, 0, -1):
                        try:
                            item.doActionNamed(ui.launcher_doubleclick_action)
                        except dogtail.tree.ActionNotSupported as ex:
                            print(f"{ex}, retrying up to {again} times")
                        else:
                            break
                elif ui.launcher_selectgame_action:
                    item.doActionNamed(ui.launcher_selectgame_action)
                    my_click(ui, btn_play)
                else:
                    print("WARNING: Don't know how to launch a game")
                    return
            else:
                if ui.launcher_doubleclick_action:
                    for again in range(5, 0, -1):
                        try:
                            item.doActionNamed(ui.launcher_doubleclick_action)
                        except dogtail.tree.ActionNotSupported as ex:
                            print(f"{ex}, retrying up to {again} times")
                        else:
                            break
                elif ui.launcher_selectgame_action:
                    item.doActionNamed(ui.launcher_selectgame_action)
                    my_click(ui, btn_play)
                else:
                    print("WARNING: Don't know how to launch a game")
                    return

                dlg_params = app.dialog("omaha - %s parameters" % game)
                for label, value in paramset[0].items():
                    param_panel = dlg_params.childNamed(label)
                    assert isinstance(value, tuple)
                    role, value = value
                    field = param_panel.child(roleName=role)
                    if role == "combo box":
                        field.childNamed(value).doActionNamed(ui.combo_item_select_action)
                my_click(ui, dlg_params.button("OK"))

            dlg_players = app.dialog("omaha - player configuration")
            if paramset[1]:
                for player, (driver, command) in paramset[1].items():
                    player_panel = dlg_players.findChild(predicate.GenericPredicate(
                        roleName="panel", name=player))
                    player_combo = player_panel.findChild(predicate.GenericPredicate(roleName="combo box"))
                    player_combo.childNamed(driver).doActionNamed(ui.combo_item_select_action)

                    cmd_panel = player_panel.childNamed("Command", showingOnly=True)
                    engine_cmd = cmd_panel.findChild(predicate.GenericPredicate(roleName="text"))
                    engine_cmd.text = command

                my_click(ui, dlg_players.button("OK"))
                time.sleep(2)
                dlg_endgame = app.dialog("omaha - Game over")
                my_click(ui, dlg_endgame.button("OK"))
                # close window
                window = app.window("omaha - " + game)
                my_click(ui, window.button("Close"))
            else:
                my_click(ui, dlg_players.button("OK"))
                excercise_game(ui, app, game)

# Forked from dogtail.utils.run to support Qt4.
# See https://gitlab.com/dogtail/dogtail/-/issues/13
def dogtail_wait_app(appName):
    time = 0
    interval = dogtail.config.config.runInterval
    desktop = dogtail.tree.root
    while time < dogtail.config.config.runTimeout:
        time = time + interval
        try:
            for child in desktop.children[::-1]:
                if child.name == appName:
                    for grandchild in child.children:
                        if grandchild.roleName in ['frame', 'window']:
                            focus.application.node = child
                            dogtail.utils.doDelay(interval)
                            return child
        except AttributeError:  # pragma: no cover
            pass
        doDelay(interval)
    return None

class IsAWindowNamed(predicate.IsAWindowNamed):
    def _genCompareFunc(self):
        def satisfiedByNode(node):
            return node.roleName in ['frame', 'window'] and predicate.stringMatches(self.windowName, node.name)
        return satisfiedByNode

def uitest(ui, args):
    try:
        pid = dogtail.utils.run(
            f"{args.python} ./scripts/omaha {ui.flags}",
            dumb=True,
            appName="omaha")
        app = dogtail_wait_app("omaha")
        if not app:
            print("Dogtail failed to find app")
            exit(1)

        # check toolkit and extract its version
        ui.check_toolkit(app)

        assert app.name == "omaha"
        launcher = app.findChild(IsAWindowNamed(windowName="omaha - launcher"))

        btn_play = launcher.button("Play")

        test_games(ui, app, launcher, games_params, btn_play)
        load_games(ui, app, launcher, files)

    except SystemExit:
        raise

    else:
        print("UI test finished.")
        my_click(ui, launcher.button("Quit"))

#

def cli_parser():
    cli_parser = argparse.ArgumentParser(
        description="Omaha UI tester")
    cli_parser.add_argument('-v', '--verbose', action="count", default=0,
                            help='increase verbosity level')
    cli_parser.add_argument('--toolkit', default='Gtk',
                            help='toolkit to use in Omaha')
    cli_parser.add_argument('--python', default='python3 -Wd',
                            help='python interpreter to use for Omaha')
    return cli_parser

if __name__ == '__main__':
    args = cli_parser().parse_args()

    if args.verbose >= 1:
        dogtail.config.config.debugSearching = True
        if args.verbose >= 2:
            dogtail.config.config.debugSleep = True
            dogtail.config.config.debugSearchPaths = True
            if args.verbose >= 3:
                dogtail.config.config.actionDelay = (args.verbose - 2) * 0.3

    if args.toolkit == "Qt":
        print("ERROR: QtWidgets AT-SPI support has too many limitations for uitest")
        # namely:
        # - cannot select a combo item without resorting to click(), which is
        #   broken (in pyatspi?) when running in xvfb
        # - cannot properly change QLineEdit text from atspi
        # - you name it
        sys.exit(1)

    uitest(TOOLKITS[args.toolkit], args)
