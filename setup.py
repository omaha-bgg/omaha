#!/usr/bin/env python3

from setuptools import setup, find_packages
import os, sys

## real setup work

setup(name='omaha',
      description='A (wannabe) GUI to play arbitrary board games.',
      url='https://gitlab.com/omaha-bgg/omaha',
      author='Yann Dirson',
      author_email='ydirson@free.fr',

      python_requires = ">3.9",
      setup_requires = ['setuptools_scm'],
      use_scm_version=True,

      packages = find_packages(),
      scripts=[ 'scripts/omaha' ],
      data_files=( [ ('share/applications', ['omaha.desktop']) ] +
                   # recursive copy of relevant plugin files
                   [ ('share/games/omaha/' + path,
                      [ path + '/' + f for f in files
                        if f.endswith('.desktop')
                        or f.endswith('.png') or f.endswith('.svg') ])
                     for path, dirs, files in os.walk('plugins')
                     ] +
                   [ ('share/' + path,
                      [ path + '/' + f for f in files
                        if f.endswith('.png') or f.endswith('.svg') ])
                     for path, dirs, files in os.walk('icons')
                     ]),

      package_data = {'Omaha/test': ['data/*']},

      install_requires=[
          'pyparsing',
          'frozendict',
          'numpy',
          'typing_extensions >= 4.0; python_version<"3.11"',
      ],
      extras_require = {
          'gtk': ['PyGObject', 'pycairo'],
          'qt': ['PySide2'],
          'dev': ['nose', 'coverage'],
      }
)
